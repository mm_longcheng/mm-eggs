/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFilePath.h"
#include "core/mm_file_system.h"
#include "core/mm_byte.h"

MM_EXPORT_DISH void mmPathSplitFileName(struct mm_string* qualified_name, struct mm_string* basename, struct mm_string* pathname)
{
    struct mm_string path;
    size_t i = -1;
    mm_string_init(&path);
    mm_string_assign(&path, qualified_name);
    // Replace \ with / first
    mm_string_replace_char(&path, '\\', '/');
    // split based on final /
    i = mm_string_find_last_of(&path, '/');
    if (-1 == i)
    {
        mm_string_assign(basename, qualified_name);
        mm_string_clear(pathname);
    }
    else
    {
        mm_string_assignsn(basename, &(path.s[i + 1]), path.l - i - 1);
        mm_string_assignsn(pathname, &(path.s[0]), i + 1);
    }
    //
    mm_string_destroy(&path);
}
// a.txt => a .txt
MM_EXPORT_DISH void mmPathSplitSuffixName(struct mm_string* qualified_name, struct mm_string* basename, struct mm_string* suffixname)
{
    struct mm_string path;
    size_t i = (size_t)MM_STRING_INVALID_POS;
    char c = 0;
    mm_string_init(&path);
    mm_string_assign(&path, qualified_name);
    // split based on final '.' but can not cross '\\' '/'.
    if (0 < path.l)
    {
        for (i = path.l - 1; (size_t)MM_STRING_INVALID_POS != i; --i)
        {
            c = path.s[i];
            if ('/' == c || '\\' == c || '.' == c)
            {
                break;
            }
        }
    }

    if ((size_t)MM_STRING_INVALID_POS != i)
    {
        i = ('.' == c) ? i : (size_t)MM_STRING_INVALID_POS;
    }

    if ((size_t)MM_STRING_INVALID_POS == i)
    {
        mm_string_assign(basename, qualified_name);
        mm_string_clear(suffixname);
    }
    else
    {
        mm_string_assignsn(suffixname, &(path.s[i + 1]), path.l - i - 1);
        mm_string_assignsn(basename, &(path.s[0]), i);
    }
    //
    mm_string_destroy(&path);
}
MM_EXPORT_DISH void mmCleanPath(struct mm_string* clean_path, const char* complex_path)
{
    static const char* exclude_path_0 = "/..";
    static const char* exclude_path_1 = "/./";
    static const size_t exclude_size = 3;
    size_t i = 0;
    size_t last_index = 0;
    struct mm_string v;
    struct mm_string l;
    struct mm_string r;
    mm_string_init(&v);
    mm_string_init(&l);
    mm_string_init(&r);
    mm_string_assigns(&v, complex_path);
    // "./xxx" => xxx
    if (2 < v.l && '.' == v.s[0] && '/' == v.s[1])
    {
        mm_string_substr(&v, &v, 2, v.l - 2);
    }
    // "xxx/." => xxx
    if (2 < v.l && '/' == v.s[v.l - 2] && '.' == v.s[v.l - 1])
    {
        mm_string_substr(&v, &v, 0, v.l - 2);
    }
    // "/./"
    if (exclude_size < v.l)
    {
        mm_string_assign(&l, &v);
        do
        {
            mm_string_assign(&v, &l);
            for (i = 0; i <= v.l - exclude_size; ++i)
            {
                if (0 == mm_memcmp(&v.s[i], exclude_path_1, exclude_size) && ((0 != i) || (1 == i && '.' != v.s[i - 1])))
                {
                    mm_string_substr(&v, &l, 0, i);
                    mm_string_substr(&v, &r, i + 2, v.l);
                    mm_string_append(&l, r.s);
                    break;
                }
            }
        } while (i <= v.l - exclude_size);
        mm_string_assign(&v, &l);
    }
    // "../.." "./.."
    if (exclude_size < v.l)
    {
        mm_string_assign(&l, &v);
        do
        {
            mm_string_assign(&v, &l);
            for (i = 0; i <= v.l - exclude_size; ++i)
            {
                if (0 == mm_memcmp(&v.s[i], exclude_path_0, exclude_size) && ((0 != i && '.' != v.s[i - 1])))
                {
                    mm_string_substr(&v, &l, 0, last_index);
                    mm_string_substr(&v, &r, i + exclude_size, v.l);
                    mm_string_append(&l, r.s);
                    break;
                }
                if ('/' == v.s[i])
                {
                    last_index = i;
                }
            }
            last_index = 0;
        } while (i <= v.l - exclude_size);
        mm_string_assign(&v, &l);
    }
    // v directory none suffix.
    mmDirectoryNoneSuffix(&v, v.s);
    // copy v to clean_path.
    mm_string_assign(clean_path, &v);
    mm_string_destroy(&v);
    mm_string_destroy(&l);
    mm_string_destroy(&r);
}
MM_EXPORT_DISH void mmDirectoryHaveSuffix(struct mm_string* directory_path, const char* complex_path)
{
    mm_string_assigns(directory_path, complex_path);
    if (!mm_string_empty(directory_path))
    {
        char c = directory_path->s[directory_path->l - 1];
        if ('/' == c || '\\' == c)
        {
            directory_path->s[directory_path->l - 1] = '/';
        }
        else
        {
            mm_string_append(directory_path, "/");
        }
    }
}
MM_EXPORT_DISH void mmDirectoryNoneSuffix(struct mm_string* directory_path, const char* complex_path)
{
    mm_string_assigns(directory_path, complex_path);
    if (!mm_string_empty(directory_path))
    {
        char c = directory_path->s[directory_path->l - 1];
        if ('/' == c || '\\' == c)
        {
            directory_path->s[directory_path->l - 1] = '\0';
            directory_path->l--;
        }
    }
}
MM_EXPORT_DISH void mmDirectoryRemovePrefixPath(struct mm_string* directory_path, const char* pathname, const char* prefix_path)
{
    // prefix_path = "." need remove none.
    struct mm_string prefix;
    struct mm_string path;
    mm_string_init(&prefix);
    mm_string_init(&path);
    mm_string_assigns(&path, pathname);
    mm_string_assigns(&prefix, prefix_path);
    mm_string_append(&prefix, 0 == mm_strlen(prefix_path) ? "" : "/");
    mm_string_substr(&path, &path, ((0 == mm_memcmp(prefix.s, path.s, prefix.l)) ? prefix.l : 0), path.l);
    mmCleanPath(&path, path.s);
    mm_string_assign(directory_path, &path);
    mm_string_destroy(&prefix);
    mm_string_destroy(&path);
}
MM_EXPORT_DISH void mmDirectoryParentPath(struct mm_string* qualified_name, struct mm_string* basename, struct mm_string* pathname)
{
    mmPathSplitFileName(qualified_name, basename, pathname);
    mmDirectoryNoneSuffix(pathname, pathname->s);
}

MM_EXPORT_DISH void mmConcatenatePath(struct mm_string* path, const char* base, const char* name)
{
    if (0 == mm_strlen(base) || 0 != mm_is_absolute_path(name))
    {
        mm_string_assigns(path, name);
    }
    else
    {
        mm_string_assigns(path, base);
        mm_string_append(path, "/");
        mm_string_append(path, name);
    }
}
// acquire file buffer.
MM_EXPORT_DISH void mmAbsolutePathAcquireFileByteBuffer(const char* file_name, struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_reset(byte_buffer);
    //
    if (NULL != file_name)
    {
        FILE* fp = fopen(file_name, "rb");
        if (fp)
        {
            fseek(fp, 0, SEEK_END);
            byte_buffer->length = ftell(fp);
            fseek(fp, 0, SEEK_SET);
            mm_byte_buffer_malloc(byte_buffer, byte_buffer->length);
            byte_buffer->length = (mm_uint32_t)fread(byte_buffer->buffer, sizeof(char), byte_buffer->length, fp);
            fclose(fp);
        }
    }
}
// release file buffer.
MM_EXPORT_DISH void mmAbsolutePathReleaseRileByteBuffer(struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_free(byte_buffer);
}
// mkdir if not exist.
MM_EXPORT_DISH int mmMkDirIfNotExist(const char* directory)
{
    return 0 != mm_access(directory, MM_ACCESS_X_OK) ? mm_mkdir(directory, 0777) : 0;
}
