/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileZip_h__
#define __mmFileZip_h__

#include "core/mm_core.h"
#include "core/mm_string.h"
#include "core/mm_byte.h"

#include "container/mm_rbtree_string.h"

#include "dish/mm_dish_export.h"

#include "core/mm_prefix.h"

struct zzip_dir;

struct mmZipEntry
{
    /* compression method */
    int	 	compr;
    /* compressed size */
    int     csize;
    /* file size / decompressed size */
    int	 	dsize;
    /* file attib */
    int     attri;
    /* file name / strdupped name */
    struct mm_string fullname;
    /* base name / strdupped name */
    struct mm_string basename;
    /* path name / strdupped name */
    struct mm_string pathname;
};
MM_EXPORT_DISH void mmZipEntry_Init(struct mmZipEntry* p);
MM_EXPORT_DISH void mmZipEntry_Destroy(struct mmZipEntry* p);
//
// check file name whether is directory.1 is directory 0 not.
MM_EXPORT_DISH void mmZipEntry_SetIsDirectory(struct mmZipEntry* p, int is_directory);
MM_EXPORT_DISH int mmZipEntry_GetIsDirectory(struct mmZipEntry* p);
MM_EXPORT_DISH void mmZipEntry_UpdateAttri(struct mmZipEntry* p);

struct mmZipDirectory
{
    struct mm_string pathname;
    struct mm_rbtree_string_vpt entry_rbtree;
};
MM_EXPORT_DISH void mmZipDirectory_Init(struct mmZipDirectory* p);
MM_EXPORT_DISH void mmZipDirectory_Destroy(struct mmZipDirectory* p);
//
MM_EXPORT_DISH struct mmZipEntry* mmZipDirectory_Add(struct mmZipDirectory* p, struct mm_string* fullname);
MM_EXPORT_DISH struct mmZipEntry* mmZipDirectory_Get(struct mmZipDirectory* p, struct mm_string* fullname);
MM_EXPORT_DISH struct mmZipEntry* mmZipDirectory_GetInstance(struct mmZipDirectory* p, struct mm_string* fullname);
MM_EXPORT_DISH void mmZipDirectory_Rmv(struct mmZipDirectory* p, struct mm_string* fullname);
MM_EXPORT_DISH void mmZipDirectory_Clear(struct mmZipDirectory* p);

struct mmFileZip
{
    struct mm_string filename;
    struct mm_string filter;
    struct mm_rbtree_string_vpt directory_rbtree;
    struct zzip_dir* zzip_directory;
};
MM_EXPORT_DISH void mmFileZip_Init(struct mmFileZip* p);
MM_EXPORT_DISH void mmFileZip_Destroy(struct mmFileZip* p);
//
MM_EXPORT_DISH void mmFileZip_SetFilename(struct mmFileZip* p, const char* filename);
MM_EXPORT_DISH void mmFileZip_SetFilter(struct mmFileZip* p, const char* filter);
//
MM_EXPORT_DISH void mmFileZip_Fopen(struct mmFileZip* p);
MM_EXPORT_DISH void mmFileZip_Close(struct mmFileZip* p);
//
MM_EXPORT_DISH void mmFileZip_AcquireAnalysis(struct mmFileZip* p);
MM_EXPORT_DISH void mmFileZip_ReleaseAnalysis(struct mmFileZip* p);
//
// acquire file buffer.
MM_EXPORT_DISH void mmFileZip_AcquireFileByteBuffer(struct mmFileZip* p, const char* fullname, struct mm_byte_buffer* byte_buffer);
// release file buffer.
MM_EXPORT_DISH void mmFileZip_ReleaseFileByteBuffer(struct mmFileZip* p, struct mm_byte_buffer* byte_buffer);
// check file name exists.1 exists 0 not exists.
MM_EXPORT_DISH int mmFileZip_Exists(struct mmFileZip* p, const char* fullname);
// get entry for file name.
MM_EXPORT_DISH struct mmZipEntry* mmFileZip_GetEntry(struct mmFileZip* p, const char* fullname);
//
MM_EXPORT_DISH struct mmZipDirectory* mmFileZip_Add(struct mmFileZip* p, struct mm_string* pathname);
MM_EXPORT_DISH struct mmZipDirectory* mmFileZip_Get(struct mmFileZip* p, struct mm_string* pathname);
MM_EXPORT_DISH struct mmZipDirectory* mmFileZip_GetInstance(struct mmFileZip* p, struct mm_string* pathname);
MM_EXPORT_DISH void mmFileZip_Rmv(struct mmFileZip* p, struct mm_string* pathname);
MM_EXPORT_DISH void mmFileZip_Clear(struct mmFileZip* p);

#include "core/mm_suffix.h"

#endif//__mmFileZip_h__
