/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFilePath_h__
#define __mmFilePath_h__

#include "core/mm_core.h"
#include "core/mm_string.h"
#include "core/mm_byte.h"

#include "dish/mm_dish_export.h"

#include "core/mm_prefix.h"

// a/b/c.txt => a/b/ c.txt
MM_EXPORT_DISH void mmPathSplitFileName(struct mm_string* qualified_name, struct mm_string* basename, struct mm_string* pathname);

// a.txt => a .txt
MM_EXPORT_DISH void mmPathSplitSuffixName(struct mm_string* qualified_name, struct mm_string* basename, struct mm_string* suffixname);

// clean path.
//   "./xxx" => xxx
//   "xxx/." => xxx
//   "/.." "/./"
// ././a/b/../c.txt => ./a/c.txt
MM_EXPORT_DISH void mmCleanPath(struct mm_string* clean_path, const char* complex_path);

// a/b  => a/b/
MM_EXPORT_DISH void mmDirectoryHaveSuffix(struct mm_string* directory_path, const char* complex_path);

// a/b/ => a/b
MM_EXPORT_DISH void mmDirectoryNoneSuffix(struct mm_string* directory_path, const char* complex_path);

// a/b/c.txt a  => b.txt
MM_EXPORT_DISH void mmDirectoryRemovePrefixPath(struct mm_string* directory_path, const char* pathname, const char* prefix_path);

// a/b/c.txt => a/ b
// a/b/      => a/ b
MM_EXPORT_DISH void mmDirectoryParentPath(struct mm_string* qualified_name, struct mm_string* basename, struct mm_string* pathname);

// a b.txt   => a/b.txt
MM_EXPORT_DISH void mmConcatenatePath(struct mm_string* path, const char* base, const char* name);

// acquire file buffer.
MM_EXPORT_DISH void mmAbsolutePathAcquireFileByteBuffer(const char* file_name, struct mm_byte_buffer* byte_buffer);

// release file buffer.
MM_EXPORT_DISH void mmAbsolutePathReleaseFileByteBuffer(struct mm_byte_buffer* byte_buffer);

// mkdir if not exist.
MM_EXPORT_DISH int mmMkDirIfNotExist(const char* directory);

#include "core/mm_suffix.h"

#endif//__mmFilePath_h__
