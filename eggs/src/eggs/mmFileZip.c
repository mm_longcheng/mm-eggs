/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFileZip.h"
#include "mmFilePath.h"

#include "core/mm_logger.h"
#include "core/mm_file_system.h"

#include <zzip/zzip.h>

// Utility method to format out zzip errors
static const char* __static_GetZzipErrorDescription(zzip_error_t zzip_error_code)
{
    const char* error_desc = NULL;
    switch (zzip_error_code)
    {
    case ZZIP_NO_ERROR:
        break;
    case ZZIP_OUTOFMEM:
        error_desc = "out of memory.";
        break;
    case ZZIP_DIR_OPEN:
    case ZZIP_DIR_STAT:
    case ZZIP_DIR_SEEK:
    case ZZIP_DIR_READ:
        error_desc = "unable to read zip file.";
        break;
    case ZZIP_UNSUPP_COMPR:
        error_desc = "unsupported compression format.";
        break;
    case ZZIP_CORRUPTED:
        error_desc = "corrupted archive.";
        break;
    default:
        error_desc = "unknown error.";
        break;
    };
    return error_desc;
}

static void __static_CheckZzipError(int zzip_error_code, const char* operation)
{
    if (zzip_error_code != ZZIP_NO_ERROR)
    {
        const char* error_desc = __static_GetZzipErrorDescription((zzip_error_t)(zzip_error_code));
        struct mm_logger* g_logger = mm_logger_instance();
        mm_logger_log_E(g_logger, "%s %d zzip error(%d):%s.", __FUNCTION__, __LINE__, zzip_error_code, error_desc);
    }
}

MM_EXPORT_DISH void mmZipEntry_Init(struct mmZipEntry* p)
{
    p->compr = 0;
    p->csize = 0;
    p->dsize = 0;
    p->attri = _A_NORMAL;
    mm_string_init(&p->fullname);
    mm_string_init(&p->basename);
    mm_string_init(&p->pathname);
    //
    mm_string_assigns(&p->fullname, "");
    mm_string_assigns(&p->basename, "");
    mm_string_assigns(&p->pathname, "");
}
MM_EXPORT_DISH void mmZipEntry_Destroy(struct mmZipEntry* p)
{
    p->compr = 0;
    p->csize = 0;
    p->dsize = 0;
    p->attri = _A_NORMAL;
    mm_string_destroy(&p->fullname);
    mm_string_destroy(&p->basename);
    mm_string_destroy(&p->pathname);
}
MM_EXPORT_DISH void mmZipEntry_SetIsDirectory(struct mmZipEntry* p, int is_directory)
{
    if (0 == is_directory)
    {
        p->attri = p->attri & (~_A_SUBDIR);
    }
    else
    {
        p->attri = p->attri | _A_SUBDIR;
    }
}
MM_EXPORT_DISH int mmZipEntry_GetIsDirectory(struct mmZipEntry* p)
{
    return (0 != (p->attri & _A_SUBDIR)) ? 1 : 0;
}
MM_EXPORT_DISH void mmZipEntry_UpdateAttri(struct mmZipEntry* p)
{
    int d = (0 != p->fullname.l && 0 == p->basename.l && 0 != p->pathname.l) ? 1 : 0;
    mmZipEntry_SetIsDirectory(p, d);
}

MM_EXPORT_DISH void mmZipDirectory_Init(struct mmZipDirectory* p)
{
    struct mm_rbtree_string_vpt_alloc rbtree_string_vpt_alloc;
    //
    mm_string_init(&p->pathname);
    mm_rbtree_string_vpt_init(&p->entry_rbtree);
    //
    rbtree_string_vpt_alloc.alloc = &mm_rbtree_string_vpt_weak_alloc;
    rbtree_string_vpt_alloc.relax = &mm_rbtree_string_vpt_weak_relax;
    rbtree_string_vpt_alloc.obj = p;
    mm_rbtree_string_vpt_assign_alloc(&p->entry_rbtree, &rbtree_string_vpt_alloc);
}
MM_EXPORT_DISH void mmZipDirectory_Destroy(struct mmZipDirectory* p)
{
    mmZipDirectory_Clear(p);
    //
    mm_string_destroy(&p->pathname);
    mm_rbtree_string_vpt_destroy(&p->entry_rbtree);
}
//
MM_EXPORT_DISH struct mmZipEntry* mmZipDirectory_Add(struct mmZipDirectory* p, struct mm_string* fullname)
{
    struct mmZipEntry* e = mmZipDirectory_Get(p, fullname);
    if (NULL == e)
    {
        e = (struct mmZipEntry*)mm_malloc(sizeof(struct mmZipEntry));
        mmZipEntry_Init(e);
        // cache the entry info.
        mm_rbtree_string_vpt_set(&p->entry_rbtree, fullname, e);
    }
    return e;
}
MM_EXPORT_DISH struct mmZipEntry* mmZipDirectory_Get(struct mmZipDirectory* p, struct mm_string* fullname)
{
    return (struct mmZipEntry*)mm_rbtree_string_vpt_get(&p->entry_rbtree, fullname);
}
MM_EXPORT_DISH struct mmZipEntry* mmZipDirectory_GetInstance(struct mmZipDirectory* p, struct mm_string* fullname)
{
    struct mmZipEntry* e = mmZipDirectory_Get(p, fullname);
    if (NULL == e)
    {
        e = mmZipDirectory_Add(p, fullname);
    }
    return e;
}
MM_EXPORT_DISH void mmZipDirectory_Rmv(struct mmZipDirectory* p, struct mm_string* fullname)
{
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmZipEntry* e = NULL;

    it = mm_rbtree_string_vpt_get_iterator(&p->entry_rbtree, fullname);
    if (NULL != it)
    {
        e = (struct mmZipEntry*)(it->v);
        mm_rbtree_string_vpt_erase(&p->entry_rbtree, it);
        mmZipEntry_Destroy(e);
        mm_free(e);
    }
}
MM_EXPORT_DISH void mmZipDirectory_Clear(struct mmZipDirectory* p)
{
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmZipEntry* e = NULL;
    n = mm_rb_first(&p->entry_rbtree.rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmZipEntry*)(it->v);
        mm_rbtree_string_vpt_erase(&p->entry_rbtree, it);
        mmZipEntry_Destroy(e);
        mm_free(e);
    }
}

MM_EXPORT_DISH void mmFileZip_Init(struct mmFileZip* p)
{
    struct mm_rbtree_string_vpt_alloc rbtree_string_vpt_alloc;
    //
    mm_string_init(&p->filename);
    mm_string_init(&p->filter);
    mm_rbtree_string_vpt_init(&p->directory_rbtree);
    p->zzip_directory = NULL;
    //
    rbtree_string_vpt_alloc.alloc = &mm_rbtree_string_vpt_weak_alloc;
    rbtree_string_vpt_alloc.relax = &mm_rbtree_string_vpt_weak_relax;
    rbtree_string_vpt_alloc.obj = p;
    mm_rbtree_string_vpt_assign_alloc(&p->directory_rbtree, &rbtree_string_vpt_alloc);
}
MM_EXPORT_DISH void mmFileZip_Destroy(struct mmFileZip* p)
{
    mmFileZip_Clear(p);
    mmFileZip_Close(p);
    //
    mm_string_destroy(&p->filename);
    mm_string_destroy(&p->filter);
    mm_rbtree_string_vpt_destroy(&p->directory_rbtree);
    p->zzip_directory = NULL;
}
//
MM_EXPORT_DISH void mmFileZip_SetFilename(struct mmFileZip* p, const char* filename)
{
    mm_string_assigns(&p->filename, filename);
}
MM_EXPORT_DISH void mmFileZip_SetFilter(struct mmFileZip* p, const char* filter)
{
    if (0 == mm_strcmp(filter, ".") || 0 == mm_strcmp(filter, "./"))
    {
        mm_string_assigns(&p->filter, "");
    }
    else
    {
        mm_string_assigns(&p->filter, filter);
    }
}
//
MM_EXPORT_DISH void mmFileZip_Fopen(struct mmFileZip* p)
{
    if (!mm_string_empty(&p->filename))
    {
        zzip_error_t zzip_error_code;
        // try close the current zip file.
        mmFileZip_Close(p);
        p->zzip_directory = zzip_dir_open(p->filename.s, &zzip_error_code);
        __static_CheckZzipError(zzip_error_code, "opening archive");
    }
}
MM_EXPORT_DISH void mmFileZip_Close(struct mmFileZip* p)
{
    if (NULL != p->zzip_directory)
    {
        zzip_dir_close(p->zzip_directory);
        p->zzip_directory = NULL;
    }
}
//
MM_EXPORT_DISH void mmFileZip_AcquireAnalysis(struct mmFileZip* p)
{
    mmFileZip_Clear(p);
    //
    if (NULL != p->zzip_directory)
    {
        struct mm_string qualified_name;
        struct mm_string pathname;
        struct mm_string basename;
        struct mm_string path_substr;
        ZZIP_DIRENT zzip_entry;
        struct mmZipDirectory* zip_directory = NULL;
        struct mmZipEntry* zip_entry = NULL;
        mm_string_init(&qualified_name);
        mm_string_init(&pathname);
        mm_string_init(&basename);
        mm_string_init(&path_substr);
        // cache names
        while (zzip_dir_read(p->zzip_directory, &zzip_entry))
        {
            mm_string_assigns(&qualified_name, zzip_entry.d_name);
            mm_string_substr(&qualified_name, &path_substr, 0, p->filter.l);
            if (0 == p->filter.l || 0 == mm_string_compare(&path_substr, &p->filter))
            {
                mmPathSplitFileName(&qualified_name, &basename, &pathname);
                // directory
                zip_directory = mmFileZip_GetInstance(p, &pathname);
                // entry
                zip_entry = mmZipDirectory_GetInstance(zip_directory, &qualified_name);

                zip_entry->compr = zzip_entry.d_compr;
                zip_entry->csize = zzip_entry.d_csize;
                zip_entry->dsize = zzip_entry.st_size;
                mm_string_assign(&zip_entry->fullname, &qualified_name);
                mm_string_assign(&zip_entry->basename, &basename);
                mm_string_assign(&zip_entry->pathname, &pathname);
                mmZipEntry_UpdateAttri(zip_entry);

                // add into parent path.
                mmDirectoryNoneSuffix(&qualified_name, pathname.s);
                mmPathSplitFileName(&qualified_name, &basename, &pathname);

                // directory
                zip_directory = mmFileZip_GetInstance(p, &pathname);
                // entry
                zip_entry = mmZipDirectory_GetInstance(zip_directory, &qualified_name);

                zip_entry->compr = 0;
                zip_entry->csize = 0;
                zip_entry->dsize = 0;
                mm_string_assign(&zip_entry->fullname, &qualified_name);
                mm_string_assign(&zip_entry->basename, &basename);
                mm_string_assign(&zip_entry->pathname, &pathname);
                mmZipEntry_SetIsDirectory(zip_entry, 1);
            }
        }
        mm_string_destroy(&qualified_name);
        mm_string_destroy(&pathname);
        mm_string_destroy(&basename);
        mm_string_destroy(&path_substr);
    }
}
MM_EXPORT_DISH void mmFileZip_ReleaseAnalysis(struct mmFileZip* p)
{
    mmFileZip_Clear(p);
}
//
// acquire file buffer.
MM_EXPORT_DISH void mmFileZip_AcquireFileByteBuffer(struct mmFileZip* p, const char* fullname, struct mm_byte_buffer* byte_buffer)
{
    do
    {
        struct mm_logger* g_logger = mm_logger_instance();
        struct mmZipEntry* entry = NULL;
        ZZIP_FILE* zzip_file = NULL;
        zzip_ssize_t r = 0;
        //
        if (NULL == p->zzip_directory)
        {
            // need do nothing.
            break;
        }
        //
        mm_byte_buffer_reset(byte_buffer);
        //
        entry = mmFileZip_GetEntry(p, fullname);
        if (NULL == entry)
        {
            mm_logger_log_E(g_logger, "%s %d can not find file:%s %s.", __FUNCTION__, __LINE__, p->filename.s, fullname);
            break;
        }
        // format not used here (always binary)
        zzip_file = zzip_file_open(p->zzip_directory, fullname, ZZIP_ONLYZIP | ZZIP_CASELESS);
        if (NULL == zzip_file) // try if we find the file
        {
            int zzip_error_code = zzip_error(p->zzip_directory);
            const char* error_desc = __static_GetZzipErrorDescription((zzip_error_t)(zzip_error_code));
            mm_logger_log_E(g_logger, "%s %d can not find file:%s %s error(%d):%s.", __FUNCTION__, __LINE__, p->filename.s, fullname, zzip_error_code, error_desc);
            break;
        }
        //
        mm_byte_buffer_malloc(byte_buffer, entry->dsize);
        //
        r = zzip_file_read(zzip_file, byte_buffer->buffer + byte_buffer->offset, byte_buffer->length);
        if (0 > r)
        {
            ZZIP_DIR* dir = zzip_dirhandle(zzip_file);
            const char* msg = zzip_strerror_of(dir);
            mm_logger_log_E(g_logger, "%s %d can not read file:%s %s %s.", __FUNCTION__, __LINE__, p->filename.s, fullname, msg);
            // free buffer.
            mm_free(byte_buffer->buffer);
            mm_byte_buffer_reset(byte_buffer);
        }
        zzip_file_close(zzip_file);
    } while (0);
}
// release file buffer.
MM_EXPORT_DISH void mmFileZip_ReleaseFileByteBuffer(struct mmFileZip* p, struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_free(byte_buffer);
}
// check file name exists.1 exists 0 not exists.
MM_EXPORT_DISH int mmFileZip_Exists(struct mmFileZip* p, const char* fullname)
{
    int code = 0;
    do
    {
        //
        if (NULL == p->zzip_directory)
        {
            // need do nothing.
            code = 0;
            break;
        }
        code = NULL == mmFileZip_GetEntry(p, fullname) ? 0 : 1;
    } while (0);
    return code;
}
// get entry for file name.
MM_EXPORT_DISH struct mmZipEntry* mmFileZip_GetEntry(struct mmFileZip* p, const char* fullname)
{
    struct mm_string qualified_name;
    struct mm_string pathname;
    struct mm_string basename;
    struct mmZipDirectory* zip_directory = NULL;
    struct mmZipEntry* zip_entry = NULL;

    mm_string_make_weak(&qualified_name, fullname);

    mm_string_init(&pathname);
    mm_string_init(&basename);

    mmPathSplitFileName(&qualified_name, &basename, &pathname);

    do
    {
        // directory
        zip_directory = mmFileZip_Get(p, &pathname);
        if (NULL == zip_directory)
        {
            // can not find the directory.
            break;
        }
        // entry
        zip_entry = mmZipDirectory_Get(zip_directory, &qualified_name);
    } while (0);

    mm_string_destroy(&pathname);
    mm_string_destroy(&basename);
    //
    return zip_entry;
}
MM_EXPORT_DISH struct mmZipDirectory* mmFileZip_Add(struct mmFileZip* p, struct mm_string* pathname)
{
    struct mmZipDirectory* e = mmFileZip_Get(p, pathname);
    if (NULL == e)
    {
        e = (struct mmZipDirectory*)mm_malloc(sizeof(struct mmZipDirectory));
        mmZipDirectory_Init(e);
        // cache the entry info.
        mm_rbtree_string_vpt_set(&p->directory_rbtree, pathname, e);
    }
    return e;
}
MM_EXPORT_DISH struct mmZipDirectory* mmFileZip_Get(struct mmFileZip* p, struct mm_string* pathname)
{
    struct mmZipDirectory* e = NULL;

    if (0 == mm_memcmp(pathname->s, "./", 2))
    {
        struct mm_string real_pathname;
        mm_string_init(&real_pathname);
        mm_string_substr(pathname, &real_pathname, 2, pathname->l - 2);
        e = (struct mmZipDirectory*)mm_rbtree_string_vpt_get(&p->directory_rbtree, &real_pathname);
        mm_string_destroy(&real_pathname);
    }
    else
    {
        e = (struct mmZipDirectory*)mm_rbtree_string_vpt_get(&p->directory_rbtree, pathname);
    }
    return e;
}
MM_EXPORT_DISH struct mmZipDirectory* mmFileZip_GetInstance(struct mmFileZip* p, struct mm_string* pathname)
{
    struct mmZipDirectory* e = mmFileZip_Get(p, pathname);
    if (NULL == e)
    {
        e = mmFileZip_Add(p, pathname);
    }
    return e;
}
MM_EXPORT_DISH void mmFileZip_Rmv(struct mmFileZip* p, struct mm_string* pathname)
{
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmZipDirectory* e = NULL;

    it = mm_rbtree_string_vpt_get_iterator(&p->directory_rbtree, pathname);
    if (NULL != it)
    {
        e = (struct mmZipDirectory*)(it->v);
        mm_rbtree_string_vpt_erase(&p->directory_rbtree, it);
        mmZipDirectory_Destroy(e);
        mm_free(e);
    }
}
MM_EXPORT_DISH void mmFileZip_Clear(struct mmFileZip* p)
{
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmZipDirectory* e = NULL;
    n = mm_rb_first(&p->directory_rbtree.rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmZipDirectory*)(it->v);
        mm_rbtree_string_vpt_erase(&p->directory_rbtree, it);
        mmZipDirectory_Destroy(e);
        mm_free(e);
    }
}

