/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFileContext.h"

#include "core/mm_logger.h"

#include "mmFilePath.h"


static const char* g_AssetsRootFolderName = "__assets_root_folder__";
static const char* g_AssetsRootSourceName = "__assets_root_source__";

static mm_uint32_t __static_FileAssets_AssetsType(struct mmFileAssetsInterface* p)
{
    return 0;
}
static int __static_FileAssets_IsDataExists(struct mmFileAssetsInterface* p, const char* real_name)
{
    return 0;
}
static void __static_FileAssets_FileInfoName(struct mmFileAssetsInterface* p, struct mm_string* file_info_name, const char* file_name)
{

}
static void __static_FileAssets_PathNameToRealName(struct mmFileAssetsInterface* p, struct mm_string* real_name, const char* path_name)
{

}
static void __static_FileAssets_RealNameToPathName(struct mmFileAssetsInterface* p, struct mm_string* path_name, const char* real_name)
{

}
static void __static_FileAssets_AcquireFileByteBuffer(struct mmFileAssetsInterface* p, const char* real_name, struct mm_byte_buffer* byte_buffer)
{

}
static void __static_FileAssets_ReleaseFileByteBuffer(struct mmFileAssetsInterface* p, struct mm_byte_buffer* byte_buffer)
{

}
static void __static_FileAssets_AcquireFindFiles(struct mmFileAssetsInterface* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{

}
static void __static_FileAssets_ReleaseFindFiles(struct mmFileAssetsInterface* p, struct mm_rbtree_string_vpt* rbtree)
{

}
MM_EXPORT_DISH void mmFileAssetsInterface_Init(struct mmFileAssetsInterface* p)
{
    p->AssetsType = &__static_FileAssets_AssetsType;
    p->IsDataExists = &__static_FileAssets_IsDataExists;
    p->FileInfoName = &__static_FileAssets_FileInfoName;
    p->PathNameToRealName = &__static_FileAssets_PathNameToRealName;
    p->RealNameToPathName = &__static_FileAssets_RealNameToPathName;
    p->AcquireFileByteBuffer = &__static_FileAssets_AcquireFileByteBuffer;
    p->ReleaseFileByteBuffer = &__static_FileAssets_ReleaseFileByteBuffer;
    p->AcquireFindFiles = &__static_FileAssets_AcquireFindFiles;
    p->ReleaseFindFiles = &__static_FileAssets_ReleaseFindFiles;
}
MM_EXPORT_DISH void mmFileAssetsInterface_Destroy(struct mmFileAssetsInterface* p)
{
    p->AssetsType = &__static_FileAssets_AssetsType;
    p->IsDataExists = &__static_FileAssets_IsDataExists;
    p->FileInfoName = &__static_FileAssets_FileInfoName;
    p->PathNameToRealName = &__static_FileAssets_PathNameToRealName;
    p->RealNameToPathName = &__static_FileAssets_RealNameToPathName;
    p->AcquireFileByteBuffer = &__static_FileAssets_AcquireFileByteBuffer;
    p->ReleaseFileByteBuffer = &__static_FileAssets_ReleaseFileByteBuffer;
    p->AcquireFindFiles = &__static_FileAssets_AcquireFindFiles;
    p->ReleaseFindFiles = &__static_FileAssets_ReleaseFindFiles;
}

MM_EXPORT_DISH void mmFileAssetsFolder_Init(struct mmFileAssetsFolder* p)
{
    mmFileAssetsInterface_Init(&p->hSuper);

    mm_string_init(&p->assets_name);
    mm_string_init(&p->assets_path);
    mm_string_init(&p->assets_base);
    //
    p->hSuper.AssetsType = &mmFileAssetsFolder_OnAssetsType;
    p->hSuper.IsDataExists = &mmFileAssetsFolder_OnIsDataExists;
    p->hSuper.FileInfoName = &mmFileAssetsFolder_OnFileInfoName;
    p->hSuper.PathNameToRealName = &mmFileAssetsFolder_OnPathNameToRealName;
    p->hSuper.RealNameToPathName = &mmFileAssetsFolder_OnRealNameToPathName;
    p->hSuper.AcquireFileByteBuffer = &mmFileAssetsFolder_OnAcquireFileByteBuffer;
    p->hSuper.ReleaseFileByteBuffer = &mmFileAssetsFolder_OnReleaseFileByteBuffer;
    p->hSuper.AcquireFindFiles = &mmFileAssetsFolder_OnAcquireFindFiles;
    p->hSuper.ReleaseFindFiles = &mmFileAssetsFolder_OnReleaseFindFiles;
}
MM_EXPORT_DISH void mmFileAssetsFolder_Destroy(struct mmFileAssetsFolder* p)
{
    mmFileAssetsFolder_ReleaseEntry(p);
    //
    mmFileAssetsInterface_Destroy(&p->hSuper);

    mm_string_destroy(&p->assets_name);
    mm_string_destroy(&p->assets_path);
    mm_string_destroy(&p->assets_base);
}

MM_EXPORT_DISH void mmFileAssetsFolder_SetAssets(struct mmFileAssetsFolder* p, const char* name, const char* path, const char* base)
{
    mm_string_assigns(&p->assets_name, name);
    mm_string_assigns(&p->assets_path, path);
    mm_string_assigns(&p->assets_base, base);
    mmDirectoryNoneSuffix(&p->assets_path, p->assets_path.s);
}
MM_EXPORT_DISH void mmFileAssetsFolder_AcquireEntry(struct mmFileAssetsFolder* p)
{
    // need do nothing.
}
MM_EXPORT_DISH void mmFileAssetsFolder_ReleaseEntry(struct mmFileAssetsFolder* p)
{
    // need do nothing.
}

// virtual function for super.
MM_EXPORT_DISH mm_uint32_t mmFileAssetsFolder_OnAssetsType(struct mmFileAssetsInterface* pSuper)
{
    return MM_FILE_ASSETS_TYPE_FOLDER;
}
MM_EXPORT_DISH int mmFileAssetsFolder_OnIsDataExists(struct mmFileAssetsInterface* pSuper, const char* real_name)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    return mmFileAssetsFolder_IsDataExists(impl, real_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnFileInfoName(struct mmFileAssetsInterface* pSuper, struct mm_string* file_info_name, const char* file_name)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_FileInfoName(impl, file_info_name, file_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnPathNameToRealName(struct mmFileAssetsInterface* pSuper, struct mm_string* real_name, const char* path_name)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_PathNameToRealName(impl, real_name, path_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnRealNameToPathName(struct mmFileAssetsInterface* pSuper, struct mm_string* path_name, const char* real_name)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_RealNameToPathName(impl, path_name, real_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnAcquireFileByteBuffer(struct mmFileAssetsInterface* pSuper, const char* real_name, struct mm_byte_buffer* byte_buffer)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_AcquireFileByteBuffer(impl, real_name, byte_buffer);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnReleaseFileByteBuffer(struct mmFileAssetsInterface* pSuper, struct mm_byte_buffer* byte_buffer)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_ReleaseFileByteBuffer(impl, byte_buffer);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnAcquireFindFiles(struct mmFileAssetsInterface* pSuper, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_AcquireFindFiles(impl, directory, pattern, recursive, dirs, ignore_hidden, rbtree);
}
MM_EXPORT_DISH void mmFileAssetsFolder_OnReleaseFindFiles(struct mmFileAssetsInterface* pSuper, struct mm_rbtree_string_vpt* rbtree)
{
    struct mmFileAssetsFolder* impl = (struct mmFileAssetsFolder*)(pSuper);
    mmFileAssetsFolder_ReleaseFindFiles(impl, rbtree);
}

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH int mmFileAssetsFolder_IsDataExists(struct mmFileAssetsFolder* p, const char* real_name)
{
    return mm_absolute_path_is_data_exists(real_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_FileInfoName(struct mmFileAssetsFolder* p, struct mm_string* file_info_name, const char* file_name)
{
    mm_string_assigns(file_info_name, "folder");
    mm_string_append(file_info_name, " ");
    mm_string_append(file_info_name, p->assets_name.s);
    mm_string_append(file_info_name, " ");
    mm_string_append(file_info_name, file_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_PathNameToRealName(struct mmFileAssetsFolder* p, struct mm_string* real_name, const char* path_name)
{
    struct mm_string path;
    mm_string_init(&path);

    if (!((0 == p->assets_path.l) || 0 == mm_string_compare_c_str(&p->assets_path, ".")))
    {
        mm_string_append(&path, p->assets_path.s);
        mm_string_append(&path, "/");
    }
    if (!((0 == p->assets_base.l) || 0 == mm_string_compare_c_str(&p->assets_base, ".")))
    {
        mm_string_append(&path, p->assets_base.s);
        mm_string_append(&path, "/");
    }
    mm_string_append(&path, path_name);

    mmCleanPath(&path, path.s);
    mm_string_assign(real_name, &path);
    mm_string_destroy(&path);
}
MM_EXPORT_DISH void mmFileAssetsFolder_RealNameToPathName(struct mmFileAssetsFolder* p, struct mm_string* path_name, const char* real_name)
{
    struct mm_string prefix;
    struct mm_string path;
    mm_string_init(&prefix);
    mm_string_init(&path);
    mm_string_assigns(&path, real_name);

    if (!((0 == p->assets_path.l) || 0 == mm_string_compare_c_str(&p->assets_path, ".")))
    {
        mm_string_assign(&prefix, &p->assets_path);
        mm_string_append(&prefix, "/");
        mm_string_substr(&path, &path, prefix.l, path.l);
    }
    if (!((0 == p->assets_base.l) || 0 == mm_string_compare_c_str(&p->assets_base, ".")))
    {
        mm_string_assign(&prefix, &p->assets_base);
        mm_string_append(&prefix, "/");
        mm_string_substr(&path, &path, prefix.l, path.l);
    }

    mmCleanPath(&path, path.s);
    mm_string_assign(path_name, &path);
    mm_string_destroy(&prefix);
    mm_string_destroy(&path);
}
// acquire file buffer.
MM_EXPORT_DISH void mmFileAssetsFolder_AcquireFileByteBuffer(struct mmFileAssetsFolder* p, const char* real_name, struct mm_byte_buffer* byte_buffer)
{
    mmAbsolutePathAcquireFileByteBuffer(real_name, byte_buffer);
}
// release file buffer.
MM_EXPORT_DISH void mmFileAssetsFolder_ReleaseFileByteBuffer(struct mmFileAssetsFolder* p, struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_free(byte_buffer);
}

MM_EXPORT_DISH void mmFileAssetsFolder_AcquireFindFiles(struct mmFileAssetsFolder* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{
    size_t pos1 = 0;
    size_t pos2 = 0;
    struct mmFileAssetsInfo* fi = NULL;
    struct mm_string unit_name;
    struct mm_string pattern_string;
    struct mm_string directory_string;
    struct mm_string full_pattern;
    struct mm_string file_info_name;

    intptr_t data_handle, res;
    struct _finddata_t find_data;

    mm_string_init(&unit_name);
    mm_string_init(&pattern_string);
    mm_string_init(&directory_string);
    mm_string_init(&full_pattern);
    mm_string_init(&file_info_name);

    mm_string_assigns(&pattern_string, pattern);
    mmFileAssetsFolder_PathNameToRealName(p, &unit_name, directory);

    // pattern can contain a directory name, separate it from mask
    pos1 = mm_string_find_last_of(&pattern_string, '/');
    pos2 = mm_string_find_last_of(&pattern_string, '\\');
    if (MM_STRING_INVALID_POS == pos1 || ((MM_STRING_INVALID_POS != pos2) && (pos1 < pos2)))
    {
        pos1 = pos2;
    }
    if (MM_STRING_INVALID_POS != pos1)
    {
        mm_string_substr(&pattern_string, &directory_string, 0, pos1 + 1);
    }
    // concatenate
    mmConcatenatePath(&full_pattern, unit_name.s, pattern);
    //
    data_handle = mm_findfirst(full_pattern.s, &find_data);
    res = 0;
    while (data_handle != -1 && res != -1)
    {
        if ((dirs == ((find_data.attrib & _A_SUBDIR) != 0)) &&
            (0 == ignore_hidden || (find_data.attrib & _A_HIDDEN) == 0) &&
            (0 == dirs || 0 == mm_is_reserved_directory(find_data.name)))
        {
            fi = (struct mmFileAssetsInfo*)mm_malloc(sizeof(struct mmFileAssetsInfo));
            mmFileAssetsInfo_Init(fi);
            mm_string_assigns(&fi->filename, directory_string.s);
            mm_string_append(&fi->filename, find_data.name);
            mm_string_assigns(&fi->basename, find_data.name);
            mm_string_assigns(&fi->pathname, directory_string.s);
            fi->csize = find_data.size;
            fi->dsize = find_data.size;
            fi->file_assets.type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
            fi->file_assets.file_assets = (struct mmFileAssetsInterface*)p;
            mmFileAssetsFolder_FileInfoName(p, &file_info_name, fi->filename.s);
            mm_rbtree_string_vpt_set(rbtree, &file_info_name, fi);
        }
        res = mm_findnext(data_handle, &find_data);
    }
    // Close if we found any files
    if (data_handle != -1)
    {
        mm_findclose(data_handle);
    }

    // Now find directories
    if (0 != recursive)
    {
        struct mm_string base_dir;
        struct mm_string mask;
        struct mm_string pattern_substr;
        mm_string_init(&base_dir);
        mm_string_init(&mask);
        mm_string_init(&pattern_substr);
        mm_string_assigns(&base_dir, 0 == unit_name.l ? "." : unit_name.s);
        if (!mm_string_empty(&directory_string))
        {
            mmConcatenatePath(&base_dir, base_dir.s, directory_string.s);
            // Remove the last '/'
            if ('/' == base_dir.s[base_dir.l - 1] || '\\' == base_dir.s[base_dir.l - 1])
            {
                base_dir.s[base_dir.l - 1] = 0;
                base_dir.l--;
            }
        }
        mm_string_append(&base_dir, "/*");
        //
        // Remove directory name from pattern
        mm_string_assigns(&mask, "/");
        if (MM_STRING_INVALID_POS != pos1)
        {
            mm_string_substr(&pattern_string, &pattern_substr, pos1 + 1, pattern_string.l);
            mm_string_append(&mask, pattern_substr.s);
        }
        else
        {
            mm_string_append(&mask, pattern);
        }

        data_handle = mm_findfirst(base_dir.s, &find_data);
        res = 0;
        while (data_handle != -1 && res != -1)
        {
            if ((find_data.attrib & _A_SUBDIR) &&
                (0 == ignore_hidden || (find_data.attrib & _A_HIDDEN) == 0) &&
                (0 == mm_is_reserved_directory(find_data.name)))
            {
                // recurse
                mm_string_assign(&base_dir, &directory_string);
                mm_string_append(&base_dir, find_data.name);
                mm_string_append(&base_dir, mask.s);
                mmFileAssetsFolder_AcquireFindFiles(p, directory, base_dir.s, recursive, dirs, ignore_hidden, rbtree);
            }
            res = mm_findnext(data_handle, &find_data);
        }
        // Close if we found any files
        if (data_handle != -1)
        {
            mm_findclose(data_handle);
        }
        //
        mm_string_destroy(&base_dir);
        mm_string_destroy(&mask);
        mm_string_destroy(&pattern_substr);
    }
    mm_string_destroy(&unit_name);
    mm_string_destroy(&pattern_string);
    mm_string_destroy(&directory_string);
    mm_string_destroy(&full_pattern);
    mm_string_destroy(&file_info_name);
}
MM_EXPORT_DISH void mmFileAssetsFolder_ReleaseFindFiles(struct mmFileAssetsFolder* p, struct mm_rbtree_string_vpt* rbtree)
{
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmFileAssetsInfo* e = NULL;
    n = mm_rb_first(&rbtree->rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsInfo*)(it->v);
        mm_rbtree_string_vpt_erase(rbtree, it);
        mmFileAssetsInfo_Destroy(e);
        mm_free(e);
    }
}

MM_EXPORT_DISH void mmFileAssetsSource_Init(struct mmFileAssetsSource* p)
{
    mmFileAssetsInterface_Init(&p->hSuper);

    mm_string_init(&p->assets_name);
    mm_string_init(&p->assets_path);
    mm_string_init(&p->assets_base);
    mmFileZip_Init(&p->zip_file);
    //
    p->hSuper.AssetsType = &mmFileAssetsSource_OnAssetsType;
    p->hSuper.IsDataExists = &mmFileAssetsSource_OnIsDataExists;
    p->hSuper.FileInfoName = &mmFileAssetsSource_OnFileInfoName;
    p->hSuper.PathNameToRealName = &mmFileAssetsSource_OnPathNameToRealName;
    p->hSuper.RealNameToPathName = &mmFileAssetsSource_OnRealNameToPathName;
    p->hSuper.AcquireFileByteBuffer = &mmFileAssetsSource_OnAcquireFileByteBuffer;
    p->hSuper.ReleaseFileByteBuffer = &mmFileAssetsSource_OnReleaseFileByteBuffer;
    p->hSuper.AcquireFindFiles = &mmFileAssetsSource_OnAcquireFindFiles;
    p->hSuper.ReleaseFindFiles = &mmFileAssetsSource_OnReleaseFindFiles;
}
MM_EXPORT_DISH void mmFileAssetsSource_Destroy(struct mmFileAssetsSource* p)
{
    mmFileAssetsSource_ReleaseEntry(p);
    //
    mmFileAssetsInterface_Destroy(&p->hSuper);

    mm_string_destroy(&p->assets_name);
    mm_string_destroy(&p->assets_path);
    mm_string_destroy(&p->assets_base);
    mmFileZip_Destroy(&p->zip_file);
}

MM_EXPORT_DISH void mmFileAssetsSource_SetAssets(struct mmFileAssetsSource* p, const char* name, const char* path, const char* base)
{
    mm_string_assigns(&p->assets_name, name);
    mm_string_assigns(&p->assets_path, path);
    mm_string_assigns(&p->assets_base, base);
    mmDirectoryNoneSuffix(&p->assets_path, p->assets_path.s);
    //
    mmFileZip_SetFilename(&p->zip_file, path);
    mmFileZip_SetFilter(&p->zip_file, base);
}
MM_EXPORT_DISH void mmFileAssetsSource_AcquireEntry(struct mmFileAssetsSource* p)
{
    mmFileZip_Fopen(&p->zip_file);
    mmFileZip_AcquireAnalysis(&p->zip_file);
}
MM_EXPORT_DISH void mmFileAssetsSource_ReleaseEntry(struct mmFileAssetsSource* p)
{
    mmFileZip_ReleaseAnalysis(&p->zip_file);
    mmFileZip_Close(&p->zip_file);
}

// virtual function for super.
MM_EXPORT_DISH mm_uint32_t mmFileAssetsSource_OnAssetsType(struct mmFileAssetsInterface* pSuper)
{
    return MM_FILE_ASSETS_TYPE_SOURCE;
}
MM_EXPORT_DISH int mmFileAssetsSource_OnIsDataExists(struct mmFileAssetsInterface* pSuper, const char* real_name)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    return mmFileAssetsSource_IsDataExists(impl, real_name);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnFileInfoName(struct mmFileAssetsInterface* pSuper, struct mm_string* file_info_name, const char* file_name)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_FileInfoName(impl, file_info_name, file_name);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnPathNameToRealName(struct mmFileAssetsInterface* pSuper, struct mm_string* real_name, const char* path_name)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_PathNameToRealName(impl, real_name, path_name);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnRealNameToPathName(struct mmFileAssetsInterface* pSuper, struct mm_string* path_name, const char* real_name)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_RealNameToPathName(impl, path_name, real_name);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnAcquireFileByteBuffer(struct mmFileAssetsInterface* pSuper, const char* real_name, struct mm_byte_buffer* byte_buffer)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_AcquireFileByteBuffer(impl, real_name, byte_buffer);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnReleaseFileByteBuffer(struct mmFileAssetsInterface* pSuper, struct mm_byte_buffer* byte_buffer)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_ReleaseFileByteBuffer(impl, byte_buffer);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnAcquireFindFiles(struct mmFileAssetsInterface* pSuper, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_AcquireFindFiles(impl, directory, pattern, recursive, dirs, ignore_hidden, rbtree);
}
MM_EXPORT_DISH void mmFileAssetsSource_OnReleaseFindFiles(struct mmFileAssetsInterface* pSuper, struct mm_rbtree_string_vpt* rbtree)
{
    struct mmFileAssetsSource* impl = (struct mmFileAssetsSource*)(pSuper);
    mmFileAssetsSource_ReleaseFindFiles(impl, rbtree);
}

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH int mmFileAssetsSource_IsDataExists(struct mmFileAssetsSource* p, const char* real_name)
{
    return mmFileZip_Exists(&p->zip_file, real_name);
}
MM_EXPORT_DISH void mmFileAssetsSource_FileInfoName(struct mmFileAssetsSource* p, struct mm_string* file_info_name, const char* file_name)
{
    mm_string_assigns(file_info_name, "source");
    mm_string_append(file_info_name, " ");
    mm_string_append(file_info_name, p->assets_name.s);
    mm_string_append(file_info_name, " ");
    mm_string_append(file_info_name, file_name);
}
MM_EXPORT_DISH void mmFileAssetsSource_PathNameToRealName(struct mmFileAssetsSource* p, struct mm_string* real_name, const char* path_name)
{
    struct mm_string path;
    mm_string_init(&path);

    if (!((0 == p->assets_base.l) || 0 == mm_string_compare_c_str(&p->assets_base, ".")))
    {
        mm_string_append(&path, p->assets_base.s);
        mm_string_append(&path, "/");
    }
    mm_string_append(&path, path_name);

    mmCleanPath(&path, path.s);
    mm_string_assign(real_name, &path);
    mm_string_destroy(&path);
}
MM_EXPORT_DISH void mmFileAssetsSource_RealNameToPathName(struct mmFileAssetsSource* p, struct mm_string* path_name, const char* real_name)
{
    struct mm_string path;
    struct mm_string prefix;
    mm_string_init(&path);
    mm_string_init(&prefix);
    mm_string_assigns(&path, real_name);
    if (!((0 == p->assets_base.l) || 0 == mm_string_compare_c_str(&p->assets_base, ".")))
    {
        mm_string_assign(&prefix, &p->assets_base);
        mm_string_append(&prefix, "/");
        mm_string_substr(&path, &path, prefix.l, path.l);
    }
    mmCleanPath(&path, path.s);
    mm_string_assign(path_name, &path);
    mm_string_destroy(&path);
    mm_string_destroy(&prefix);
}
// acquire file buffer.
MM_EXPORT_DISH void mmFileAssetsSource_AcquireFileByteBuffer(struct mmFileAssetsSource* p, const char* real_name, struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_reset(byte_buffer);
    //
    if (NULL != real_name)
    {
        mmFileZip_AcquireFileByteBuffer(&p->zip_file, real_name, byte_buffer);
    }
}
// release file buffer.
MM_EXPORT_DISH void mmFileAssetsSource_ReleaseFileByteBuffer(struct mmFileAssetsSource* p, struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_free(byte_buffer);
}

MM_EXPORT_DISH void mmFileAssetsSource_AcquireFindFiles(struct mmFileAssetsSource* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{
    size_t pos1 = 0;
    size_t pos2 = 0;
    struct mmFileAssetsInfo* fi = NULL;
    struct mm_string unit_name;
    struct mm_string pattern_string;
    struct mm_string directory_string;
    struct mm_string full_pattern;
    struct mm_string file_info_name;

    struct mm_string path;
    struct mm_string path_substr;
    struct mm_string basename;
    struct mm_string pathname;
    struct mmZipDirectory* zip_directory = NULL;
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmZipEntry* e = NULL;
    int is_local_directory = 0;

    mm_string_init(&unit_name);
    mm_string_init(&pattern_string);
    mm_string_init(&directory_string);
    mm_string_init(&full_pattern);
    mm_string_init(&file_info_name);

    mm_string_init(&path);
    mm_string_init(&path_substr);
    mm_string_init(&basename);
    mm_string_init(&pathname);

    mm_string_assigns(&pattern_string, pattern);
    mmFileAssetsSource_PathNameToRealName(p, &unit_name, directory);

    // pattern can contain a directory name, separate it from mask
    pos1 = mm_string_find_last_of(&pattern_string, '/');
    pos2 = mm_string_find_last_of(&pattern_string, '\\');
    if (MM_STRING_INVALID_POS == pos1 || ((MM_STRING_INVALID_POS != pos2) && (pos1 < pos2)))
    {
        pos1 = pos2;
    }
    if (MM_STRING_INVALID_POS != pos1)
    {
        mm_string_substr(&pattern_string, &directory_string, 0, pos1 + 1);
    }
    // concatenate
    mmConcatenatePath(&full_pattern, unit_name.s, pattern);

    do
    {
        mmPathSplitFileName(&full_pattern, &basename, &pathname);

        mmDirectoryNoneSuffix(&pathname, pathname.s);
        mm_string_append(&pathname, "/");

        zip_directory = mmFileZip_Get(&p->zip_file, &pathname);
        if (NULL == zip_directory)
        {
            // zip directory is not exist.
            break;
        }

        if (0 == mm_string_compare_c_str(&unit_name, "."))
        {
            is_local_directory = 1;
            // remove the "./"
            mm_string_substr(&full_pattern, &full_pattern, 2, full_pattern.l - 2);
        }

        n = mm_rb_first(&zip_directory->entry_rbtree.rbt);
        while (NULL != n)
        {
            it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
            n = mm_rb_next(n);
            e = (struct mmZipEntry*)(it->v);
            mm_string_assign(&path, &e->fullname);
            mm_string_substr(&path, &path_substr, 0, unit_name.l);
            if ((!mm_string_empty(&e->fullname)) &&
                (0 == mm_string_compare(&path_substr, &unit_name) || 1 == is_local_directory) &&
                (mm_path_match(e->fullname.s, full_pattern.s)) &&
                (dirs == (0 != mmZipEntry_GetIsDirectory(e))) &&
                (0 == dirs || 0 == mm_is_reserved_directory(e->fullname.s)))
            {
                fi = (struct mmFileAssetsInfo*)mm_malloc(sizeof(struct mmFileAssetsInfo));
                mmFileAssetsInfo_Init(fi);
                mmFileAssetsSource_RealNameToPathName(p, &fi->filename, e->fullname.s);
                mmDirectoryRemovePrefixPath(&fi->filename, fi->filename.s, directory);
                mmPathSplitFileName(&fi->filename, &fi->basename, &fi->pathname);
                fi->csize = e->csize;
                fi->dsize = e->dsize;
                fi->file_assets.type_assets = MM_FILE_ASSETS_TYPE_SOURCE;
                fi->file_assets.file_assets = (struct mmFileAssetsInterface*)p;
                mmFileAssetsSource_FileInfoName(p, &file_info_name, fi->filename.s);
                mm_rbtree_string_vpt_set(rbtree, &file_info_name, fi);
            }
        }
        // Now find directories
        if (0 != recursive)
        {
            struct mm_string base_dir;
            struct mm_string mask;
            struct mm_string pattern_substr;
            mm_string_init(&base_dir);
            mm_string_init(&mask);
            mm_string_init(&pattern_substr);
            mm_string_assigns(&base_dir, 0 == unit_name.l ? "." : unit_name.s);
            if (!mm_string_empty(&directory_string))
            {
                mmConcatenatePath(&base_dir, base_dir.s, directory_string.s);
                // Remove the last '/'
                if ('/' == base_dir.s[base_dir.l - 1] || '\\' == base_dir.s[base_dir.l - 1])
                {
                    base_dir.s[base_dir.l - 1] = 0;
                    base_dir.l--;
                }
            }
            mm_string_append(&base_dir, "/*");
            //
            // Remove directory name from pattern
            mm_string_assigns(&mask, "/");
            if (MM_STRING_INVALID_POS != pos1)
            {
                mm_string_substr(&pattern_string, &pattern_substr, pos1 + 1, pattern_string.l);
                mm_string_append(&mask, pattern_substr.s);
            }
            else
            {
                mm_string_append(&mask, pattern);
            }

            n = mm_rb_first(&zip_directory->entry_rbtree.rbt);
            while (NULL != n)
            {
                it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
                n = mm_rb_next(n);
                e = (struct mmZipEntry*)(it->v);
                mm_string_assigns(&path, e->fullname.s);
                mm_string_substr(&path, &path_substr, 0, unit_name.l);
                if ((0 == mm_string_compare(&path_substr, &unit_name) || 1 == is_local_directory) &&
                    (0 != mmZipEntry_GetIsDirectory(e)) &&
                    (0 == mm_is_reserved_directory(e->fullname.s)) &&
                    (!mm_string_empty(&e->basename)))
                {
                    // recurse
                    mm_string_assign(&base_dir, &directory_string);
                    mm_string_append(&base_dir, e->basename.s);
                    mm_string_append(&base_dir, mask.s);
                    mmFileAssetsSource_AcquireFindFiles(p, directory, base_dir.s, recursive, dirs, ignore_hidden, rbtree);
                }
            }
            //
            mm_string_destroy(&base_dir);
            mm_string_destroy(&mask);
            mm_string_destroy(&pattern_substr);
        }
    } while (0);

    mm_string_destroy(&unit_name);
    mm_string_destroy(&pattern_string);
    mm_string_destroy(&directory_string);
    mm_string_destroy(&full_pattern);
    mm_string_destroy(&file_info_name);

    mm_string_destroy(&path);
    mm_string_destroy(&path_substr);
    mm_string_destroy(&basename);
    mm_string_destroy(&pathname);
}
MM_EXPORT_DISH void mmFileAssetsSource_ReleaseFindFiles(struct mmFileAssetsSource* p, struct mm_rbtree_string_vpt* rbtree)
{
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmFileAssetsInfo* e = NULL;
    n = mm_rb_first(&rbtree->rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsInfo*)(it->v);
        mm_rbtree_string_vpt_erase(rbtree, it);
        mmFileAssetsInfo_Destroy(e);
        mm_free(e);
    }
}

static const struct mmFileAssetsFolder g_FileAssetsFolderAbsolutePath =
{
    {
        &mmFileAssetsFolder_OnAssetsType,
        &mmFileAssetsFolder_OnIsDataExists,
        &mmFileAssetsFolder_OnFileInfoName,
        &mmFileAssetsFolder_OnPathNameToRealName,
        &mmFileAssetsFolder_OnRealNameToPathName,
        &mmFileAssetsFolder_OnAcquireFileByteBuffer,
        &mmFileAssetsFolder_OnReleaseFileByteBuffer,
        &mmFileAssetsFolder_OnAcquireFindFiles,
        &mmFileAssetsFolder_OnReleaseFindFiles,
    },
    mm_string_make(""),
    mm_string_make(""),
    mm_string_make(""),
};

MM_EXPORT_DISH void mmFileAssetsCacher_Init(struct mmFileAssetsCacher* p)
{
    p->type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
    p->file_assets = NULL;
}
MM_EXPORT_DISH void mmFileAssetsCacher_Destroy(struct mmFileAssetsCacher* p)
{
    p->type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
    p->file_assets = NULL;
}

// copy q to p.
MM_EXPORT_DISH void mmFileAssetsCacher_Copy(struct mmFileAssetsCacher* p, struct mmFileAssetsCacher* q)
{
    p->type_assets = q->type_assets;
    p->file_assets = q->file_assets;
}

MM_EXPORT_DISH mm_uint32_t mmFileAssetsCacher_AssetsType(struct mmFileAssetsCacher* p)
{
    return (*(p->file_assets->AssetsType))(p->file_assets);
}
MM_EXPORT_DISH int mmFileAssetsCacher_IsDataExists(struct mmFileAssetsCacher* p, const char* real_name)
{
    return (*(p->file_assets->IsDataExists))(p->file_assets, real_name);
}
MM_EXPORT_DISH void mmFileAssetsCacher_FileInfoName(struct mmFileAssetsCacher* p, struct mm_string* file_info_name, const char* file_name)
{
    (*(p->file_assets->FileInfoName))(p->file_assets, file_info_name, file_name);
}
MM_EXPORT_DISH void mmFileAssetsCacher_PathNameToRealName(struct mmFileAssetsCacher* p, struct mm_string* real_name, const char* path_name)
{
    (*(p->file_assets->PathNameToRealName))(p->file_assets, real_name, path_name);
}
MM_EXPORT_DISH void mmFileAssetsCacher_RealNameToPathName(struct mmFileAssetsCacher* p, struct mm_string* path_name, const char* real_name)
{
    (*(p->file_assets->RealNameToPathName))(p->file_assets, path_name, real_name);
}
MM_EXPORT_DISH void mmFileAssetsCacher_AcquireFileByteBuffer(struct mmFileAssetsCacher* p, const char* real_name, struct mm_byte_buffer* byte_buffer)
{
    (*(p->file_assets->AcquireFileByteBuffer))(p->file_assets, real_name, byte_buffer);
}
MM_EXPORT_DISH void mmFileAssetsCacher_ReleaseFileByteBuffer(struct mmFileAssetsCacher* p, struct mm_byte_buffer* byte_buffer)
{
    (*(p->file_assets->ReleaseFileByteBuffer))(p->file_assets, byte_buffer);
}
MM_EXPORT_DISH void mmFileAssetsCacher_AcquireFindFiles(struct mmFileAssetsCacher* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{
    (*(p->file_assets->AcquireFindFiles))(p->file_assets, directory, pattern, recursive, dirs, ignore_hidden, rbtree);
}
MM_EXPORT_DISH void mmFileAssetsCacher_ReleaseFindFiles(struct mmFileAssetsCacher* p, struct mm_rbtree_string_vpt* rbtree)
{
    (*(p->file_assets->ReleaseFindFiles))(p->file_assets, rbtree);
}

MM_EXPORT_DISH void mmFileAssetsInfo_Init(struct mmFileAssetsInfo* p)
{
    mmFileAssetsCacher_Init(&p->file_assets);
    mm_string_init(&p->filename);
    mm_string_init(&p->basename);
    mm_string_init(&p->pathname);
    p->csize = 0;
    p->dsize = 0;
}
MM_EXPORT_DISH void mmFileAssetsInfo_Destroy(struct mmFileAssetsInfo* p)
{
    mmFileAssetsCacher_Destroy(&p->file_assets);
    mm_string_destroy(&p->filename);
    mm_string_destroy(&p->basename);
    mm_string_destroy(&p->pathname);
    p->csize = 0;
    p->dsize = 0;
}

MM_EXPORT_DISH void mmFileContext_Init(struct mmFileContext* p)
{
    struct mm_rbtree_string_vpt_alloc rbtree_string_vpt_alloc;

    mm_string_init(&p->assets_root_folder_path);
    mm_string_init(&p->assets_root_folder_base);
    mm_string_init(&p->assets_root_source_path);
    mm_string_init(&p->assets_root_source_base);
    mm_rbtree_string_vpt_init(&p->assets_folder);
    mm_rbtree_string_vpt_init(&p->assets_source);
    mm_rbtree_string_vpt_init(&p->assets_cacher);

    mm_string_assigns(&p->assets_root_folder_path, ".");
    mm_string_assigns(&p->assets_root_source_path, "");

    rbtree_string_vpt_alloc.alloc = &mm_rbtree_string_vpt_weak_alloc;
    rbtree_string_vpt_alloc.relax = &mm_rbtree_string_vpt_weak_relax;
    rbtree_string_vpt_alloc.obj = p;
    mm_rbtree_string_vpt_assign_alloc(&p->assets_folder, &rbtree_string_vpt_alloc);
    mm_rbtree_string_vpt_assign_alloc(&p->assets_source, &rbtree_string_vpt_alloc);
    mm_rbtree_string_vpt_assign_alloc(&p->assets_cacher, &rbtree_string_vpt_alloc);
}
MM_EXPORT_DISH void mmFileContext_Destroy(struct mmFileContext* p)
{
    mmFileContext_ClearAssetsCacher(p);
    mmFileContext_ClearAssetsFolder(p);
    mmFileContext_ClearAssetsSource(p);
    //
    mm_string_destroy(&p->assets_root_folder_path);
    mm_string_destroy(&p->assets_root_folder_base);
    mm_string_destroy(&p->assets_root_source_path);
    mm_string_destroy(&p->assets_root_source_base);
    mm_rbtree_string_vpt_destroy(&p->assets_folder);
    mm_rbtree_string_vpt_destroy(&p->assets_source);
    mm_rbtree_string_vpt_destroy(&p->assets_cacher);
}

MM_EXPORT_DISH void mmFileContext_SetAssetsRootFolder(struct mmFileContext* p, const char* root_folder, const char* root_base)
{
    mm_string_assigns(&p->assets_root_folder_path, root_folder);
    mmDirectoryNoneSuffix(&p->assets_root_folder_path, p->assets_root_folder_path.s);
    //
    mm_string_assigns(&p->assets_root_folder_base, root_base);
    mmDirectoryNoneSuffix(&p->assets_root_folder_base, p->assets_root_folder_base.s);
    //
    mmFileContext_RmvAssetsFolder(p, g_AssetsRootFolderName);
    mmFileContext_AddAssetsFolder(p, g_AssetsRootFolderName, root_folder, root_base);
}
MM_EXPORT_DISH void mmFileContext_SetAssetsRootSource(struct mmFileContext* p, const char* root_source, const char* root_base)
{
    mm_string_assigns(&p->assets_root_source_path, root_source);
    mmDirectoryNoneSuffix(&p->assets_root_source_path, p->assets_root_source_path.s);
    //
    mm_string_assigns(&p->assets_root_source_base, root_base);
    mmDirectoryNoneSuffix(&p->assets_root_source_base, p->assets_root_source_base.s);
    //
    mmFileContext_RmvAssetsSource(p, g_AssetsRootSourceName);
    mmFileContext_AddAssetsSource(p, g_AssetsRootSourceName, root_source, root_base);
}

MM_EXPORT_DISH struct mmFileAssetsFolder* mmFileContext_AddAssetsFolder(struct mmFileContext* p, const char* name, const char* path, const char* base)
{
    struct mmFileAssetsFolder* assets = mmFileContext_GetAssetsFolder(p, name);
    if (NULL == assets)
    {
        struct mm_string k;
        assets = (struct mmFileAssetsFolder*)mm_malloc(sizeof(struct mmFileAssetsFolder));
        mm_string_make_weak(&k, name);
        mmFileAssetsFolder_Init(assets);
        mmFileAssetsFolder_SetAssets(assets, name, path, base);
        mmFileAssetsFolder_AcquireEntry(assets);
        mm_rbtree_string_vpt_set(&p->assets_folder, &k, assets);
    }
    return assets;
}
MM_EXPORT_DISH struct mmFileAssetsFolder* mmFileContext_GetAssetsFolder(struct mmFileContext* p, const char* name)
{
    struct mm_string k;
    mm_string_make_weak(&k, name);
    return (struct mmFileAssetsFolder*)mm_rbtree_string_vpt_get(&p->assets_folder, &k);
}
MM_EXPORT_DISH void mmFileContext_RmvAssetsFolder(struct mmFileContext* p, const char* name)
{
    struct mmFileAssetsFolder* e = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mm_string k;
    mm_string_make_weak(&k, name);
    it = mm_rbtree_string_vpt_get_iterator(&p->assets_folder, &k);
    if (NULL != it)
    {
        e = (struct mmFileAssetsFolder*)(it->v);
        mm_rbtree_string_vpt_erase(&p->assets_folder, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsFolder_Destroy(e);
        mm_free(e);
    }
}
MM_EXPORT_DISH void mmFileContext_ClearAssetsFolder(struct mmFileContext* p)
{
    struct mmFileAssetsFolder* e = NULL;
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    n = mm_rb_first(&p->assets_folder.rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsFolder*)(it->v);
        mm_rbtree_string_vpt_erase(&p->assets_folder, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsFolder_Destroy(e);
        mm_free(e);
    }
}

MM_EXPORT_DISH struct mmFileAssetsSource* mmFileContext_AddAssetsSource(struct mmFileContext* p, const char* name, const char* path, const char* base)
{
    struct mmFileAssetsSource* assets = mmFileContext_GetAssetsSource(p, name);
    if (NULL == assets)
    {
        struct mm_string k;
        assets = (struct mmFileAssetsSource*)mm_malloc(sizeof(struct mmFileAssetsSource));
        mm_string_make_weak(&k, name);
        mmFileAssetsSource_Init(assets);
        mmFileAssetsSource_SetAssets(assets, name, path, base);
        mmFileAssetsSource_AcquireEntry(assets);
        mm_rbtree_string_vpt_set(&p->assets_source, &k, assets);
    }
    return assets;
}
MM_EXPORT_DISH struct mmFileAssetsSource* mmFileContext_GetAssetsSource(struct mmFileContext* p, const char* name)
{
    struct mm_string k;
    mm_string_make_weak(&k, name);
    return (struct mmFileAssetsSource*)mm_rbtree_string_vpt_get(&p->assets_source, &k);
}
MM_EXPORT_DISH void mmFileContext_RmvAssetsSource(struct mmFileContext* p, const char* name)
{
    struct mmFileAssetsSource* e = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mm_string k;
    mm_string_make_weak(&k, name);
    it = mm_rbtree_string_vpt_get_iterator(&p->assets_source, &k);
    if (NULL != it)
    {
        e = (struct mmFileAssetsSource*)(it->v);
        mm_rbtree_string_vpt_erase(&p->assets_source, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsSource_Destroy(e);
        mm_free(e);
    }
}
MM_EXPORT_DISH void mmFileContext_ClearAssetsSource(struct mmFileContext* p)
{
    struct mmFileAssetsSource* e = NULL;
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    n = mm_rb_first(&p->assets_source.rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsSource*)(it->v);
        mm_rbtree_string_vpt_erase(&p->assets_source, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsSource_Destroy(e);
        mm_free(e);
    }
}

MM_EXPORT_DISH struct mmFileAssetsCacher* mmFileContext_AddAssetsCacher(struct mmFileContext* p, const char* real_name)
{
    struct mmFileAssetsCacher* assets = mmFileContext_GetAssetsCacher(p, real_name);
    if (NULL == assets)
    {
        struct mm_string k;
        assets = (struct mmFileAssetsCacher*)mm_malloc(sizeof(struct mmFileAssetsCacher));
        mm_string_make_weak(&k, real_name);
        mmFileAssetsCacher_Init(assets);
        mm_rbtree_string_vpt_set(&p->assets_cacher, &k, assets);
    }
    return assets;
}
MM_EXPORT_DISH struct mmFileAssetsCacher* mmFileContext_GetAssetsCacher(struct mmFileContext* p, const char* real_name)
{
    struct mm_string k;
    mm_string_make_weak(&k, real_name);
    return (struct mmFileAssetsCacher*)mm_rbtree_string_vpt_get(&p->assets_cacher, &k);
}
MM_EXPORT_DISH void mmFileContext_RmvAssetsCacher(struct mmFileContext* p, const char* real_name)
{
    struct mmFileAssetsCacher* e = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mm_string k;
    mm_string_make_weak(&k, real_name);
    it = mm_rbtree_string_vpt_get_iterator(&p->assets_cacher, &k);
    if (NULL != it)
    {
        e = (struct mmFileAssetsCacher*)(it->v);
        mm_rbtree_string_vpt_erase(&p->assets_cacher, it);
        mmFileAssetsCacher_Destroy(e);
        mm_free(e);
    }
}
MM_EXPORT_DISH void mmFileContext_ClearAssetsCacher(struct mmFileContext* p)
{
    struct mmFileAssetsCacher* e = NULL;
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    n = mm_rb_first(&p->assets_cacher.rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsCacher*)(it->v);
        mm_rbtree_string_vpt_erase(&p->assets_cacher, it);
        mmFileAssetsCacher_Destroy(e);
        mm_free(e);
    }
}
MM_EXPORT_DISH void mmFileContext_ClearAssetsCacherByPointer(struct mmFileContext* p, void* file_assets)
{
    struct mmFileAssetsCacher* e = NULL;
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    n = mm_rb_first(&p->assets_cacher.rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsCacher*)(it->v);
        if (file_assets == e->file_assets)
        {
            mm_rbtree_string_vpt_erase(&p->assets_cacher, it);
            mmFileAssetsCacher_Destroy(e);
            mm_free(e);
        }
    }
}

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH int mmFileContext_IsFileExists(struct mmFileContext* p, const char* path_name)
{
    int code = 0;
    struct mm_string unit_name;
    struct mm_string real_name;
    struct mmFileAssetsCacher file_assets_cacher;
    mm_string_init(&unit_name);
    mm_string_init(&real_name);
    mmFileAssetsCacher_Init(&file_assets_cacher);
    mmCleanPath(&unit_name, path_name);
    do
    {
        struct mm_rbtree_string_vpt_iterator* it = NULL;
        struct mm_rb_node* n = NULL;
        //
        {
            struct mmFileAssetsCacher* e = mmFileContext_GetAssetsCacher(p, unit_name.s);
            code = NULL != e ? 1 : 0;
            if (NULL != e)
            {
                file_assets_cacher.type_assets = e->type_assets;
                file_assets_cacher.file_assets = e->file_assets;
            }
        }
        if (0 != code) { break; }
        if (0 != mm_is_absolute_path(unit_name.s))
        {
            code = mm_absolute_path_is_data_exists(unit_name.s);
            if (0 != code)
            {
                file_assets_cacher.type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
                file_assets_cacher.file_assets = (struct mmFileAssetsInterface*)&g_FileAssetsFolderAbsolutePath;
            }
        }
        if (0 != code) { break; }
        if (0 != p->assets_folder.size)
        {
            struct mmFileAssetsFolder* e = NULL;
            n = mm_rb_first(&p->assets_folder.rbt);
            while (NULL != n)
            {
                it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
                n = mm_rb_next(n);
                e = (struct mmFileAssetsFolder*)(it->v);
                mmFileAssetsFolder_PathNameToRealName(e, &real_name, unit_name.s);
                code = mmFileAssetsFolder_IsDataExists(e, real_name.s);
                if (0 != code)
                {
                    file_assets_cacher.type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
                    file_assets_cacher.file_assets = (struct mmFileAssetsInterface*)e;
                    break;
                }
            }
        }
        if (0 != code) { break; }
        if (0 != p->assets_source.size)
        {
            struct mmFileAssetsSource* e = NULL;
            n = mm_rb_first(&p->assets_source.rbt);
            while (NULL != n)
            {
                it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
                n = mm_rb_next(n);
                e = (struct mmFileAssetsSource*)(it->v);
                mmFileAssetsSource_PathNameToRealName(e, &real_name, unit_name.s);
                code = mmFileAssetsSource_IsDataExists(e, real_name.s);
                if (0 != code)
                {
                    file_assets_cacher.type_assets = MM_FILE_ASSETS_TYPE_SOURCE;
                    file_assets_cacher.file_assets = (struct mmFileAssetsInterface*)e;
                    break;
                }
            }
        }
    } while (0);
    if (0 != code)
    {
        struct mmFileAssetsCacher* e = mmFileContext_AddAssetsCacher(p, unit_name.s);
        e->type_assets = file_assets_cacher.type_assets;
        e->file_assets = file_assets_cacher.file_assets;
    }
    else
    {
        mmFileContext_RmvAssetsCacher(p, real_name.s);
    }
    mm_string_destroy(&real_name);
    mmFileAssetsCacher_Destroy(&file_assets_cacher);
    return code;
}
// acquire file buffer.
// priority 0 folder 1 source, note: we will first acquire folder file byte buffer.
MM_EXPORT_DISH void mmFileContext_AcquireFileByteBuffer(struct mmFileContext* p, const char* path_name, struct mm_byte_buffer* byte_buffer)
{
    struct mm_string unit_name;
    struct mm_string real_name;
    struct mmFileAssetsCacher file_assets_cacher;
    mm_string_init(&unit_name);
    mm_string_init(&real_name);
    mmFileAssetsCacher_Init(&file_assets_cacher);
    mmCleanPath(&unit_name, path_name);
    mm_byte_buffer_reset(byte_buffer);
    do
    {
        struct mm_rbtree_string_vpt_iterator* it = NULL;
        struct mm_rb_node* n = NULL;
        {
            struct mmFileAssetsCacher* e = mmFileContext_GetAssetsCacher(p, unit_name.s);
            if (NULL != e)
            {
                mmFileAssetsCacher_PathNameToRealName(e, &real_name, unit_name.s);
                mmFileAssetsCacher_AcquireFileByteBuffer(e, real_name.s, byte_buffer);
                file_assets_cacher.type_assets = e->type_assets;
                file_assets_cacher.file_assets = e->file_assets;
            }
        }
        if (0 != byte_buffer->length) { break; }
        if (0 != mm_is_absolute_path(unit_name.s))
        {
            mmAbsolutePathAcquireFileByteBuffer(unit_name.s, byte_buffer);
            if (0 != byte_buffer->length)
            {
                file_assets_cacher.type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
                file_assets_cacher.file_assets = (struct mmFileAssetsInterface*)&g_FileAssetsFolderAbsolutePath;
            }
        }
        if (0 != byte_buffer->length) { break; }
        if (0 != p->assets_folder.size)
        {
            struct mmFileAssetsFolder* e = NULL;
            n = mm_rb_first(&p->assets_folder.rbt);
            while (NULL != n)
            {
                it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
                n = mm_rb_next(n);
                e = (struct mmFileAssetsFolder*)(it->v);
                mmFileAssetsFolder_PathNameToRealName(e, &real_name, unit_name.s);
                mmFileAssetsFolder_AcquireFileByteBuffer(e, real_name.s, byte_buffer);
                if (0 != byte_buffer->length)
                {
                    file_assets_cacher.type_assets = MM_FILE_ASSETS_TYPE_FOLDER;
                    file_assets_cacher.file_assets = (struct mmFileAssetsInterface*)e;
                    break;
                }
            }
        }
        if (0 != byte_buffer->length) { break; }
        if (0 != p->assets_source.size)
        {
            struct mmFileAssetsSource* e = NULL;
            n = mm_rb_first(&p->assets_source.rbt);
            while (NULL != n)
            {
                it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
                n = mm_rb_next(n);
                e = (struct mmFileAssetsSource*)(it->v);
                mmFileAssetsSource_PathNameToRealName(e, &real_name, unit_name.s);
                mmFileAssetsSource_AcquireFileByteBuffer(e, real_name.s, byte_buffer);
                if (0 != byte_buffer->length)
                {
                    file_assets_cacher.type_assets = MM_FILE_ASSETS_TYPE_SOURCE;
                    file_assets_cacher.file_assets = (struct mmFileAssetsInterface*)e;
                    break;
                }
            }
        }
    } while (0);
    if (0 != byte_buffer->length)
    {
        struct mmFileAssetsCacher* e = mmFileContext_AddAssetsCacher(p, unit_name.s);
        e->type_assets = file_assets_cacher.type_assets;
        e->file_assets = file_assets_cacher.file_assets;
    }
    else
    {
        struct mm_logger* g_logger = mm_logger_instance();
        mm_logger_log_T(g_logger, "%s %d acquire path_name:%s failure.", __FUNCTION__, __LINE__, path_name);
        mmFileContext_RmvAssetsCacher(p, real_name.s);
    }
    mm_string_destroy(&unit_name);
    mm_string_destroy(&real_name);
    mmFileAssetsCacher_Destroy(&file_assets_cacher);
}
// release file buffer.
MM_EXPORT_DISH void mmFileContext_ReleaseFileByteBuffer(struct mmFileContext* p, struct mm_byte_buffer* byte_buffer)
{
    mm_byte_buffer_free(byte_buffer);
}

MM_EXPORT_DISH void mmFileContext_AcquireFindFiles(struct mmFileContext* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree)
{
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mm_rb_node* n = NULL;
    if (0 != p->assets_folder.size)
    {
        struct mmFileAssetsFolder* e = NULL;
        n = mm_rb_first(&p->assets_folder.rbt);
        while (NULL != n)
        {
            it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
            n = mm_rb_next(n);
            e = (struct mmFileAssetsFolder*)(it->v);
            mmFileAssetsFolder_AcquireFindFiles(e, directory, pattern, recursive, dirs, ignore_hidden, rbtree);
        }
    }
    if (0 != p->assets_source.size)
    {
        struct mmFileAssetsSource* e = NULL;
        n = mm_rb_first(&p->assets_source.rbt);
        while (NULL != n)
        {
            it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
            n = mm_rb_next(n);
            e = (struct mmFileAssetsSource*)(it->v);
            mmFileAssetsSource_AcquireFindFiles(e, directory, pattern, recursive, dirs, ignore_hidden, rbtree);
        }
    }
}
MM_EXPORT_DISH void mmFileContext_ReleaseFindFiles(struct mmFileContext* p, struct mm_rbtree_string_vpt* rbtree)
{
    struct mm_rb_node* n = NULL;
    struct mm_rbtree_string_vpt_iterator* it = NULL;
    struct mmFileAssetsInfo* e = NULL;
    n = mm_rb_first(&rbtree->rbt);
    while (NULL != n)
    {
        it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
        n = mm_rb_next(n);
        e = (struct mmFileAssetsInfo*)(it->v);
        mm_rbtree_string_vpt_erase(rbtree, it);
        mmFileAssetsInfo_Destroy(e);
        mm_free(e);
    }
}
