/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmActivityMaster.h"

#include "core/mm_alloc.h"

#include "nwsi/mmEventSurface.h"

#include "OgreRoot.h"
#include "OgreSceneManager.h"
#include "OgreSceneNode.h"
#include "OgreEntity.h"
#include "OgreCamera.h"
#include "OgreViewport.h"

#include "CEGUI/SchemeManager.h"
#include "CEGUI/FontManager.h"
#include "CEGUI/WindowManager.h"

static void __static_mmActivityMaster_CreateScene(struct mmActivityMaster* p);
static void __static_mmActivityMaster_DestroyScene(struct mmActivityMaster* p);

static void __static_mmActivityMaster_CreateLayer(struct mmActivityMaster* p);
static void __static_mmActivityMaster_DestroyLayer(struct mmActivityMaster* p);

static bool __static_mmActivityMaster_OnUpdatedDisplay(void* obj, const mm::mmEventArgs& args);
static bool __static_mmActivityMaster_OnKeypadStatus(void* obj, const mm::mmEventArgs& args);

MM_EXPORT_NWSI void mmActivityMaster_Init(struct mmActivityMaster* p)
{
    mmActivityInterface_Init(&p->hSuper);
    p->pContextMaster = NULL;
    p->pSurfaceMaster = NULL;
    
    p->pSceneManager = NULL;
    p->pCamera = NULL;
    p->pViewport = NULL;
    p->pLight = NULL;
    
    p->pEnttity = NULL;
    p->pSceneNode = NULL;
    
    p->pRootWindow = NULL;
    p->pRootSafety = NULL;
    p->pFrameWindow = NULL;
    p->pEditbox0 = NULL;
    p->pEditbox1 = NULL;
    
    p->hSuper.SetContext = &mmActivityMaster_SetContext;
    p->hSuper.SetSurface = &mmActivityMaster_SetSurface;
    p->hSuper.OnStart = &mmActivityMaster_OnStart;
    p->hSuper.OnInterrupt = &mmActivityMaster_OnInterrupt;
    p->hSuper.OnShutdown = &mmActivityMaster_OnShutdown;
    p->hSuper.OnJoin = &mmActivityMaster_OnJoin;
    p->hSuper.OnFinishLaunching = &mmActivityMaster_OnFinishLaunching;
    p->hSuper.OnBeforeTerminate = &mmActivityMaster_OnBeforeTerminate;
    p->hSuper.OnEnterBackground = &mmActivityMaster_OnEnterBackground;
    p->hSuper.OnEnterForeground = &mmActivityMaster_OnEnterForeground;
}
MM_EXPORT_NWSI void mmActivityMaster_Destroy(struct mmActivityMaster* p)
{
    p->pEditbox1 = NULL;
    p->pEditbox0 = NULL;
    p->pFrameWindow = NULL;
    p->pRootSafety = NULL;
    p->pRootWindow = NULL;
    
    p->pSceneNode = NULL;
    p->pEnttity = NULL;
    
    p->pLight = NULL;
    p->pViewport = NULL;
    p->pCamera = NULL;
    p->pSceneManager = NULL;
    
    p->pSurfaceMaster = NULL;
    p->pContextMaster = NULL;
    mmActivityInterface_Destroy(&p->hSuper);
}
// virtual function for super interface implement.
MM_EXPORT_NWSI void mmActivityMaster_SetContext(struct mmActivityInterface* pSuper, struct mmContextMaster* pContextMaster)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    p->pContextMaster = pContextMaster;
}
MM_EXPORT_NWSI void mmActivityMaster_SetSurface(struct mmActivityInterface* pSuper, struct mmSurfaceMaster* pSurfaceMaster)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    p->pSurfaceMaster = pSurfaceMaster;
}
MM_EXPORT_NWSI void mmActivityMaster_OnStart(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
MM_EXPORT_NWSI void mmActivityMaster_OnInterrupt(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
MM_EXPORT_NWSI void mmActivityMaster_OnShutdown(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
MM_EXPORT_NWSI void mmActivityMaster_OnJoin(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}

MM_EXPORT_NWSI void mmActivityMaster_OnFinishLaunching(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    __static_mmActivityMaster_CreateScene(p);
    __static_mmActivityMaster_CreateLayer(p);
}
MM_EXPORT_NWSI void mmActivityMaster_OnBeforeTerminate(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
    
    __static_mmActivityMaster_DestroyLayer(p);
    __static_mmActivityMaster_DestroyScene(p);
}

MM_EXPORT_NWSI void mmActivityMaster_OnEnterBackground(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}
MM_EXPORT_NWSI void mmActivityMaster_OnEnterForeground(struct mmActivityInterface* pSuper)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(pSuper);
}

static struct mmActivityInterface* __static_mmActivityMaster_Produce(void)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)mm_malloc(sizeof(struct mmActivityMaster));
    mmActivityMaster_Init(p);
    return (struct mmActivityInterface*)p;
}
static void __static_mmActivityMaster_Recycle(struct mmActivityInterface* m)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(m);
    mmActivityMaster_Destroy(p);
    mm_free(p);
}
MM_EXPORT_NWSI const struct mmActivityInterfaceCreator MM_ACTIVITYINTERFACECREATOR_MASTER =
{
    __static_mmActivityMaster_Produce,
    __static_mmActivityMaster_Recycle,
};


static void __static_mmActivityMaster_CreateScene(struct mmActivityMaster* p)
{
    struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    struct mmOgreSystem* pOgreSystem = &pContextMaster->hOgreSystem;
    Ogre::Root* _OgreRoot = pOgreSystem->pOgreRoot;
    Ogre::RenderWindow* pRenderWindow = pSurfaceMaster->pRenderWindow;
    
    Ogre::RTShader::ShaderGenerator* _ShaderGenerator = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
    Ogre::ResourceGroupManager* _ResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();
    
    p->pSceneManager = _OgreRoot->createSceneManager("DefaultSceneManager", "DummyScene");
    _ShaderGenerator->addSceneManager(p->pSceneManager);
    
    _ResourceGroupManager->addResourceLocation("media/models/coin", "mmFileSystem", "media/models/coin");
    
    _ResourceGroupManager->initialiseResourceGroup("media/models/coin");
    
    // Create the camera
    p->pCamera = p->pSceneManager->createCamera("PlayerCam");
    // Position it at 500 in Z direction
    p->pCamera->setPosition(Ogre::Vector3(0,0,20));
    // Look back along -Z
    p->pCamera->lookAt(Ogre::Vector3(0,0,-300));
    p->pCamera->setNearClipDistance(5);
    // create viewports
    // Create one viewport, entire window
    p->pViewport = pRenderWindow->addViewport(p->pCamera);
    p->pViewport->setBackgroundColour(Ogre::ColourValue(0x00, 0x00, 0x00));
    // Alter the camera aspect ratio to match the viewport
    int _actualW = p->pViewport->getActualWidth();
    int _actualH = p->pViewport->getActualHeight();
    Ogre::Real _hAspectRatio = (Ogre::Real)_actualW / (Ogre::Real)_actualH;
    p->pCamera->setAspectRatio(_hAspectRatio);
    p->pCamera->setAutoAspectRatio(true);
    // Set ambient light
    p->pSceneManager->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
    
    // Create a light
    p->pLight = p->pSceneManager->createLight("MainLight");
    p->pLight->setPosition(Ogre::Vector3(20, 80, 50));
    
    // Add entity
    Ogre::SceneNode* _rootNode = p->pSceneManager->getRootSceneNode();
    p->pEnttity = p->pSceneManager->createEntity("coin", "coin.mesh", "media/models/coin");
    p->pSceneNode = _rootNode->createChildSceneNode();
    p->pSceneNode->attachObject(p->pEnttity);
    p->pSceneNode->pitch(Ogre::Radian(Ogre::Degree(60.0f)));
    p->pSceneNode->scale(1.5f, 1.5f, 1.5f);
    
    mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventUpdatedDisplay, &__static_mmActivityMaster_OnUpdatedDisplay, p);
    pEventSet->SubscribeEvent(mmSurfaceMaster_EventKeypadStatus, &__static_mmActivityMaster_OnKeypadStatus, p);
}
static void __static_mmActivityMaster_DestroyScene(struct mmActivityMaster* p)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    Ogre::RenderWindow* pRenderWindow = pSurfaceMaster->pRenderWindow;
    
    Ogre::RTShader::ShaderGenerator* _ShaderGenerator = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
    Ogre::ResourceGroupManager* _ResourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();
    
    mm::mmEventSet* pEventSet = &pSurfaceMaster->hEventSet;
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventKeypadStatus, &__static_mmActivityMaster_OnKeypadStatus, p);
    pEventSet->UnsubscribeEvent(mmSurfaceMaster_EventUpdatedDisplay, &__static_mmActivityMaster_OnUpdatedDisplay, p);
    
    p->pSceneNode->detachObject(p->pEnttity);
    p->pSceneManager->destroySceneNode(p->pSceneNode);
    p->pSceneManager->destroyEntity(p->pEnttity);
    
    p->pSceneManager->destroyLight(p->pLight);
    
    pRenderWindow->removeViewport(p->pViewport->getZOrder());
    p->pSceneManager->destroyCamera(p->pCamera);
    
    _ResourceGroupManager->removeResourceLocation("media/models/coin");
    
    _ShaderGenerator->removeSceneManager(p->pSceneManager);
    
    p->pSceneNode = NULL;
    p->pEnttity = NULL;
    
    p->pLight = NULL;
    p->pViewport = NULL;
    p->pCamera = NULL;
    p->pSceneManager = NULL;
}

static void __static_mmActivityMaster_CreateLayer(struct mmActivityMaster* p)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    
    //static const float DEFAULT_SYSTEM_FONT_SIZE = 20.0f;
    CEGUI::GUIContext* pGUIContext = pSurfaceMaster->pGUIContext;
    //
    //const struct mm_vector2& _window_size = _surface->get_window_size();
    //const struct mm_vector2& _canvas_size = _surface->get_canvas_size();

    //int index = 0;
    //float font_size = DEFAULT_SYSTEM_FONT_SIZE;
    //char index_string[64] = { 0 };
    //mm_sprintf(index_string, "_%f_%d", font_size, index);

    //std::string system_font_name = "system_font";
    //system_font_name = system_font_name + index_string;
    //const char* font_name = system_font_name.c_str();
    //
    CEGUI::SchemeManager* _SchemeManager = CEGUI::SchemeManager::getSingletonPtr();
    CEGUI::FontManager* _FontManager = CEGUI::FontManager::getSingletonPtr();
    //
    // CEGUI relies on various systems being set-up, so this is what we do
    // here first.
    //
    // The first thing to do is load a CEGUI 'scheme' this is basically a file
    // that groups all the required resources and definitions for a particular
    // skin so they can be loaded / initialised easily
    //
    // So, we use the SchemeManager singleton to load in a scheme that loads the
    // imagery and registers widgets for the TaharezLook skin.  This scheme also
    // loads in a font that gets used as the system default.
    _SchemeManager->createFromFile("TaharezLook.scheme");

    // We need Manual notifyDisplaySizeChanged event for CEGUI::FontManager
    // after the scheme resource is loaded.
    //_FontManager->notifyDisplaySizeChanged(CEGUI::Sizef((float)_window_size.x, (float)_window_size.y));

    // create the default surface font.
    //CEGUI::Font& _defaultFont = _cegui_system->create_system_font_by_size(_canvas_size, font_name, font_size);
    //CEGUI::Font& _defaultFont = _cegui_system->create_font_size(_canvas_size, font_name, font_size, "wqy-microhei.ttc");
    //CEGUI::Font& _hDefaultFont = _FontManager->createFromFile("wqy-microhei-20.font");
    CEGUI::Font& _hDefaultFont = _FontManager->createFromFile("DejaVuSansMono-20.font");

    // The next thing we do is to set a default mouse cursor image.  This is
    // not strictly essential, although it is nice to always have a visible
    // cursor if a window or widget does not explicitly set one of its own.
    //
    // The TaharezLook Imageset contains an Image named "MouseArrow" which is
    // the ideal thing to have as a defult mouse cursor image.
    CEGUI::MouseCursor& _hMouseCursor = pGUIContext->getMouseCursor();
    _hMouseCursor.setDefaultImage("TaharezLook/MouseArrow");
    // _hMouseCursor.setVisible(false);

    // load font and setup default if not loaded via scheme
    // Set default font for the gui context
    pGUIContext->setDefaultFont(&_hDefaultFont);
    
    CEGUI::WindowManager* _WindowManager = CEGUI::WindowManager::getSingletonPtr();
    
    p->pRootWindow = (CEGUI::Window*)_WindowManager->createWindow("DefaultWindow", "RootWindow");
    p->pRootSafety = (CEGUI::Window*)_WindowManager->createWindow("DefaultWindow", "RootSafety");
    p->pRootSafety->setZOrderingEnabled(false);
    
    // Create a FrameWindow in the TaharezLook style, and name it 'Demo Window'
    p->pFrameWindow = (CEGUI::Window*)_WindowManager->createWindow("TaharezLook/FrameWindow", "Demo Window");
    p->pFrameWindow->setPosition(CEGUI::UVector2(cegui_reldim(0.25f), cegui_reldim( 0.25f)));
    p->pFrameWindow->setSize(CEGUI::USize(cegui_reldim(0.5f), cegui_reldim( 0.5f)));
    p->pFrameWindow->setMaxSize(CEGUI::USize(cegui_reldim(1.0f), cegui_reldim( 1.0f)));
    p->pFrameWindow->setMinSize(CEGUI::USize(cegui_reldim(0.1f), cegui_reldim( 0.1f)));
    p->pFrameWindow->setText("Hello World!");
    p->pFrameWindow->setZOrderingEnabled(false);
    
    p->pEditbox0 = static_cast<CEGUI::Window*>(_WindowManager->createWindow("TaharezLook/Editbox", "NewColIDBox0"));
    p->pEditbox0->setPosition(CEGUI::UVector2(cegui_reldim(0.02f), cegui_reldim( 0.02f)));
    p->pEditbox0->setSize(CEGUI::USize(cegui_reldim(0.96f), cegui_reldim( 0.06f)));
    p->pEditbox0->setText("Test -- 0 ");
    
    p->pEditbox1 = static_cast<CEGUI::Window*>(_WindowManager->createWindow("TaharezLook/Editbox", "NewColIDBox1"));
    p->pEditbox1->setPosition(CEGUI::UVector2(cegui_reldim(0.02f), cegui_reldim( 0.92f)));
    p->pEditbox1->setSize(CEGUI::USize(cegui_reldim(0.96f), cegui_reldim( 0.06f)));
    p->pEditbox1->setText("Test -- 1 ");    
    
    p->pRootSafety->addChild(p->pEditbox0);
    p->pRootSafety->addChild(p->pEditbox1);
    
    p->pRootWindow->addChild(p->pRootSafety);
    p->pRootWindow->addChild(p->pFrameWindow);
    
    pGUIContext->setRootWindow(p->pRootWindow);
    
    mmSurfaceMaster_SetRootSafety(pSurfaceMaster, p->pRootSafety);
}
static void __static_mmActivityMaster_DestroyLayer(struct mmActivityMaster* p)
{
    struct mmSurfaceMaster* pSurfaceMaster = p->pSurfaceMaster;
    
    CEGUI::GUIContext* pGUIContext = pSurfaceMaster->pGUIContext;
    
    CEGUI::WindowManager* _WindowManager = CEGUI::WindowManager::getSingletonPtr();
    
    mmSurfaceMaster_SetRootSafety(pSurfaceMaster, NULL);
    
    pGUIContext->setRootWindow(NULL);
    
    _WindowManager->destroyWindow(p->pEditbox1);
    _WindowManager->destroyWindow(p->pEditbox0);
    _WindowManager->destroyWindow(p->pFrameWindow);
    _WindowManager->destroyWindow(p->pRootSafety);
    _WindowManager->destroyWindow(p->pRootWindow);
    
    p->pEditbox1 = NULL;
    p->pEditbox0 = NULL;
    p->pFrameWindow = NULL;
    p->pRootWindow = NULL;
    p->pRootSafety = NULL;
}

static bool __static_mmActivityMaster_OnUpdatedDisplay(void* obj, const mm::mmEventArgs& args)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(obj);
    const mm::mmEventUpdate& hEventArgs = (const mm::mmEventUpdate&)(args);
    struct mmSurfaceContentUpdate* pContentUpdate = hEventArgs.content;
    
    p->pSceneNode->pitch(Ogre::Degree(5.0f * pContentUpdate->interval));
    return false;
}

static bool __static_mmActivityMaster_OnKeypadStatus(void* obj, const mm::mmEventArgs& args)
{
    struct mmActivityMaster* p = (struct mmActivityMaster*)(obj);
    const mm::mmEventKeypadStatus& hEventArgs = (const mm::mmEventKeypadStatus&)(args);
    struct mmSurfaceContentKeypadStatus* pContentKeypadStatus = hEventArgs.content;
    
    pContentKeypadStatus->handle = 0;
    return false;
}
