/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileContext_h__
#define __mmFileContext_h__

#include "core/mm_core.h"
#include "core/mm_list.h"
#include "core/mm_byte.h"
#include "core/mm_file_system.h"

#include "container/mm_rbtree_string.h"

#include "mmFileZip.h"

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "dish/mm_dish_export.h"

#include "core/mm_prefix.h"

struct mmFileAssetsInterface
{
    mm_uint32_t(*AssetsType)(struct mmFileAssetsInterface* p);
    int(*IsDataExists)(struct mmFileAssetsInterface* p, const char* real_name);
    void(*FileInfoName)(struct mmFileAssetsInterface* p, struct mm_string* file_info_name, const char* file_name);
    void(*PathNameToRealName)(struct mmFileAssetsInterface* p, struct mm_string* real_name, const char* path_name);
    void(*RealNameToPathName)(struct mmFileAssetsInterface* p, struct mm_string* path_name, const char* real_name);
    void(*AcquireFileByteBuffer)(struct mmFileAssetsInterface* p, const char* real_name, struct mm_byte_buffer* byte_buffer);
    void(*ReleaseFileByteBuffer)(struct mmFileAssetsInterface* p, struct mm_byte_buffer* byte_buffer);
    void(*AcquireFindFiles)(struct mmFileAssetsInterface* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
    void(*ReleaseFindFiles)(struct mmFileAssetsInterface* p, struct mm_rbtree_string_vpt* rbtree);
};

MM_EXPORT_DISH void mmFileAssetsInterface_Init(struct mmFileAssetsInterface* p);
MM_EXPORT_DISH void mmFileAssetsInterface_Destroy(struct mmFileAssetsInterface* p);

// ../../resources/assets
// assets_path ../../resources
// assets_base assets
struct mmFileAssetsFolder
{
    struct mmFileAssetsInterface hSuper;

    // name for assets.
    struct mm_string assets_name;
    // path for assets.
    struct mm_string assets_path;
    // base for assets.
    struct mm_string assets_base;
};

MM_EXPORT_DISH void mmFileAssetsFolder_Init(struct mmFileAssetsFolder* p);
MM_EXPORT_DISH void mmFileAssetsFolder_Destroy(struct mmFileAssetsFolder* p);

MM_EXPORT_DISH void mmFileAssetsFolder_SetAssets(struct mmFileAssetsFolder* p, const char* name, const char* path, const char* base);
MM_EXPORT_DISH void mmFileAssetsFolder_AcquireEntry(struct mmFileAssetsFolder* p);
MM_EXPORT_DISH void mmFileAssetsFolder_ReleaseEntry(struct mmFileAssetsFolder* p);

// virtual function for super.
MM_EXPORT_DISH mm_uint32_t mmFileAssetsFolder_OnAssetsType(struct mmFileAssetsInterface* pSuper);
MM_EXPORT_DISH int mmFileAssetsFolder_OnIsDataExists(struct mmFileAssetsInterface* pSuper, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsFolder_OnFileInfoName(struct mmFileAssetsInterface* pSuper, struct mm_string* file_info_name, const char* file_name);
MM_EXPORT_DISH void mmFileAssetsFolder_OnPathNameToRealName(struct mmFileAssetsInterface* pSuper, struct mm_string* real_name, const char* path_name);
MM_EXPORT_DISH void mmFileAssetsFolder_OnRealNameToPathName(struct mmFileAssetsInterface* pSuper, struct mm_string* path_name, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsFolder_OnAcquireFileByteBuffer(struct mmFileAssetsInterface* pSuper, const char* real_name, struct mm_byte_buffer* byte_buffer);
MM_EXPORT_DISH void mmFileAssetsFolder_OnReleaseFileByteBuffer(struct mmFileAssetsInterface* pSuper, struct mm_byte_buffer* byte_buffer);
MM_EXPORT_DISH void mmFileAssetsFolder_OnAcquireFindFiles(struct mmFileAssetsInterface* pSuper, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
MM_EXPORT_DISH void mmFileAssetsFolder_OnReleaseFindFiles(struct mmFileAssetsInterface* pSuper, struct mm_rbtree_string_vpt* rbtree);

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH int mmFileAssetsFolder_IsDataExists(struct mmFileAssetsFolder* p, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsFolder_FileInfoName(struct mmFileAssetsFolder* p, struct mm_string* file_info_name, const char* file_name);
MM_EXPORT_DISH void mmFileAssetsFolder_PathNameToRealName(struct mmFileAssetsFolder* p, struct mm_string* real_name, const char* path_name);
MM_EXPORT_DISH void mmFileAssetsFolder_RealNameToPathName(struct mmFileAssetsFolder* p, struct mm_string* path_name, const char* real_name);
// acquire file buffer.
MM_EXPORT_DISH void mmFileAssetsFolder_AcquireFileByteBuffer(struct mmFileAssetsFolder* p, const char* real_name, struct mm_byte_buffer* byte_buffer);
// release file buffer.
MM_EXPORT_DISH void mmFileAssetsFolder_ReleaseFileByteBuffer(struct mmFileAssetsFolder* p, struct mm_byte_buffer* byte_buffer);

MM_EXPORT_DISH void mmFileAssetsFolder_AcquireFindFiles(struct mmFileAssetsFolder* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
MM_EXPORT_DISH void mmFileAssetsFolder_ReleaseFindFiles(struct mmFileAssetsFolder* p, struct mm_rbtree_string_vpt* rbtree);

// ../../resources/assets.zip
// assets_path ../../resources/assets.zip
// assets_base assets (zip file inside )
struct mmFileAssetsSource
{
    struct mmFileAssetsInterface hSuper;

    // name for assets.
    struct mm_string assets_name;
    // path for assets.
    struct mm_string assets_path;
    // base for assets.
    struct mm_string assets_base;
    struct mmFileZip zip_file;
};

MM_EXPORT_DISH void mmFileAssetsSource_Init(struct mmFileAssetsSource* p);
MM_EXPORT_DISH void mmFileAssetsSource_Destroy(struct mmFileAssetsSource* p);

MM_EXPORT_DISH void mmFileAssetsSource_SetAssets(struct mmFileAssetsSource* p, const char* name, const char* path, const char* base);
MM_EXPORT_DISH void mmFileAssetsSource_AcquireEntry(struct mmFileAssetsSource* p);
MM_EXPORT_DISH void mmFileAssetsSource_ReleaseEntry(struct mmFileAssetsSource* p);

// virtual function for super.
MM_EXPORT_DISH mm_uint32_t mmFileAssetsSource_OnAssetsType(struct mmFileAssetsInterface* pSuper);
MM_EXPORT_DISH int mmFileAssetsSource_OnIsDataExists(struct mmFileAssetsInterface* pSuper, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsSource_OnFileInfoName(struct mmFileAssetsInterface* pSuper, struct mm_string* file_info_name, const char* file_name);
MM_EXPORT_DISH void mmFileAssetsSource_OnPathNameToRealName(struct mmFileAssetsInterface* pSuper, struct mm_string* real_name, const char* path_name);
MM_EXPORT_DISH void mmFileAssetsSource_OnRealNameToPathName(struct mmFileAssetsInterface* pSuper, struct mm_string* path_name, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsSource_OnAcquireFileByteBuffer(struct mmFileAssetsInterface* pSuper, const char* real_name, struct mm_byte_buffer* byte_buffer);
MM_EXPORT_DISH void mmFileAssetsSource_OnReleaseFileByteBuffer(struct mmFileAssetsInterface* pSuper, struct mm_byte_buffer* byte_buffer);
MM_EXPORT_DISH void mmFileAssetsSource_OnAcquireFindFiles(struct mmFileAssetsInterface* pSuper, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
MM_EXPORT_DISH void mmFileAssetsSource_OnReleaseFindFiles(struct mmFileAssetsInterface* pSuper, struct mm_rbtree_string_vpt* rbtree);

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH int mmFileAssetsSource_IsDataExists(struct mmFileAssetsSource* p, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsSource_FileInfoName(struct mmFileAssetsSource* p, struct mm_string* file_info_name, const char* file_name);
MM_EXPORT_DISH void mmFileAssetsSource_PathNameToRealName(struct mmFileAssetsSource* p, struct mm_string* real_name, const char* path_name);
MM_EXPORT_DISH void mmFileAssetsSource_RealNameToPathName(struct mmFileAssetsSource* p, struct mm_string* path_name, const char* real_name);
// acquire file buffer.
MM_EXPORT_DISH void mmFileAssetsSource_AcquireFileByteBuffer(struct mmFileAssetsSource* p, const char* real_name, struct mm_byte_buffer* byte_buffer);
// release file buffer.
MM_EXPORT_DISH void mmFileAssetsSource_ReleaseFileByteBuffer(struct mmFileAssetsSource* p, struct mm_byte_buffer* byte_buffer);

MM_EXPORT_DISH void mmFileAssetsSource_AcquireFindFiles(struct mmFileAssetsSource* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
MM_EXPORT_DISH void mmFileAssetsSource_ReleaseFindFiles(struct mmFileAssetsSource* p, struct mm_rbtree_string_vpt* rbtree);

enum mmFileAssetsType
{
    MM_FILE_ASSETS_TYPE_FOLDER = 0,
    MM_FILE_ASSETS_TYPE_SOURCE = 1,
};

struct mmFileAssetsCacher
{
    mm_uint32_t type_assets;
    struct mmFileAssetsInterface* file_assets;
};

MM_EXPORT_DISH void mmFileAssetsCacher_Init(struct mmFileAssetsCacher* p);
MM_EXPORT_DISH void mmFileAssetsCacher_Destroy(struct mmFileAssetsCacher* p);

// copy q to p.
MM_EXPORT_DISH void mmFileAssetsCacher_Copy(struct mmFileAssetsCacher* p, struct mmFileAssetsCacher* q);

MM_EXPORT_DISH mm_uint32_t mmFileAssetsCacher_AssetsType(struct mmFileAssetsCacher* p);
MM_EXPORT_DISH int mmFileAssetsCacher_IsDataExists(struct mmFileAssetsCacher* p, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsCacher_FileInfoName(struct mmFileAssetsCacher* p, struct mm_string* file_info_name, const char* file_name);
MM_EXPORT_DISH void mmFileAssetsCacher_PathNameToRealName(struct mmFileAssetsCacher* p, struct mm_string* real_name, const char* path_name);
MM_EXPORT_DISH void mmFileAssetsCacher_RealNameToPathName(struct mmFileAssetsCacher* p, struct mm_string* path_name, const char* real_name);
MM_EXPORT_DISH void mmFileAssetsCacher_AcquireFileByteBuffer(struct mmFileAssetsCacher* p, const char* real_name, struct mm_byte_buffer* byte_buffer);
MM_EXPORT_DISH void mmFileAssetsCacher_ReleaseFileByteBuffer(struct mmFileAssetsCacher* p, struct mm_byte_buffer* byte_buffer);
MM_EXPORT_DISH void mmFileAssetsCacher_AcquireFindFiles(struct mmFileAssetsCacher* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
MM_EXPORT_DISH void mmFileAssetsCacher_ReleaseFindFiles(struct mmFileAssetsCacher* p, struct mm_rbtree_string_vpt* rbtree);

struct mmFileAssetsInfo
{
    struct mmFileAssetsCacher file_assets;
    // the file's fully qualified name
    struct mm_string filename;
    // base filename
    struct mm_string basename;
    // path name; separated by '/' and ending with '/'
    struct mm_string pathname;
    // compressed size
    size_t csize;
    // uncompressed size
    size_t dsize;
};
MM_EXPORT_DISH void mmFileAssetsInfo_Init(struct mmFileAssetsInfo* p);
MM_EXPORT_DISH void mmFileAssetsInfo_Destroy(struct mmFileAssetsInfo* p);

struct mmFileContext
{
    struct mm_string assets_root_folder_path;
    struct mm_string assets_root_folder_base;
    struct mm_string assets_root_source_path;
    struct mm_string assets_root_source_base;
    //
    struct mm_rbtree_string_vpt assets_folder;
    struct mm_rbtree_string_vpt assets_source;
    struct mm_rbtree_string_vpt assets_cacher;
};

MM_EXPORT_DISH void mmFileContext_Init(struct mmFileContext* p);
MM_EXPORT_DISH void mmFileContext_Destroy(struct mmFileContext* p);

MM_EXPORT_DISH void mmFileContext_SetAssetsRootFolder(struct mmFileContext* p, const char* root_folder, const char* root_base);
MM_EXPORT_DISH void mmFileContext_SetAssetsRootSource(struct mmFileContext* p, const char* root_source, const char* root_base);

MM_EXPORT_DISH struct mmFileAssetsFolder* mmFileContext_AddAssetsFolder(struct mmFileContext* p, const char* name, const char* path, const char* base);
MM_EXPORT_DISH struct mmFileAssetsFolder* mmFileContext_GetAssetsFolder(struct mmFileContext* p, const char* name);
MM_EXPORT_DISH void mmFileContext_RmvAssetsFolder(struct mmFileContext* p, const char* name);
MM_EXPORT_DISH void mmFileContext_ClearAssetsFolder(struct mmFileContext* p);

MM_EXPORT_DISH struct mmFileAssetsSource* mmFileContext_AddAssetsSource(struct mmFileContext* p, const char* name, const char* path, const char* base);
MM_EXPORT_DISH struct mmFileAssetsSource* mmFileContext_GetAssetsSource(struct mmFileContext* p, const char* name);
MM_EXPORT_DISH void mmFileContext_RmvAssetsSource(struct mmFileContext* p, const char* name);
MM_EXPORT_DISH void mmFileContext_ClearAssetsSource(struct mmFileContext* p);

MM_EXPORT_DISH struct mmFileAssetsCacher* mmFileContext_AddAssetsCacher(struct mmFileContext* p, const char* real_name);
MM_EXPORT_DISH struct mmFileAssetsCacher* mmFileContext_GetAssetsCacher(struct mmFileContext* p, const char* real_name);
MM_EXPORT_DISH void mmFileContext_RmvAssetsCacher(struct mmFileContext* p, const char* real_name);
MM_EXPORT_DISH void mmFileContext_ClearAssetsCacher(struct mmFileContext* p);
MM_EXPORT_DISH void mmFileContext_ClearAssetsCacherByPointer(struct mmFileContext* p, void* file_assets);

// check file exists.1 exists 0 not exists.
// path_name is assets logic absolute.
MM_EXPORT_DISH int mmFileContext_IsFileExists(struct mmFileContext* p, const char* path_name);
// acquire file buffer.
// priority 0 folder 1 source, note: we will first acquire folder file byte buffer.
MM_EXPORT_DISH void mmFileContext_AcquireFileByteBuffer(struct mmFileContext* p, const char* path_name, struct mm_byte_buffer* byte_buffer);
// release file buffer.
MM_EXPORT_DISH void mmFileContext_ReleaseFileByteBuffer(struct mmFileContext* p, struct mm_byte_buffer* byte_buffer);

MM_EXPORT_DISH void mmFileContext_AcquireFindFiles(struct mmFileContext* p, const char* directory, const char* pattern, int recursive, int dirs, int ignore_hidden, struct mm_rbtree_string_vpt* rbtree);
MM_EXPORT_DISH void mmFileContext_ReleaseFindFiles(struct mmFileContext* p, struct mm_rbtree_string_vpt* rbtree);

#include "core/mm_suffix.h"

#endif//__mmFileContext_h__
