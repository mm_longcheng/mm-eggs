//
//  mmViewController.m
//  mm_nwsi
//
//  Created by mm_longcheng on 2019/10/15.
//  Copyright © 2019 mm_longcheng@icloud.com. All rights reserved.
//

#import "mmViewController.h"

#import "nwsi/mmObjcARC.h"

#import "nwsi/mmNativeApplication.h"
#import "nwsi/mmUIViewSurfaceMaster.h"


@interface mmViewController ()
{
    struct mmNativeApplication* pNativeApplication;
}

@property (weak, nonatomic) UIWindow* windowMaster;

@end

@implementation mmViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // static const char* f = "/System/Library/Fonts/Core/Helvetica.ttc";
    // static const char* f = "/System/Library/Fonts/Core";
    static const char* f = "/System/Library/Fonts";
    // static const char* f = "/System/Library/Fonts/PingFang.ttc";
    // static const char* f = "/System/Library/Fonts/Helvetica.ttc";
    
    mm_stat_t s;
    if (0 != mm_stat(f, &s))
    {
        // the source item can not stat.
        printf("none\n");
    }
    else
    {
        printf("have\n");
        
        char fdbf[1024] = { 0 };
        
        long fdsize = 0;
        FILE* fd = fopen(f, "rb");
        fseek(fd, 0, SEEK_END);
        fdsize = ftell(fd);
        fseek(fd, 0, SEEK_SET);
        fread(fdbf, sizeof(char), fdsize, fd);
        fclose(fd);
        
        struct mm_file_context fileContext;
        struct mm_rbtree_string_vpt rbtree;
        
        mm_file_context_init(&fileContext);
        mm_rbtree_string_vpt_init(&rbtree);
        
        mm_file_context_assign_assets_root_folder(&fileContext, "/System/Library/Fonts", "");
        
        mm_file_context_acquire_find_files(&fileContext, "", "*", 1, 0, 1, &rbtree);
        
        {
            struct mm_file_info* file_info = NULL;

            struct mm_rb_node* n = NULL;
            struct mm_rbtree_string_vpt_iterator* it = NULL;

            n = mm_rb_first(&rbtree.rbt);
            while (NULL != n)
            {
                it = (struct mm_rbtree_string_vpt_iterator*)mm_rb_entry(n, struct mm_rbtree_string_vpt_iterator, n);
                n = mm_rb_next(n);
                file_info = (struct mm_file_info*)(it->v);

                printf("%s\n", file_info->filename.s);
            }
        }
        
        mm_file_context_release_find_files(&fileContext, &rbtree);
        
        mm_rbtree_string_vpt_destroy(&rbtree);
        mm_file_context_destroy(&fileContext);
    }
    
    NSArray* fontFamilyArray = [UIFont familyNames];
    for (NSString* familyName in fontFamilyArray)
    {
        NSArray* fontArray = [UIFont fontNamesForFamilyName:familyName];
        
        printf("familyName: %s\n", [familyName UTF8String]);
        
        for (NSString* fontName in fontArray)
        {
            printf("    fontName:%s\n", [fontName UTF8String]);
        }
    }
}

- (void)dealloc
{
    mmObjc_Dealloc(super);
}

- (void)SetWindowMaster:(UIWindow*)window
{
    self.windowMaster = window;
}
- (void)SetNativeApplication:(struct mmNativeApplication*)pNativeApplication
{
    self->pNativeApplication = pNativeApplication;
}

- (void)OnFinishLaunching
{
    struct mmSurfaceKeypadInterface* pSurfaceKeypadInterface = [self.viewSurfaceMaster GetSurfaceKeypadInterface];
    struct mmSurfaceMaster* pSurfaceMaster = &self->pNativeApplication->hSurfaceMaster;    
    void* viewSurfaceMasterPtr = (__bridge void*)self.viewSurfaceMaster;
    
    mmSurfaceMaster_SetViewSurface(pSurfaceMaster, viewSurfaceMasterPtr);
    mmSurfaceMaster_SetSurfaceKeypadInterface(pSurfaceMaster, pSurfaceKeypadInterface);
    
    [self.viewSurfaceMaster SetSurfaceMaster: pSurfaceMaster];
    [self.viewSurfaceMaster SetWindowMaster: self.windowMaster];
    [self.viewSurfaceMaster OnFinishLaunching];
}
- (void)OnBeforeTerminate
{
    [self.viewSurfaceMaster OnBeforeTerminate];
}

@end
