//
//  mmAppDelegate.m
//  mm_nwsi
//
//  Created by mm_longcheng on 2019/10/15.
//  Copyright © 2019 mm_longcheng@icloud.com. All rights reserved.
//

#import "mmAppDelegate.h"

#import "nwsi/mmUIWindow.h"
#import "nwsi/mmNativeApplication.h"

#import "mmViewController.h"
#import "mmActivityMaster.h"
#import "nwsi/mmUIViewSurfaceMaster.h"

@interface mmAppDelegate()
{
    struct mmActivityMaster pActivityMaster;
    struct mmNativeApplication hApplication;
}

@property (weak, nonatomic) mmViewController* viewController;

@end

@implementation mmAppDelegate


void static __static_mmAppDelegate_AttachStoryboard(mmAppDelegate* p);
void static __static_mmAppDelegate_DetachStoryboard(mmAppDelegate* p);


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // Create storyboard.
    __static_mmAppDelegate_AttachStoryboard(self);
    // Launching.
    mmActivityMaster_Init(&self->pActivityMaster);
    mmNativeApplication_Init(&self->hApplication);
    mmNativeApplication_SetNativeApplicationHandler(&self->hApplication, (__bridge void*)application);
    mmNativeApplication_SetActivityMaster(&self->hApplication, &self->pActivityMaster.hSuper);
    mmNativeApplication_OnFinishLaunching(&self->hApplication);
    mmNativeApplication_OnStart(&self->hApplication);
    // ViewController launching.
    [self.viewController OnFinishLaunching];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain
    // types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits
    // the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks.
    // Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application
    // state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate:
    // when the user quits.
    mmNativeApplication_OnEnterBackground(&self->hApplication);
    
    // Make Sure the applicationWillTerminate be call.
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the
    // changes made on entering the background.
    mmNativeApplication_OnEnterForeground(&self->hApplication);
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // ViewController terminate.
    [self.viewController OnBeforeTerminate];
    // Terminate.
    mmNativeApplication_OnShutdown(&self->hApplication);
    mmNativeApplication_OnJoin(&self->hApplication);
    mmNativeApplication_OnBeforeTerminate(&self->hApplication);
    mmNativeApplication_Destroy(&self->hApplication);
    mmActivityMaster_Destroy(&self->pActivityMaster);
    // Destroy storyboard.
    __static_mmAppDelegate_DetachStoryboard(self);
}

static void __static_mmAppDelegate_AttachStoryboard(mmAppDelegate* p)
{
    [p setViewController:(mmViewController*)p.window.rootViewController];
    [p.viewController SetWindowMaster: p.window];
    [p.viewController SetNativeApplication:&p->hApplication];
    // Make key window, will load the view.
    [p.window makeKeyAndVisible];
}
static void __static_mmAppDelegate_DetachStoryboard(mmAppDelegate* p)
{
    // Destroy memory.
    [p setViewController:nil];
}

@end
