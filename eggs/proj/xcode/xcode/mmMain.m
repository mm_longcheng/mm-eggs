//
//  mmMain.m
//  mm_nwsi
//
//  Created by mm_longcheng on 2019/10/15.
//  Copyright © 2019 mm_longcheng@icloud.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "mmAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        NSString* _pAppClassString = NSStringFromClass([mmAppDelegate class]);
        return UIApplicationMain(argc, argv, nil, _pAppClassString);
    }
}
