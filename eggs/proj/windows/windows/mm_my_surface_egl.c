#include "mm_my_surface_egl.h"
#include "core/mm_logger.h"

/* Errors / GetError return values */
// #define EGL_SUCCESS               0x3000
// #define EGL_NOT_INITIALIZED       0x3001
// #define EGL_BAD_ACCESS            0x3002
// #define EGL_BAD_ALLOC             0x3003
// #define EGL_BAD_ATTRIBUTE         0x3004
// #define EGL_BAD_CONFIG            0x3005
// #define EGL_BAD_CONTEXT           0x3006
// #define EGL_BAD_CURRENT_SURFACE   0x3007
// #define EGL_BAD_DISPLAY           0x3008
// #define EGL_BAD_MATCH             0x3009
// #define EGL_BAD_NATIVE_PIXMAP     0x300A
// #define EGL_BAD_NATIVE_WINDOW     0x300B
// #define EGL_BAD_PARAMETER         0x300C
// #define EGL_BAD_SURFACE           0x300D
// #define EGL_CONTEXT_LOST          0x300E	/* EGL 1.1 - IMG_power_management */

const struct mm_my_surface_egl_error_string MY_SURFACE_EGL_ERRORCODE_ARRAYS[] =
{
	{ EGL_SUCCESS             , "The last function succeeded without error.", },
	{ EGL_NOT_INITIALIZED     , "EGL is not initialized, or could not be initialized, for the specified EGL display connection.", },
	{ EGL_BAD_ACCESS          , "EGL cannot access a requested resource(for example a context is bound in another thread).", },
	{ EGL_BAD_ALLOC           , "EGL failed to allocate resources for the requested operation.", },
	{ EGL_BAD_ATTRIBUTE       , "An unrecognized attribute or attribute value was passed in the attribute list.", },
	{ EGL_BAD_CONFIG          , "An EGLConfig argument does not name a valid EGL frame buffer configuration.", },
	{ EGL_BAD_CONTEXT         , "An EGLContext argument does not name a valid EGL rendering context.", },
	{ EGL_BAD_CURRENT_SURFACE , "The current surface of the calling thread is a window, pixel buffer or pixmap that is no longer valid.", },
	{ EGL_BAD_DISPLAY         , "An EGLDisplay argument does not name a valid EGL display connection.", },
	{ EGL_BAD_MATCH           , "Arguments are inconsistent(for example, a valid context requires buffers not supplied by a valid surface).", },
	{ EGL_BAD_NATIVE_PIXMAP   , "A NativePixmapType argument does not refer to a valid native pixmap.", },
	{ EGL_BAD_NATIVE_WINDOW   , "A NativeWindowType argument does not refer to a valid native window.", },
	{ EGL_BAD_PARAMETER       , "One or more argument values are invalid.", },
	{ EGL_BAD_SURFACE         , "An EGLSurface argument does not name a valid surface(window, pixel buffer or pixmap) configured for GL rendering.", },
	{ EGL_CONTEXT_LOST        , "A power management event has occurred.The application must destroy all contexts and reinitialise OpenGL ES state and objects to continue rendering.", },
};

static const size_t __egl_code_arrays_size = MM_ARRAY_SIZE(MY_SURFACE_EGL_ERRORCODE_ARRAYS);

static size_t __static_egl_code_arrays_information_binary_serach(size_t o, size_t l, mm_uint32_t key)
{
	size_t mid = 0;
	size_t left = o;
	size_t right = o + l - 1;
	mm_sint32_t result = 0;

	if (0 == l)
	{
		return (size_t)-1;
	}
	else
	{
		while (left <= right && (size_t)(-1) != right)
		{
			// Avoid overflow problems
			mid = left + (right - left) / 2;

			result = MY_SURFACE_EGL_ERRORCODE_ARRAYS[mid].code - key;

			if (result > 0)
			{
				right = mid - 1;
			}
			else if (result < 0)
			{
				left = mid + 1;
			}
			else
			{
				return mid;
			}
		}

		return -1;
	}
}

const char* mm_my_surface_egl_error_message(EGLint errorcode)
{
	const char* error_message = "";
	size_t index = 0;

	index = __static_egl_code_arrays_information_binary_serach(0, __egl_code_arrays_size, errorcode);

	if ((size_t)(-1) != index && index < __egl_code_arrays_size)
	{
		error_message = MY_SURFACE_EGL_ERRORCODE_ARRAYS[index].message;
	}
	else
	{
		error_message = "unknown mistake.";
	}
	return error_message;
}

static void __static_logger_egl_error(struct mm_logger* p, mm_uint32_t lvl, EGLint errorcode)
{
	const char* error_message = mm_my_surface_egl_error_message(errorcode);
	mm_logger_message(p, lvl, "error(%u):%s", errorcode, error_message);
}

// #define EGL_BUFFER_SIZE              0x3020
// #define EGL_ALPHA_SIZE               0x3021
// #define EGL_BLUE_SIZE                0x3022
// #define EGL_GREEN_SIZE               0x3023
// #define EGL_RED_SIZE                 0x3024
// #define EGL_DEPTH_SIZE               0x3025
// #define EGL_STENCIL_SIZE             0x3026
// #define EGL_CONFIG_CAVEAT            0x3027
// #define EGL_CONFIG_ID                0x3028
// #define EGL_LEVEL                    0x3029
// #define EGL_MAX_PBUFFER_HEIGHT       0x302A
// #define EGL_MAX_PBUFFER_PIXELS       0x302B
// #define EGL_MAX_PBUFFER_WIDTH        0x302C
// #define EGL_NATIVE_RENDERABLE        0x302D
// #define EGL_NATIVE_VISUAL_ID         0x302E
// #define EGL_NATIVE_VISUAL_TYPE       0x302F
// #define EGL_PRESERVED_RESOURCES      0x3030
// #define EGL_SAMPLES                  0x3031
// #define EGL_SAMPLE_BUFFERS           0x3032
// #define EGL_SURFACE_TYPE             0x3033
// #define EGL_TRANSPARENT_TYPE         0x3034
// #define EGL_TRANSPARENT_BLUE_VALUE   0x3035
// #define EGL_TRANSPARENT_GREEN_VALUE  0x3036
// #define EGL_TRANSPARENT_RED_VALUE    0x3037
// #define EGL_NONE                     0x3038	/* Attrib list terminator */
// #define EGL_BIND_TO_TEXTURE_RGB      0x3039
// #define EGL_BIND_TO_TEXTURE_RGBA     0x303A
// #define EGL_MIN_SWAP_INTERVAL        0x303B
// #define EGL_MAX_SWAP_INTERVAL        0x303C
// #define EGL_LUMINANCE_SIZE           0x303D
// #define EGL_ALPHA_MASK_SIZE          0x303E
// #define EGL_COLOR_BUFFER_TYPE        0x303F
// #define EGL_RENDERABLE_TYPE          0x3040
// #define EGL_MATCH_NATIVE_PIXMAP      0x3041	/* Pseudo-attribute (not queryable) */
// #define EGL_CONFORMANT               0x3042

const struct mm_my_surface_egl_config_attribute MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS[] =
{
	{ EGL_BUFFER_SIZE , "EGL_BUFFER_SIZE", "color buffer bits, r+g+b+ alpha", },
	{ EGL_ALPHA_SIZE , "EGL_ALPHA_SIZE", "bits for alpha", },
	{ EGL_BLUE_SIZE , "EGL_BLUE_SIZE", "blue color bits", },
	{ EGL_GREEN_SIZE , "EGL_GREEN_SIZE", "green color bits", },
	{ EGL_RED_SIZE , "EGL_RED_SIZE", "red color bits", },
	{ EGL_DEPTH_SIZE , "EGL_DEPTH_SIZE", "z buffer depth", },
	{ EGL_STENCIL_SIZE , "EGL_STENCIL_SIZE", "stancil bits per pixel", },
	{ EGL_CONFIG_CAVEAT , "EGL_CONFIG_CAVEAT", "side effect of the config, EGL_NONE(0x3038), EGL_SLOW_CONFIG(0x3050), EGL_NON_COMFORMANT_CONFIG(0x3051, native optimized, but failed to EGL standard comformant test)", },
	{ EGL_CONFIG_ID , "EGL_CONFIG_ID", "given config ID", },
	{ EGL_LEVEL , "EGL_LEVEL", "0 is defalt, <0 underlay, >0 overlay", },
	{ EGL_MAX_PBUFFER_HEIGHT , "EGL_MAX_PBUFFER_HEIGHT", "maximum pixels in y direction", },
	{ EGL_MAX_PBUFFER_PIXELS , "EGL_MAX_PBUFFER_PIXELS", "maximum pixels in a pbuffer,  maybe not max_width * max_height", },
	{ EGL_MAX_PBUFFER_WIDTH , "EGL_MAX_PBUFFER_WIDTH", "maximum pixels in x direction", },
	{ EGL_NATIVE_RENDERABLE , "EGL_NATIVE_RENDERABLE", "native API (GDI) can draw on EGL surface", },
	{ EGL_NATIVE_VISUAL_ID , "EGL_NATIVE_VISUAL_ID", "native visual (pixel format) ID", },
	{ EGL_NATIVE_VISUAL_TYPE , "EGL_NATIVE_VISUAL_TYPE", "native visual type (PIXELFORMAT) ?", },
	{ EGL_PRESERVED_RESOURCES , "EGL_PRESERVED_RESOURCES", "egl context preserved resources", },
	{ EGL_SAMPLES , "EGL_SAMPLES", "sample count required by a multisampling buffer. 0 if none sample buffer", },
	{ EGL_SAMPLE_BUFFERS , "EGL_SAMPLE_BUFFERS", "number of multisampling buffers", },
	{ EGL_SURFACE_TYPE , "EGL_SURFACE_TYPE", "supported surface type in config, EGL_WINDOW_BIT(1) | EGL_PIXMAP_BIT(2) | EGL_PBUFFER_BIT(4)", },
	{ EGL_TRANSPARENT_TYPE , "EGL_TRANSPARENT_TYPE", "support a special color as transparent. EGL_NONE(0x3038), EGL_TRANSPARENT_RGB(0x3052)", },
	{ EGL_TRANSPARENT_BLUE_VALUE , "EGL_TRANSPARENT_BLUE_VALUE", "blue component of transparent color", },
	{ EGL_TRANSPARENT_GREEN_VALUE , "EGL_TRANSPARENT_GREEN_VALUE", "green component of transparent color", },
	{ EGL_TRANSPARENT_RED_VALUE , "EGL_TRANSPARENT_RED_VALUE", "red component of transparent color", },
	// EGL 1.1
	/* Attrib list terminator */
	{ EGL_NONE , "EGL_NONE", "none", },

	{ EGL_BIND_TO_TEXTURE_RGB , "EGL_BIND_TO_TEXTURE_RGB", "true if bindalble to RGB texture", },
	{ EGL_BIND_TO_TEXTURE_RGBA , "EGL_BIND_TO_TEXTURE_RGBA", "true if bindalbe to RGBA texture", },
	{ EGL_MIN_SWAP_INTERVAL , "EGL_MIN_SWAP_INTERVAL", "minimum swap interval", },
	{ EGL_MAX_SWAP_INTERVAL , "EGL_MAX_SWAP_INTERVAL", "maximum swap interval", },
	{ EGL_LUMINANCE_SIZE , "EGL_LUMINANCE_SIZE", "luminance size", },
	{ EGL_ALPHA_MASK_SIZE , "EGL_ALPHA_MASK_SIZE", "alpha mask size", },
	{ EGL_COLOR_BUFFER_TYPE , "EGL_COLOR_BUFFER_TYPE", "color buffer type", },
	{ EGL_RENDERABLE_TYPE , "EGL_RENDERABLE_TYPE", "renderable type", },
	/* Pseudo-attribute (not queryable) */
	{ EGL_MATCH_NATIVE_PIXMAP , "EGL_MATCH_NATIVE_PIXMAP", "match native pixmap", },
	{ EGL_CONFORMANT , "EGL_CONFORMANT", "conformant", },
};
static const size_t __egl_config_attribute_arrays_size = MM_ARRAY_SIZE(MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS);

static size_t __static_egl_config_attribute_arrays_information_binary_serach(size_t o, size_t l, mm_uint32_t key)
{
	size_t mid = 0;
	size_t left = o;
	size_t right = o + l - 1;
	mm_sint32_t result = 0;

	if (0 == l)
	{
		return (size_t)-1;
	}
	else
	{
		while (left <= right && (size_t)(-1) != right)
		{
			// Avoid overflow problems
			mid = left + (right - left) / 2;

			result = MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS[mid].id - key;

			if (result > 0)
			{
				right = mid - 1;
			}
			else if (result < 0)
			{
				left = mid + 1;
			}
			else
			{
				return mid;
			}
		}

		return -1;
	}
}

const struct mm_my_surface_egl_config_attribute* mm_my_surface_egl_config_attribute(EGLint id)
{
	const struct mm_my_surface_egl_config_attribute* config_attribute = NULL;
	size_t index = 0;

	index = __static_egl_config_attribute_arrays_information_binary_serach(0, __egl_config_attribute_arrays_size, id);

	if ((size_t)(-1) != index && index < __egl_config_attribute_arrays_size)
	{
		config_attribute = &MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS[index];
	}
	else
	{
		config_attribute = NULL;
	}
	return config_attribute;
}
void mm_my_surface_egl_config_description_describe(struct mm_my_surface_egl_config_description* p, EGLDisplay _EGLDisplay, EGLConfig _EGLConfig)
{
	int i = 0;
	const struct mm_my_surface_egl_config_attribute* config_attribute = NULL;

	for (i = 0; i < __egl_config_attribute_arrays_size; ++i)
	{
		config_attribute = &MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS[i];
		eglGetConfigAttrib(_EGLDisplay, _EGLConfig, config_attribute->id, &p->attributes[i]);
	}
}

// Return attribute values description of all config in the parameter configs
void mm_my_surface_egl_describe_configs(EGLDisplay _EGLDisplay, int num, EGLConfig* configs, struct mm_my_surface_egl_config_description* config_description)
{
	int i = 0;

	for (i = 0; i < num; ++i)
	{
		mm_my_surface_egl_config_description_describe(&config_description[i], _EGLDisplay, configs[i]);
	}
}

void mm_my_surface_egl_dump_eglConfig(EGLDisplay _EGLDisplay, const char* filename)
{
	struct mm_logger* g_logger = mm_logger_instance();

	int i = 0;
	int j = 0;
	int num = 0;
	EGLConfig* configs = NULL;
	FILE* fp = NULL;

	struct mm_my_surface_egl_config_description* config_description = NULL;
	const struct mm_my_surface_egl_config_attribute* config_attribute = NULL;

	do
	{
		if (EGL_FALSE == eglGetConfigs(_EGLDisplay, NULL, 0, &num))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglGetConfigs failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}

		if (0 == num)
		{
			mm_logger_log_E(g_logger, "%s %d eglGetConfigs the number is 0.", __FUNCTION__, __LINE__);
			break;
		}

		// get configs
		configs = (EGLConfig*)mm_malloc(sizeof(EGLConfig) * num);
		config_description = (struct mm_my_surface_egl_config_description*)mm_malloc(sizeof(struct mm_my_surface_egl_config_description) * num);

		mm_memset(configs, 0, sizeof(EGLConfig) * num);
		mm_memset(config_description, 0, sizeof(struct mm_my_surface_egl_config_description) * num);

		if (EGL_FALSE == eglGetConfigs(_EGLDisplay, configs, num, &num))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglGetConfigs failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}

		fp = fopen(filename, "wb");
		if (NULL == fp)
		{
			mm_logger_log_E(g_logger, "%s %d fopen: %s failure.", __FUNCTION__, __LINE__, filename);
			break;
		}

		// export config details
		mm_my_surface_egl_describe_configs(_EGLDisplay, num, configs, config_description);

		// print titles
		fprintf(fp, "configs, ");
		for (i = 0; i < __egl_config_attribute_arrays_size; ++i)
		{
			config_attribute = &MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS[i];
			fprintf(fp, "\"%s\", ", config_attribute->name);
		}
		fprintf(fp, "\n");

		// print value.
		for (j = 0; j < num; ++j)
		{
			fprintf(fp, "\"config %p\", ", (void*)(configs[j]));

			for (i = 0; i < __egl_config_attribute_arrays_size; ++i)
			{
				fprintf(fp, "0x%x, ", config_description[j].attributes[i]);
			}

			fprintf(fp, "\n");
		}

		fclose(fp);
	} while (0);

	if (NULL != config_description)
	{
		mm_free(config_description);
		config_description = NULL;
	}
	if (NULL != configs)
	{
		mm_free(configs);
		configs = NULL;
	}
}

void mm_my_surface_egl_init(struct mm_my_surface_egl* p)
{
	p->hWnd = NULL;

	p->mNativeDisplay = NULL;

	p->mEGLConfig = NULL;
	p->mEGLSurface = EGL_NO_SURFACE;
	p->mEGLContext = EGL_NO_CONTEXT;
	p->mEGLDisplay = EGL_NO_DISPLAY;

	p->majorVersion = 0;
	p->minorVersion = 0;
}
void mm_my_surface_egl_destroy(struct mm_my_surface_egl* p)
{
	p->hWnd = NULL;

	p->mNativeDisplay = NULL;

	p->mEGLConfig = NULL;
	p->mEGLSurface = EGL_NO_SURFACE;
	p->mEGLContext = EGL_NO_CONTEXT;
	p->mEGLDisplay = EGL_NO_DISPLAY;

	p->majorVersion = 0;
	p->minorVersion = 0;
}
void mm_my_surface_egl_assign_hwnd(struct mm_my_surface_egl* p, HWND hWnd)
{
	p->hWnd = hWnd;
}
void mm_my_surface_egl_on_finish_launching(struct mm_my_surface_egl* p)
{
	static const EGLint attribs[] =
	{
		EGL_LEVEL             , 0,
		EGL_DEPTH_SIZE        , 16,
		EGL_SURFACE_TYPE      , EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE   , EGL_OPENGL_ES2_BIT,
		EGL_NATIVE_RENDERABLE , EGL_FALSE,
		EGL_DEPTH_SIZE        , EGL_DONT_CARE,
		EGL_NONE,
	};

	//static const EGLint attribs[] =
	//{
	//	EGL_SURFACE_TYPE,   EGL_WINDOW_BIT,
	//	EGL_RED_SIZE,       5,
	//	EGL_GREEN_SIZE,     6,
	//	EGL_BLUE_SIZE,      5,
	//	EGL_ALPHA_SIZE,     EGL_DONT_CARE,
	//	EGL_DEPTH_SIZE,     16,
	//	EGL_STENCIL_SIZE,   EGL_DONT_CARE,
	//	EGL_NONE,
	//};

	static const EGLint attr[] =
	{
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE,
	};

	EGLint format = 0;
	EGLint numConfigs = 0;

	EGLint _winWidth = 0;
	EGLint _winHeight = 0;

	struct mm_logger* g_logger = mm_logger_instance();

	do
	{
		p->mNativeDisplay = GetDC(p->hWnd);
		p->mEGLDisplay = eglGetDisplay(p->mNativeDisplay);

		// fallback for some emulations 
		if (EGL_NO_DISPLAY == p->mEGLDisplay)
		{
			p->mEGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		}

		if (EGL_NO_DISPLAY == p->mEGLDisplay)
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglGetDisplay failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglGetDisplay success.", __FUNCTION__, __LINE__);
		}

		if (EGL_FALSE == eglInitialize(p->mEGLDisplay, &p->majorVersion, &p->minorVersion))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglInitialize failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglInitialize success.", __FUNCTION__, __LINE__);
		}

		mm_logger_log_I(g_logger, "%s %d eglInitialize version: %u.%u.", __FUNCTION__, __LINE__, p->majorVersion, p->minorVersion);

		if (EGL_FALSE == eglBindAPI(EGL_OPENGL_ES_API))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglBindAPI failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglBindAPI success.", __FUNCTION__, __LINE__);
		}

		mm_my_surface_egl_dump_eglConfig(p->mEGLDisplay, "config_attribute.csv");

		if (EGL_FALSE == eglChooseConfig(p->mEGLDisplay, attribs, &p->mEGLConfig, 1, &numConfigs))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglChooseConfig failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglChooseConfig success.", __FUNCTION__, __LINE__);
		}

		p->mEGLSurface = eglCreateWindowSurface(p->mEGLDisplay, p->mEGLConfig, p->hWnd, NULL);
		if (EGL_NO_SURFACE == p->mEGLSurface)
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglCreateWindowSurface failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglCreateWindowSurface success.", __FUNCTION__, __LINE__);
		}

		p->mEGLContext = eglCreateContext(p->mEGLDisplay, p->mEGLConfig, EGL_NO_CONTEXT, attr);
		if (EGL_NO_CONTEXT == p->mEGLContext)
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglCreateContext failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglCreateContext success.", __FUNCTION__, __LINE__);
		}

		if (EGL_FALSE == eglMakeCurrent(p->mEGLDisplay, p->mEGLSurface, p->mEGLSurface, p->mEGLContext))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglMakeCurrent failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			break;
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglMakeCurrent success.", __FUNCTION__, __LINE__);
		}

		eglQuerySurface(p->mEGLDisplay, p->mEGLSurface, EGL_WIDTH, &_winWidth);
		eglQuerySurface(p->mEGLDisplay, p->mEGLSurface, EGL_HEIGHT, &_winHeight);

		mm_logger_log_I(g_logger, "%s %d EGLInitialize %dx%d success.", __FUNCTION__, __LINE__, _winWidth, _winHeight);
	} while (0);
}
void mm_my_surface_egl_on_before_terminate(struct mm_my_surface_egl* p)
{
	struct mm_logger* g_logger = mm_logger_instance();

	do
	{
		if (EGL_NO_DISPLAY == p->mEGLDisplay)
		{
			break;
		}

		eglMakeCurrent(p->mEGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

		if (EGL_NO_CONTEXT != p->mEGLContext)
		{
			if (EGL_FALSE == eglDestroyContext(p->mEGLDisplay, p->mEGLContext))
			{
				EGLint errorcode = eglGetError();
				mm_logger_log_E(g_logger, "%s %d eglDestroyContext failure.", __FUNCTION__, __LINE__);
				__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			}
			else
			{
				mm_logger_log_I(g_logger, "%s %d eglDestroyContext success.", __FUNCTION__, __LINE__);
			}
			p->mEGLContext = EGL_NO_CONTEXT;
		}

		if (EGL_NO_CONTEXT != p->mEGLSurface)
		{
			if (EGL_FALSE == eglDestroySurface(p->mEGLDisplay, p->mEGLSurface))
			{
				EGLint errorcode = eglGetError();
				mm_logger_log_E(g_logger, "%s %d eglDestroySurface failure.", __FUNCTION__, __LINE__);
				__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
			}
			else
			{
				mm_logger_log_I(g_logger, "%s %d eglDestroySurface success.", __FUNCTION__, __LINE__);
			}
			p->mEGLSurface = EGL_NO_CONTEXT;
		}

		// EGLConfig is a weak ref.
		p->mEGLConfig = NULL;

		if (EGL_FALSE == eglTerminate(p->mEGLDisplay))
		{
			EGLint errorcode = eglGetError();
			mm_logger_log_E(g_logger, "%s %d eglTerminate failure.", __FUNCTION__, __LINE__);
			__static_logger_egl_error(g_logger, MM_LOG_ERROR, errorcode);
		}
		else
		{
			mm_logger_log_I(g_logger, "%s %d eglTerminate success.", __FUNCTION__, __LINE__);
		}		;
		p->mEGLDisplay = NULL;

		mm_logger_log_I(g_logger, "%s %d EGLDestroy success.", __FUNCTION__, __LINE__);
	} while (0);
}
