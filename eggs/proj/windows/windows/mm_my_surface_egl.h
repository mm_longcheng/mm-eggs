#ifndef __mm_my_surface_egl_h__
#define __mm_my_surface_egl_h__

#include "core/mm_core.h"

#include <EGL/egl.h>

#include "core/mm_prefix.h"

struct mm_my_surface_egl_error_string
{
	EGLint code;
	const char* message;
};
extern const struct mm_my_surface_egl_error_string MY_SURFACE_EGL_ERRORCODE_ARRAYS[];

extern const char* mm_my_surface_egl_error_message(EGLint errorcode);

#define MM_MY_SURFACE_EGL_MAX_ATTRIBUTES_OF_CONFIG 64

struct mm_my_surface_egl_config_attribute
{
	EGLint id;
	const char* name;
	const char* explain;
};
extern const struct mm_my_surface_egl_config_attribute MY_SURFACE_EGL_CONFIG_ATTRIBUTE_ARRAYS[];

const struct mm_my_surface_egl_config_attribute* mm_my_surface_egl_config_attribute(EGLint id);

struct mm_my_surface_egl_config_description
{
	EGLint attributes[MM_MY_SURFACE_EGL_MAX_ATTRIBUTES_OF_CONFIG];
};
extern void mm_my_surface_egl_config_description_describe(struct mm_my_surface_egl_config_description* p, EGLDisplay _EGLDisplay, EGLConfig _EGLConfig);

extern void mm_my_surface_egl_describe_configs(EGLDisplay _EGLDisplay, int num, EGLConfig* configs, struct mm_my_surface_egl_config_description* config_description);
extern void mm_my_surface_egl_dump_eglConfig(EGLDisplay _EGLDisplay, const char* filename);

struct mm_my_surface_egl
{
	// strong ref.
	HWND hWnd;
	// HDC Native display.
	HDC mNativeDisplay;
	// EGL
	EGLConfig mEGLConfig;
	EGLSurface mEGLSurface;
	EGLContext mEGLContext;
	EGLDisplay mEGLDisplay;

	EGLint majorVersion;
	EGLint minorVersion;
};
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_egl_init(struct mm_my_surface_egl* p);
extern void mm_my_surface_egl_destroy(struct mm_my_surface_egl* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_egl_assign_hwnd(struct mm_my_surface_egl* p, HWND hWnd);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_egl_on_finish_launching(struct mm_my_surface_egl* p);
extern void mm_my_surface_egl_on_before_terminate(struct mm_my_surface_egl* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_my_surface_egl_h__