#ifndef __mm_my_surface_view_h__
#define __mm_my_surface_view_h__

#include "core/mm_core.h"
#include "core/mm_adaptive_timer.h"

#include "mm_my_surface_define.h"
#include "mm_my_surface_egl.h"

#include "core/mm_prefix.h"

#define MM_MY_SURFACE_VIEW_X_DEFAULT 0
#define MM_MY_SURFACE_VIEW_Y_DEFAULT 0
// 1920 * 0.6
#define MM_MY_SURFACE_VIEW_W_DEFAULT 1152
// 1080 * 0.6
#define MM_MY_SURFACE_VIEW_H_DEFAULT 648

//////////////////////////////////////////////////////////////////////////
extern const char* MM_MY_SURFACE_VIEW_TIMER_SCHEDULE_LOGICAL;
extern const char* MM_MY_SURFACE_VIEW_TIMER_SCHEDULE_DISPLAY;
//////////////////////////////////////////////////////////////////////////
struct mm_my_surface_view
{
	struct mm_adaptive_timer adaptive_timer;
	struct mm_my_surface_egl my_surface_egl;

	char lpWindowName[MM_MAX_LOADSTRING];
	DWORD dwExStyle;
	DWORD dwStyle;
	int window_x;
	int window_y;
	int window_w;
	int window_h;
	HWND hWndParent;
	HMENU hMenu;
	LPVOID lpParam;

	double logical_frequency;
	double display_frequency;

	int drawable_x;
	int drawable_y;
	int drawable_w;
	int drawable_h;

	// strong ref.
	HWND hWnd;
	// weak ref.
	HACCEL hAccelTable;

	HINSTANCE hInstance;
	int nCmdShow;

	// thread.
	pthread_t poll_thread;
	// mm_thread_state_t,default is MM_TS_CLOSED(0)
	mm_sint8_t state;
};
extern void mm_my_surface_view_init(struct mm_my_surface_view* p);
extern void mm_my_surface_view_destroy(struct mm_my_surface_view* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_assign_window_name(struct mm_my_surface_view* p, const char* window_name);
extern void mm_my_surface_view_assign_instance(struct mm_my_surface_view* p, HINSTANCE hInstance);
extern void mm_my_surface_view_assign_cmdshow(struct mm_my_surface_view* p, int nCmdShow);
//
extern void mm_my_surface_view_assign_wnd_parent(struct mm_my_surface_view* p, HWND hWndParent);
extern void mm_my_surface_view_assign_accelerators(struct mm_my_surface_view* p, HACCEL hAccelTable);
//
extern void mm_my_surface_view_assign_rect(struct mm_my_surface_view* p, int x, int y, int w, int h);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_on_update_size_change(struct mm_my_surface_view* p, int x, int y, int w, int h);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_on_finish_launching(struct mm_my_surface_view* p);
extern void mm_my_surface_view_on_before_terminate(struct mm_my_surface_view* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_on_update(struct mm_my_surface_view* p);
extern void mm_my_surface_view_on_timewait(struct mm_my_surface_view* p);
extern void mm_my_surface_view_on_timewake(struct mm_my_surface_view* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_poll_wait(struct mm_my_surface_view* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_on_logical_one_frame(struct mm_my_surface_view* p, double interval);
extern void mm_my_surface_view_on_display_one_frame(struct mm_my_surface_view* p, double interval);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_view_start(struct mm_my_surface_view* p);
extern void mm_my_surface_view_interrupt(struct mm_my_surface_view* p);
extern void mm_my_surface_view_shutdown(struct mm_my_surface_view* p);
extern void mm_my_surface_view_join(struct mm_my_surface_view* p);
//////////////////////////////////////////////////////////////////////////
extern ATOM mm_my_surface_view_register_class(HINSTANCE hInstance);
extern BOOL mm_my_surface_view_unregister_class(HINSTANCE hInstance);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_my_surface_view_h__