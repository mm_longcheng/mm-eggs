#ifndef __mm_my_surface_interface_h__
#define __mm_my_surface_interface_h__

#include "core/mm_core.h"

#include "math/mm_vector4.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
struct mm_my_surface_content_keypad
{
	int key;
	int text;
};

struct mm_my_surface_content_cursor
{
	// motion event button mask.the state of all buttons that are pressed such as a mouse or stylus button.
	int button_mask;
	// absolute position x.
	double abs_x;
	// absolute position y.
	double abs_y;
	// absolute position z.MouseWheel if mouse data.
	double abs_z;
	// relative position x.
	double rel_x;
	// relative position y.
	double rel_y;
	// relative position z.MouseWheel if mouse data.
	double rel_z;
	//the UTC time of this fix, in milliseconds since January 1, 1970.
	// The time (in ms) when event trigger.
	mm_uint64_t timestamp;
};

enum mm_my_surface_content_touch_phase
{
	MM_SURFACE_TOUCH_BEGAN = 0,
	MM_SURFACE_TOUCH_MOVED = 1,
	MM_SURFACE_TOUCH_ENDED = 2,
	MM_SURFACE_TOUCH_CANCEL = 3,
};

struct mm_my_surface_content_touch
{
	// motion event weak ref.
	int motion_id;
	// The number of times the finger was tapped for this given touch.
	int tap_count;
	// The phase of the touch.touch_phase touch began, moved, ended, or was canceled.
	int phase;
	// motion event button mask.he state of all buttons that are pressed such as a mouse or stylus button.
	int button_mask;
	// absolute position x.
	double abs_x;
	// absolute position y.
	double abs_y;
	// absolute position z.MouseWheel if mouse data.
	double abs_z;
	// The force of the touch, where a value of 1.0 represents the force of an average touch (predetermined by the system, not user-specific).
	double force_value;
	// The maximum possible force for a touch.
	double force_maximum;
	// The major radius (in points) of the touch.
	double major_radius;
	// The minor radius (in points) of the touch.
	double minor_radius;
	// The area size value, (0, 1) an abstract metric.
	double size_value;
	//the UTC time of this fix, in milliseconds since January 1, 1970.
	// The time (in ms) when event trigger.
	mm_uint64_t timestamp;
};
struct mm_my_surface_content_touchs
{
	size_t size;// size of touchs.
	struct mm_my_surface_content_touch* touchs;// weak ref.
};

enum
{
	MM_SURFACE_SOFT_INPUT_STATE_SHOW = 0,// show soft input. 
	MM_SURFACE_SOFT_INPUT_STATE_HIDE = 1,// hide soft input.
};

struct mm_my_surface_content_keypad_status
{
	// layer:
	// screen<-window trigger window attach to screen.
	// extern         extern screen brothers.
	struct mm_vector4 screen_rect;
	struct mm_vector4 extern_rect;
	struct mm_vector4 window_rect;
	void* surface;
	mm_uint32_t state;
};

struct mm_my_surface_interface
{
	void(*on_file_drop_enter)(struct mm_my_surface_interface* p, const char* filename);

	void(*on_enter_background)(struct mm_my_surface_interface* p);
	void(*on_enter_foreground)(struct mm_my_surface_interface* p);

	void(*on_keypad_pressed)(struct mm_my_surface_interface* p, struct mm_my_surface_content_keypad* content);
	void(*on_keypad_release)(struct mm_my_surface_interface* p, struct mm_my_surface_content_keypad* content);

	void(*on_keypad_system_keys)(struct mm_my_surface_interface* p, struct mm_my_surface_content_keypad* content);

	void(*on_keypad_status)(struct mm_my_surface_interface* p, struct mm_my_surface_content_keypad_status* content);

	void(*on_cursor_moved)(struct mm_my_surface_interface* p, struct mm_my_surface_content_cursor* content);
	void(*on_cursor_began)(struct mm_my_surface_interface* p, struct mm_my_surface_content_cursor* content);
	void(*on_cursor_ended)(struct mm_my_surface_interface* p, struct mm_my_surface_content_cursor* content);
	void(*on_cursor_break)(struct mm_my_surface_interface* p, struct mm_my_surface_content_cursor* content);

	void(*on_touchs_began)(struct mm_my_surface_interface* p, struct mm_my_surface_content_touchs* content);
	void(*on_touchs_moved)(struct mm_my_surface_interface* p, struct mm_my_surface_content_touchs* content);
	void(*on_touchs_ended)(struct mm_my_surface_interface* p, struct mm_my_surface_content_touchs* content);
	void(*on_touchs_break)(struct mm_my_surface_interface* p, struct mm_my_surface_content_touchs* content);

	void(*on_window_size_changed)(struct mm_my_surface_interface* p, float w, float h);
	void(*on_canvas_size_changed)(struct mm_my_surface_interface* p, float w, float h);

	void(*on_updated_logical)(struct mm_my_surface_interface* p, double interval);
	void(*on_updated_display)(struct mm_my_surface_interface* p, double interval);
};

extern void mm_my_surface_interface_init(struct mm_my_surface_interface* p);
extern void mm_my_surface_interface_destroy(struct mm_my_surface_interface* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_my_surface_interface_h__