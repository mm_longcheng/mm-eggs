#include "mm_my_application.h"
#include "mm_my_surface_main.h"
#include "mm_my_surface_view.h"

//////////////////////////////////////////////////////////////////////////
void mm_my_parameter_init(struct mm_my_parameter* p)
{
	p->hInstance = NULL;
	p->hPrevInstance = NULL;
	p->lpCmdLine = NULL;
	p->nCmdShow = 0;
}
void mm_my_parameter_destroy(struct mm_my_parameter* p)
{
	p->hInstance = NULL;
	p->hPrevInstance = NULL;
	p->lpCmdLine = NULL;
	p->nCmdShow = 0;
}
//////////////////////////////////////////////////////////////////////////
void mm_my_application_init(struct mm_my_application* p)
{
	mm_my_parameter_init(&p->my_parameter);
	mm_my_surface_main_init(&p->my_surface_main);
}
void mm_my_application_destroy(struct mm_my_application* p)
{
	mm_my_surface_main_destroy(&p->my_surface_main);
	mm_my_parameter_destroy(&p->my_parameter);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_application_assign_instance(struct mm_my_application* p, HINSTANCE hInstance)
{
	p->my_parameter.hInstance = hInstance;
	//
	mm_my_surface_main_assign_instance(&p->my_surface_main, hInstance);
}
void mm_my_application_assign_prev_instance(struct mm_my_application* p, HINSTANCE hPrevInstance)
{
	p->my_parameter.hPrevInstance = hPrevInstance;
}
void mm_my_application_assign_cmd_line(struct mm_my_application* p, LPWSTR lpCmdLine)
{
	p->my_parameter.lpCmdLine = lpCmdLine;
}
void mm_my_application_assign_cmd_show(struct mm_my_application* p, int nCmdShow)
{
	p->my_parameter.nCmdShow = nCmdShow;
	//
	mm_my_surface_main_assign_cmdshow(&p->my_surface_main, nCmdShow);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_application_finish_launching(struct mm_my_application* p)
{
	mm_my_surface_main_on_finish_launching(&p->my_surface_main);
}
void mm_my_application_before_terminate(struct mm_my_application* p)
{
	mm_my_surface_main_on_before_terminate(&p->my_surface_main);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_application_start(struct mm_my_application* p)
{
	mm_my_surface_main_start(&p->my_surface_main);
}
void mm_my_application_interrupt(struct mm_my_application* p)
{
	mm_my_surface_main_interrupt(&p->my_surface_main);
}
void mm_my_application_shutdown(struct mm_my_application* p)
{
	mm_my_surface_main_shutdown(&p->my_surface_main);
}
void mm_my_application_join(struct mm_my_application* p)
{
	mm_my_surface_main_join(&p->my_surface_main);
}
//////////////////////////////////////////////////////////////////////////
ATOM mm_my_application_register_class(HINSTANCE hInstance)
{
	mm_my_surface_main_register_class(hInstance);
	mm_my_surface_view_register_class(hInstance);
	return 1;
}
BOOL mm_my_application_unregister_class(HINSTANCE hInstance)
{
	mm_my_surface_main_unregister_class(hInstance);
	mm_my_surface_view_unregister_class(hInstance);
	return TRUE;
}
//////////////////////////////////////////////////////////////////////////
