#ifndef __mm_my_context_h__
#define __mm_my_context_h__

#include "core/mm_core.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
struct mm_my_context
{
	HINSTANCE hInstance;
};
extern void mm_my_context_init(struct mm_my_context* p);
extern void mm_my_context_destroy(struct mm_my_context* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_context_assign_instance(struct mm_my_context* p, HINSTANCE hInstance);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_context_on_finish_launching(struct mm_my_context* p);
extern void mm_my_context_on_before_terminate(struct mm_my_context* p);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_my_context_h__