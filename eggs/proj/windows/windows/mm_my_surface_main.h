#ifndef __mm_my_surface_main_h__
#define __mm_my_surface_main_h__

#include "core/mm_core.h"
#include "core/mm_frame_timer.h"

#include "mm_my_surface_define.h"
#include "mm_my_surface_view.h"

#include "core/mm_prefix.h"

#define MM_MY_SURFACE_MAIN_X_DEFAULT 0
#define MM_MY_SURFACE_MAIN_Y_DEFAULT 0
// 1920 * 0.6
#define MM_MY_SURFACE_MAIN_W_DEFAULT 1152
// 1080 * 0.6
#define MM_MY_SURFACE_MAIN_H_DEFAULT 648

//////////////////////////////////////////////////////////////////////////
struct mm_my_surface_main
{
	struct mm_frame_task frame_task;

	char lpWindowName[MM_MAX_LOADSTRING];
	DWORD dwExStyle;
	DWORD dwStyle;
	int window_x;
	int window_y;
	int window_w;
	int window_h;
	HWND hWndParent;
	HMENU hMenu;
	LPVOID lpParam;

	int drawable_x;
	int drawable_y;
	int drawable_w;
	int drawable_h;

	// strong ref.
	HWND hWnd;
	// weak ref.
	HACCEL hAccelTable;

	HINSTANCE hInstance;
	int nCmdShow;

	struct mm_my_surface_view my_surface_view;
	HWND hWndConsole;

	BOOL bConsoleStatus;

	// mm_thread_state_t,default is MM_TS_CLOSED(0)
	mm_sint8_t state;
};
extern void mm_my_surface_main_init(struct mm_my_surface_main* p);
extern void mm_my_surface_main_destroy(struct mm_my_surface_main* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_main_assign_instance(struct mm_my_surface_main* p, HINSTANCE hInstance);
extern void mm_my_surface_main_assign_cmdshow(struct mm_my_surface_main* p, int nCmdShow);
//
extern void mm_my_surface_main_assign_rect(struct mm_my_surface_main* p, int x, int y, int w, int h);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_main_on_update_size_change(struct mm_my_surface_main* p, int x, int y, int w, int h);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_main_on_finish_launching(struct mm_my_surface_main* p);
extern void mm_my_surface_main_on_before_terminate(struct mm_my_surface_main* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_main_alloc_console(struct mm_my_surface_main* p);
extern void mm_my_surface_main_free_console(struct mm_my_surface_main* p);
extern void mm_my_surface_main_switch_console_status(struct mm_my_surface_main* p, BOOL status);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_main_poll_wait(struct mm_my_surface_main* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_surface_main_start(struct mm_my_surface_main* p);
extern void mm_my_surface_main_interrupt(struct mm_my_surface_main* p);
extern void mm_my_surface_main_shutdown(struct mm_my_surface_main* p);
extern void mm_my_surface_main_join(struct mm_my_surface_main* p);
//////////////////////////////////////////////////////////////////////////
extern ATOM mm_my_surface_main_register_class(HINSTANCE hInstance);
extern BOOL mm_my_surface_main_unregister_class(HINSTANCE hInstance);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_my_surface_main_h__