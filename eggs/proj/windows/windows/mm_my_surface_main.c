#include "mm_my_surface_main.h"

#include "core/mm_string.h"
#include "core/mm_thread.h"
#include "core/mm_logger.h"

#include "resource.h"

//////////////////////////////////////////////////////////////////////////
static char __static_my_surface_main_lpClassName[MM_MAX_LOADSTRING] = { 0 };
//////////////////////////////////////////////////////////////////////////
static BOOL __static_my_surface_main_CreateWindow(struct mm_my_surface_main* p);
static void __static_my_surface_main_DestroyWindow(struct mm_my_surface_main* p);
static void __static_my_surface_main_CreateAccelerators(struct mm_my_surface_main* p);
static void __static_my_surface_main_DestroyAccelerators(struct mm_my_surface_main* p);
static void __static_my_surface_main_ShowWindow(struct mm_my_surface_main* p);
static void __static_my_surface_main_PeekMessage(struct mm_my_surface_main* p);
//////////////////////////////////////////////////////////////////////////
static void __static_my_surface_main_OnUpdateSizeCache(struct mm_my_surface_main* p, int x, int y, int w, int h);
static void __static_my_surface_main_OnSizeChange(struct mm_my_surface_main* p, int w, int h);
//////////////////////////////////////////////////////////////////////////
static LRESULT CALLBACK __static_my_surface_main_WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK __static_my_surface_main_About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_main_init(struct mm_my_surface_main* p)
{
	mm_frame_task_init(&p->frame_task);

	mm_memset(p->lpWindowName, 0, sizeof(char) * MM_MAX_LOADSTRING);
	p->dwExStyle = 0;
	p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW;
	p->window_x = MM_MY_SURFACE_MAIN_X_DEFAULT;
	p->window_y = MM_MY_SURFACE_MAIN_Y_DEFAULT;
	p->window_w = MM_MY_SURFACE_MAIN_W_DEFAULT;
	p->window_h = MM_MY_SURFACE_MAIN_H_DEFAULT;
	p->hWndParent = NULL;
	p->hMenu = NULL;
	p->lpParam = NULL;

	p->drawable_x = p->window_x;
	p->drawable_y = p->window_y;
	p->drawable_w = p->window_w;
	p->drawable_h = p->window_h;

	p->hWnd = NULL;
	p->hAccelTable = NULL;

	p->hInstance = NULL;
	p->nCmdShow = 0;

	mm_my_surface_view_init(&p->my_surface_view);
	p->hWndConsole = NULL;

	p->bConsoleStatus = FALSE;

	p->state = MM_TS_CLOSED;

	// Initialize the global string
	LoadString(p->hInstance, IDS_APP_TITLE, p->lpWindowName, MM_MAX_LOADSTRING);
}
void mm_my_surface_main_destroy(struct mm_my_surface_main* p)
{
	mm_frame_task_destroy(&p->frame_task);

	mm_memset(p->lpWindowName, 0, sizeof(char) * MM_MAX_LOADSTRING);
	p->dwExStyle = 0;
	p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW;
	p->window_x = MM_MY_SURFACE_MAIN_X_DEFAULT;
	p->window_y = MM_MY_SURFACE_MAIN_Y_DEFAULT;
	p->window_w = MM_MY_SURFACE_MAIN_W_DEFAULT;
	p->window_h = MM_MY_SURFACE_MAIN_H_DEFAULT;
	p->hWndParent = NULL;
	p->hMenu = NULL;
	p->lpParam = NULL;

	p->drawable_x = p->window_x;
	p->drawable_y = p->window_y;
	p->drawable_w = p->window_w;
	p->drawable_h = p->window_h;

	p->hWnd = NULL;
	p->hAccelTable = NULL;

	p->hInstance = NULL;
	p->nCmdShow = 0;

	mm_my_surface_view_destroy(&p->my_surface_view);
	p->hWndConsole = NULL;

	p->bConsoleStatus = FALSE;

	p->state = MM_TS_CLOSED;
}
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_main_assign_instance(struct mm_my_surface_main* p, HINSTANCE hInstance)
{
	p->hInstance = hInstance;
	mm_my_surface_view_assign_instance(&p->my_surface_view, hInstance);
}
void mm_my_surface_main_assign_cmdshow(struct mm_my_surface_main* p, int nCmdShow)
{
	p->nCmdShow = nCmdShow;
	mm_my_surface_view_assign_cmdshow(&p->my_surface_view, nCmdShow);
}
void mm_my_surface_main_assign_rect(struct mm_my_surface_main* p, int x, int y, int w, int h)
{
	p->drawable_x = x;
	p->drawable_y = y;
	p->drawable_w = w;
	p->drawable_h = h;
}
void mm_my_surface_main_on_update_size_change(struct mm_my_surface_main* p, int x, int y, int w, int h)
{
	__static_my_surface_main_OnUpdateSizeCache(p, x, y, w, h);

	SetWindowPos(p->hWnd, NULL, p->window_x, p->window_y, p->window_w, p->window_h, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_main_on_finish_launching(struct mm_my_surface_main* p)
{
	// load Accelerator Table.
	__static_my_surface_main_CreateAccelerators(p);

	// Perform application initialization.
	__static_my_surface_main_CreateWindow(p);
	// Show window.
	__static_my_surface_main_ShowWindow(p);

	mm_my_surface_main_alloc_console(p);

	// my_surface_view 
	mm_my_surface_view_assign_wnd_parent(&p->my_surface_view, p->hWnd);
	mm_my_surface_view_assign_accelerators(&p->my_surface_view, p->hAccelTable);
	mm_my_surface_view_on_finish_launching(&p->my_surface_view);
}
void mm_my_surface_main_on_before_terminate(struct mm_my_surface_main* p)
{
	mm_my_surface_main_free_console(p);

	// my_surface_view 
	mm_my_surface_view_on_before_terminate(&p->my_surface_view);

	// Destroy Window.
	__static_my_surface_main_DestroyWindow(p);
	// Destroy Accelerator Table.
	__static_my_surface_main_DestroyAccelerators(p);
}
void mm_my_surface_main_alloc_console(struct mm_my_surface_main* p)
{
	AllocConsole();

	p->hWndConsole = GetConsoleWindow();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);

	mm_my_surface_main_switch_console_status(p, p->bConsoleStatus);
}
void mm_my_surface_main_free_console(struct mm_my_surface_main* p)
{
	FreeConsole();
	p->hWndConsole = NULL;
}
void mm_my_surface_main_switch_console_status(struct mm_my_surface_main* p, BOOL status)
{
	p->bConsoleStatus = status;
	if (FALSE == p->bConsoleStatus)
	{
		ShowWindowAsync(p->hWndConsole, SW_HIDE);
		BringWindowToTop(p->hWnd);
	}
	else
	{
		ShowWindowAsync(p->hWndConsole, SW_SHOW);
		BringWindowToTop(p->hWndConsole);
	}
}
void mm_my_surface_main_start(struct mm_my_surface_main* p)
{
	p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
	mm_frame_task_start(&p->frame_task);
	mm_my_surface_view_start(&p->my_surface_view);
}
void mm_my_surface_main_interrupt(struct mm_my_surface_main* p)
{
	p->state = MM_TS_CLOSED;
	mm_frame_task_interrupt(&p->frame_task);
	mm_my_surface_view_interrupt(&p->my_surface_view);
}
void mm_my_surface_main_shutdown(struct mm_my_surface_main* p)
{
	p->state = MM_TS_FINISH;
	mm_frame_task_shutdown(&p->frame_task);
	mm_my_surface_view_shutdown(&p->my_surface_view);
}
void mm_my_surface_main_join(struct mm_my_surface_main* p)
{
	mm_my_surface_main_poll_wait(p);
	//
	mm_frame_task_join(&p->frame_task);
	mm_my_surface_view_join(&p->my_surface_view);
}
ATOM mm_my_surface_main_register_class(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	LoadString(hInstance, IDC_WINDOWSCLASS_SURFACE_MAIN, __static_my_surface_main_lpClassName, MM_MAX_LOADSTRING);

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcex.lpfnWndProc = __static_my_surface_main_WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_48x48));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_WINDOWS_MENU_MAIN);
	wcex.lpszClassName = __static_my_surface_main_lpClassName;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON_32x32));

	return RegisterClassEx(&wcex);
}
BOOL mm_my_surface_main_unregister_class(HINSTANCE hInstance)
{
	return UnregisterClass(__static_my_surface_main_lpClassName, hInstance);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_main_poll_wait(struct mm_my_surface_main* p)
{
	// looper
	while (MM_TS_MOTION == p->state || NULL != p->my_surface_view.hWnd)
	{
		mm_frame_task_update(&p->frame_task);
		mm_frame_task_timewait(&p->frame_task);
		// PeekMessage.
		__static_my_surface_main_PeekMessage(p);
	}
}
//////////////////////////////////////////////////////////////////////////
static BOOL __static_my_surface_main_CreateWindow(struct mm_my_surface_main* p)
{
	RECT rc;
	BOOL bMenu = p->hMenu ? FALSE : TRUE;

	struct mm_logger* g_logger = mm_logger_instance();

	SetRect(&rc, p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);

	AdjustWindowRectEx(&rc, p->dwStyle, bMenu, p->dwExStyle);

	p->window_x = rc.left;
	p->window_y = rc.top;
	p->window_w = rc.right - rc.left;
	p->window_h = rc.bottom - rc.top;

	p->window_x = p->window_x < 0 ? 0 : p->window_x;
	p->window_y = p->window_y < 0 ? 0 : p->window_y;

	p->hWnd = CreateWindowEx(
		p->dwExStyle,
		__static_my_surface_main_lpClassName,
		p->lpWindowName,
		p->dwStyle,
		p->window_x,
		p->window_y,
		p->window_w,
		p->window_h,
		p->hWndParent,
		p->hMenu,
		p->hInstance,
		p->lpParam
	);

	if (!p->hWnd)
	{
		mm_logger_log_F(g_logger, "%s %d CreateWindowEx window((%d, %d), (%d, %d)) drawable((%d, %d), (%d, %d)) failure.", 
			__FUNCTION__, __LINE__, 
			p->window_x, p->window_y, p->window_w, p->window_h, 
			p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);
		return FALSE;
	}
	else
	{
		SetWindowLongPtr(p->hWnd, GWLP_USERDATA, (LONG_PTR)p);
		mm_logger_log_I(g_logger, "%s %d CreateWindowEx window((%d, %d), (%d, %d)) drawable((%d, %d), (%d, %d)) success.", 
			__FUNCTION__, __LINE__, 
			p->window_x, p->window_y, p->window_w, p->window_h,
			p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);
		return TRUE;
	}
}
static void __static_my_surface_main_DestroyWindow(struct mm_my_surface_main* p)
{
	if (NULL != p->hWnd)
	{
		DestroyWindow(p->hWnd);
		p->hWnd = NULL;
	}
}
static void __static_my_surface_main_CreateAccelerators(struct mm_my_surface_main* p)
{
	p->hAccelTable = LoadAccelerators(p->hInstance, MAKEINTRESOURCE(IDC_ACCELERATOR));
}
static void __static_my_surface_main_DestroyAccelerators(struct mm_my_surface_main* p)
{
	DestroyAcceleratorTable(p->hAccelTable);
	p->hAccelTable = NULL;
}
static void __static_my_surface_main_ShowWindow(struct mm_my_surface_main* p)
{
	ShowWindow(p->hWnd, p->nCmdShow);
	UpdateWindow(p->hWnd);
}
static void __static_my_surface_main_PeekMessage(struct mm_my_surface_main* p)
{
	MSG msg;
	// Peek message use PM_REMOVE.
	while (PeekMessage(&msg, p->hWnd, 0U, 0U, PM_REMOVE))
	{
		if (!TranslateAccelerator(msg.hwnd, p->hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
void __static_my_surface_main_OnUpdateSizeCache(struct mm_my_surface_main* p, int x, int y, int w, int h)
{
	RECT rc;
	BOOL bMenu = p->hMenu ? FALSE : TRUE;

	mm_my_surface_main_assign_rect(p, x, y, w, h);

	SetRect(&rc, p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);

	AdjustWindowRectEx(&rc, p->dwStyle, bMenu, p->dwExStyle);

	p->window_x = rc.left;
	p->window_y = rc.top;
	p->window_w = rc.right - rc.left;
	p->window_h = rc.bottom - rc.top;

	p->window_x = p->window_x < 0 ? 0 : p->window_x;
	p->window_y = p->window_y < 0 ? 0 : p->window_y;
}
void __static_my_surface_main_OnSizeChange(struct mm_my_surface_main* p, int w, int h)
{
	__static_my_surface_main_OnUpdateSizeCache(p, p->drawable_x, p->drawable_y, w, h);
	mm_my_surface_view_on_update_size_change(&p->my_surface_view, p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);
}
static LRESULT CALLBACK __static_my_surface_main_WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	struct mm_my_surface_main* p = (struct mm_my_surface_main*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	if (NULL != p)
	{
		HINSTANCE hInstance = p->hInstance;

		switch (message)
		{
		case WM_CLOSE:
		{
			mm_my_surface_main_shutdown(p);
		}
		break;
		case WM_SIZE:
		{
			if (FALSE == IsIconic(hWnd))
			{
				int w = LOWORD(lParam); // width of client area 
				int h = HIWORD(lParam); // Hight of client area 
				__static_my_surface_main_OnSizeChange(p, w, h);
			}
		}
		break;
		case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// Analysis menu selection:
			switch (wmId)
			{
			case IDM_ABOUT:
				DialogBox(hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), p->hWnd, __static_my_surface_main_About);
				break;
			case IDM_FILE_TERMINAL:
				mm_my_surface_main_switch_console_status(p, !p->bConsoleStatus);
				break;
			case IDM_EXIT:
				mm_my_surface_main_shutdown(p);
				break;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code using hdc here...
			EndPaint(hWnd, &ps);
		}
		break;
		case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
		case WM_KEYDOWN:
		{
			mm_printf("WM_KEYDOWN\n");
		}
		break;
		case WM_KEYUP:
		{
			mm_printf("WM_KEYUP\n");
		}
		break;
		default:
		{
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		}
		return 0;
	}
	else
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}
static INT_PTR CALLBACK __static_my_surface_main_About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
//////////////////////////////////////////////////////////////////////////
static void* __static_my_surface_main_poll_wait_thread(void* _arg)
{
	struct mm_my_surface_main* p = (struct mm_my_surface_main*)(_arg);
	mm_my_surface_main_poll_wait(p);
	return NULL;
}
//////////////////////////////////////////////////////////////////////////
