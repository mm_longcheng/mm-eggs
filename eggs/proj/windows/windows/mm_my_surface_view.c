#include "mm_my_surface_view.h"

#include "core/mm_string.h"
#include "core/mm_thread.h"
#include "core/mm_logger.h"

#include "resource.h"

//////////////////////////////////////////////////////////////////////////
const char* MM_MY_SURFACE_VIEW_TIMER_SCHEDULE_LOGICAL = "timer_schedule_logical";
const char* MM_MY_SURFACE_VIEW_TIMER_SCHEDULE_DISPLAY = "timer_schedule_display";
//////////////////////////////////////////////////////////////////////////
static char __static_my_surface_view_lpClassName[MM_MAX_LOADSTRING] = { "MY_SURFACE_VIEW_CLASS" };
//////////////////////////////////////////////////////////////////////////
static BOOL __static_my_surface_view_CreateWindow(struct mm_my_surface_view* p);
static void __static_my_surface_view_DestroyWindow(struct mm_my_surface_view* p);
static void __static_my_surface_view_ShowWindow(struct mm_my_surface_view* p);
static void __static_my_surface_view_PeekMessage(struct mm_my_surface_view* p);
//////////////////////////////////////////////////////////////////////////
static void __static_my_surface_view_OnUpdateSizeCache(struct mm_my_surface_view* p, int x, int y, int w, int h);
static void __static_my_surface_view_OnSizeChange(struct mm_my_surface_view* p, int w, int h);
//////////////////////////////////////////////////////////////////////////
static LRESULT CALLBACK __static_my_surface_view_WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
//////////////////////////////////////////////////////////////////////////
static void* __static_my_surface_view_poll_wait_thread(void* _arg);
//////////////////////////////////////////////////////////////////////////
static void __static_my_surface_view_adaptive_timer_unit_update_logical(void* obj, double interval);
static void __static_my_surface_view_adaptive_timer_unit_update_display(void* obj, double interval);
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_view_init(struct mm_my_surface_view* p)
{
	mm_adaptive_timer_init(&p->adaptive_timer);
	mm_my_surface_egl_init(&p->my_surface_egl);

	mm_memset(p->lpWindowName, 0, sizeof(char) * MM_MAX_LOADSTRING);
	p->dwExStyle = 0;
	p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_CHILDWINDOW;
	p->window_x = MM_MY_SURFACE_VIEW_X_DEFAULT;
	p->window_y = MM_MY_SURFACE_VIEW_Y_DEFAULT;
	p->window_w = MM_MY_SURFACE_VIEW_W_DEFAULT;
	p->window_h = MM_MY_SURFACE_VIEW_H_DEFAULT;
	p->hWndParent = NULL;
	p->hMenu = NULL;
	p->lpParam = NULL;

	p->logical_frequency = 60.0;
	p->display_frequency = 30.0;

	p->drawable_x = p->window_x;
	p->drawable_y = p->window_y;
	p->drawable_w = p->window_w;
	p->drawable_h = p->window_h;

	p->hWnd = NULL;
	p->hAccelTable = NULL;

	p->hInstance = NULL;
	p->nCmdShow = 0;

	p->state = MM_TS_CLOSED;

	// 
	strcpy(p->lpWindowName, "mm_my_surface_view");
}
void mm_my_surface_view_destroy(struct mm_my_surface_view* p)
{
	mm_adaptive_timer_destroy(&p->adaptive_timer);
	mm_my_surface_egl_destroy(&p->my_surface_egl);

	mm_memset(p->lpWindowName, 0, sizeof(char) * MM_MAX_LOADSTRING);
	p->dwExStyle = 0;
	p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_CHILDWINDOW;
	p->window_x = MM_MY_SURFACE_VIEW_X_DEFAULT;
	p->window_y = MM_MY_SURFACE_VIEW_Y_DEFAULT;
	p->window_w = MM_MY_SURFACE_VIEW_W_DEFAULT;
	p->window_h = MM_MY_SURFACE_VIEW_H_DEFAULT;
	p->hWndParent = NULL;
	p->hMenu = NULL;
	p->lpParam = NULL;

	p->logical_frequency = 60.0;
	p->display_frequency = 30.0;

	p->drawable_x = p->window_x;
	p->drawable_y = p->window_y;
	p->drawable_w = p->window_w;
	p->drawable_h = p->window_h;

	p->hWnd = NULL;
	p->hAccelTable = NULL;

	p->hInstance = NULL;
	p->nCmdShow = 0;

	p->state = MM_TS_CLOSED;
}
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_view_assign_window_name(struct mm_my_surface_view* p, const char* window_name)
{
	strcpy(p->lpWindowName, window_name);
}
void mm_my_surface_view_assign_instance(struct mm_my_surface_view* p, HINSTANCE hInstance)
{
	p->hInstance = hInstance;
}
void mm_my_surface_view_assign_cmdshow(struct mm_my_surface_view* p, int nCmdShow)
{
	p->nCmdShow = nCmdShow;
}
void mm_my_surface_view_assign_wnd_parent(struct mm_my_surface_view* p, HWND hWndParent)
{
	p->hWndParent = hWndParent;
}
void mm_my_surface_view_assign_accelerators(struct mm_my_surface_view* p, HACCEL hAccelTable)
{
	p->hAccelTable = hAccelTable;
}
void mm_my_surface_view_assign_rect(struct mm_my_surface_view* p, int x, int y, int w, int h)
{
	p->drawable_x = x;
	p->drawable_y = y;
	p->drawable_w = w;
	p->drawable_h = h;
}
void mm_my_surface_view_on_update_size_change(struct mm_my_surface_view* p, int x, int y, int w, int h)
{
	__static_my_surface_view_OnUpdateSizeCache(p, x, y, w, h);

	SetWindowPos(p->hWnd, NULL, p->window_x, p->window_y, p->window_w, p->window_h, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_view_on_finish_launching(struct mm_my_surface_view* p)
{
	struct mm_adaptive_timer_argument timer_argument;

	struct mm_logger* g_logger = mm_logger_instance();

	timer_argument.name = MM_MY_SURFACE_VIEW_TIMER_SCHEDULE_LOGICAL;
	timer_argument.frequency = p->logical_frequency;
	timer_argument.background = 1;
	timer_argument.handle = &__static_my_surface_view_adaptive_timer_unit_update_logical;
	timer_argument.u = p;
	mm_adaptive_timer_schedule_argument(&p->adaptive_timer, &timer_argument);

	timer_argument.name = MM_MY_SURFACE_VIEW_TIMER_SCHEDULE_DISPLAY;
	timer_argument.frequency = p->display_frequency;
	timer_argument.background = 0;
	timer_argument.handle = &__static_my_surface_view_adaptive_timer_unit_update_display;
	timer_argument.u = p;
	mm_adaptive_timer_schedule_argument(&p->adaptive_timer, &timer_argument);

	mm_logger_log_I(g_logger, "%s %d logical_frequency: %lf.", __FUNCTION__, __LINE__, p->logical_frequency);
	mm_logger_log_I(g_logger, "%s %d display_frequency: %lf.", __FUNCTION__, __LINE__, p->display_frequency);
}
void mm_my_surface_view_on_before_terminate(struct mm_my_surface_view* p)
{

}
void mm_my_surface_view_on_update(struct mm_my_surface_view* p)
{
	mm_adaptive_timer_update(&p->adaptive_timer);
}
void mm_my_surface_view_on_timewait(struct mm_my_surface_view* p)
{
	mm_adaptive_timer_timewait(&p->adaptive_timer);
}
void mm_my_surface_view_on_timewake(struct mm_my_surface_view* p)
{
	mm_adaptive_timer_timewake(&p->adaptive_timer);
}
void mm_my_surface_view_start(struct mm_my_surface_view* p)
{
	p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
	pthread_create(&p->poll_thread, NULL, &__static_my_surface_view_poll_wait_thread, p);
	//
	mm_adaptive_timer_start(&p->adaptive_timer);
}
void mm_my_surface_view_interrupt(struct mm_my_surface_view* p)
{
	p->state = MM_TS_CLOSED;
	mm_adaptive_timer_interrupt(&p->adaptive_timer);
}
void mm_my_surface_view_shutdown(struct mm_my_surface_view* p)
{
	p->state = MM_TS_FINISH;
	mm_adaptive_timer_shutdown(&p->adaptive_timer);
}
void mm_my_surface_view_join(struct mm_my_surface_view* p)
{
	mm_adaptive_timer_join(&p->adaptive_timer);
	//
	pthread_join(p->poll_thread, NULL);
}
ATOM mm_my_surface_view_register_class(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcex.lpfnWndProc = __static_my_surface_view_WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_48x48));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = __static_my_surface_view_lpClassName;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON_32x32));

	return RegisterClassEx(&wcex);
}
BOOL mm_my_surface_view_unregister_class(HINSTANCE hInstance)
{
	return UnregisterClass(__static_my_surface_view_lpClassName, hInstance);
}
//////////////////////////////////////////////////////////////////////////
void mm_my_surface_view_poll_wait(struct mm_my_surface_view* p)
{
	// Perform application initialization.
	__static_my_surface_view_CreateWindow(p);
	// Show window.
	__static_my_surface_view_ShowWindow(p);
	// assign hWnd.
	mm_my_surface_egl_assign_hwnd(&p->my_surface_egl, p->hWnd);
	// surface EGLInitialize.
	mm_my_surface_egl_on_finish_launching(&p->my_surface_egl);
	// looper
	while (MM_TS_MOTION == p->state)
	{
		mm_my_surface_view_on_update(p);
		mm_my_surface_view_on_timewait(p);
		// PeekMessage.
		__static_my_surface_view_PeekMessage(p);
	}
	// surface EGLDestroy.
	mm_my_surface_egl_on_before_terminate(&p->my_surface_egl);
	// Destroy Window.
	__static_my_surface_view_DestroyWindow(p);
}
void mm_my_surface_view_on_logical_one_frame(struct mm_my_surface_view* p, double interval)
{

}
void mm_my_surface_view_on_display_one_frame(struct mm_my_surface_view* p, double interval)
{

}
//////////////////////////////////////////////////////////////////////////
static BOOL __static_my_surface_view_CreateWindow(struct mm_my_surface_view* p)
{
	RECT rc;
	BOOL bMenu = p->hMenu ? FALSE : TRUE;

	struct mm_logger* g_logger = mm_logger_instance();

	SetRect(&rc, p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);

	AdjustWindowRectEx(&rc, p->dwStyle, bMenu, p->dwExStyle);

	p->window_x = rc.left;
	p->window_y = rc.top;
	p->window_w = rc.right - rc.left;
	p->window_h = rc.bottom - rc.top;

	p->window_x = p->window_x < 0 ? 0 : p->window_x;
	p->window_y = p->window_y < 0 ? 0 : p->window_y;

	p->hWnd = CreateWindowEx(
		p->dwExStyle,
		__static_my_surface_view_lpClassName,
		p->lpWindowName,
		p->dwStyle,
		p->window_x,
		p->window_y,
		p->window_w,
		p->window_h,
		p->hWndParent,
		p->hMenu,
		p->hInstance,
		p->lpParam
	);

	if (!p->hWnd)
	{
		mm_logger_log_F(g_logger, "%s %d CreateWindowEx window((%d, %d), (%d, %d)) drawable((%d, %d), (%d, %d)) failure.", 
			__FUNCTION__, __LINE__, 
			p->window_x, p->window_y, p->window_w, p->window_h, 
			p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);
		return FALSE;
	}
	else
	{
		SetWindowLongPtr(p->hWnd, GWLP_USERDATA, (LONG_PTR)p);
		mm_logger_log_I(g_logger, "%s %d CreateWindowEx window((%d, %d), (%d, %d)) drawable((%d, %d), (%d, %d)) success.", 
			__FUNCTION__, __LINE__, 
			p->window_x, p->window_y, p->window_w, p->window_h,
			p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);
		return TRUE;
	}
}
static void __static_my_surface_view_DestroyWindow(struct mm_my_surface_view* p)
{
	if (NULL != p->hWnd)
	{
		DestroyWindow(p->hWnd);
		p->hWnd = NULL;
	}
}
static void __static_my_surface_view_CreateAccelerators(struct mm_my_surface_view* p)
{
	p->hAccelTable = LoadAccelerators(p->hInstance, MAKEINTRESOURCE(IDC_ACCELERATOR));
}
static void __static_my_surface_view_DestroyAccelerators(struct mm_my_surface_view* p)
{
	DestroyAcceleratorTable(p->hAccelTable);
	p->hAccelTable = NULL;
}
static void __static_my_surface_view_ShowWindow(struct mm_my_surface_view* p)
{
	ShowWindow(p->hWnd, p->nCmdShow);
	UpdateWindow(p->hWnd);
}
static void __static_my_surface_view_PeekMessage(struct mm_my_surface_view* p)
{
	MSG msg;
	// Peek message use PM_REMOVE.
	while (PeekMessage(&msg, p->hWnd, 0U, 0U, PM_REMOVE))
	{
		if (!TranslateAccelerator(msg.hwnd, p->hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
void __static_my_surface_view_OnUpdateSizeCache(struct mm_my_surface_view* p, int x, int y, int w, int h)
{
	RECT rc;
	BOOL bMenu = p->hMenu ? FALSE : TRUE;

	mm_my_surface_view_assign_rect(p, x, y, w, h);

	SetRect(&rc, p->drawable_x, p->drawable_y, p->drawable_w, p->drawable_h);

	AdjustWindowRectEx(&rc, p->dwStyle, bMenu, p->dwExStyle);

	p->window_x = rc.left;
	p->window_y = rc.top;
	p->window_w = rc.right - rc.left;
	p->window_h = rc.bottom - rc.top;

	p->window_x = p->window_x < 0 ? 0 : p->window_x;
	p->window_y = p->window_y < 0 ? 0 : p->window_y;
}
void __static_my_surface_view_OnSizeChange(struct mm_my_surface_view* p, int w, int h)
{
	__static_my_surface_view_OnUpdateSizeCache(p, p->drawable_x, p->drawable_y, w, h);
}
static LRESULT CALLBACK __static_my_surface_view_WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	struct mm_my_surface_view* p = (struct mm_my_surface_view*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

	if (NULL != p)
	{
		switch (message)
		{
		case WM_CLOSE:
		{
			mm_my_surface_view_shutdown(p);
		}
		break;
		break;
		case WM_SIZE:
		{
			if (FALSE == IsIconic(hWnd))
			{
				int w = LOWORD(lParam); // width of client area 
				int h = HIWORD(lParam); // Hight of client area 
				__static_my_surface_view_OnSizeChange(p, w, h);
			}
		}
		break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code using hdc here...
			EndPaint(hWnd, &ps);
		}
		break;
		case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
		case WM_KEYDOWN:
		{
			mm_printf("WM_KEYDOWN\n");
		}
		break;
		case WM_KEYUP:
		{
			mm_printf("WM_KEYUP\n");
		}
		break;
		default:
		{
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		}
		return 0;
	}
	else
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}
//////////////////////////////////////////////////////////////////////////
static void* __static_my_surface_view_poll_wait_thread(void* _arg)
{
	struct mm_my_surface_view* p = (struct mm_my_surface_view*)(_arg);
	mm_my_surface_view_poll_wait(p);
	return NULL;
}
//////////////////////////////////////////////////////////////////////////
static void __static_my_surface_view_adaptive_timer_unit_update_logical(void* obj, double interval)
{
	struct mm_frame_timer* unit = (struct mm_frame_timer*)(obj);
	struct mm_my_surface_view* surface = (struct mm_my_surface_view*)(unit->callback.obj);
	mm_my_surface_view_on_logical_one_frame(surface, interval);
	// mm_frame_stats_logger_average(&unit->status, unit->name.s);
}
static void __static_my_surface_view_adaptive_timer_unit_update_display(void* obj, double interval)
{
	struct mm_frame_timer* unit = (struct mm_frame_timer*)(obj);
	struct mm_my_surface_view* surface = (struct mm_my_surface_view*)(unit->callback.obj);
	mm_my_surface_view_on_display_one_frame(surface, interval);
	// mm_frame_stats_logger_average(&unit->status, unit->name.s);
}
//////////////////////////////////////////////////////////////////////////