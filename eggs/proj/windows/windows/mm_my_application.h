#ifndef __mm_my_application_h__
#define __mm_my_application_h__

#include "core/mm_core.h"

#include "mm_my_context.h"
#include "mm_my_surface_main.h"

#include "core/mm_prefix.h"

//////////////////////////////////////////////////////////////////////////
struct mm_my_parameter
{
	HINSTANCE hInstance;
	HINSTANCE hPrevInstance;
	LPWSTR lpCmdLine;
	int nCmdShow;
};
extern void mm_my_parameter_init(struct mm_my_parameter* p);
extern void mm_my_parameter_destroy(struct mm_my_parameter* p);

struct mm_my_application
{
	struct mm_my_parameter my_parameter;
	struct mm_my_surface_main my_surface_main;
};
extern void mm_my_application_init(struct mm_my_application* p);
extern void mm_my_application_destroy(struct mm_my_application* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_application_assign_instance(struct mm_my_application* p, HINSTANCE hInstance);
extern void mm_my_application_assign_prev_instance(struct mm_my_application* p, HINSTANCE hPrevInstance);
extern void mm_my_application_assign_cmd_line(struct mm_my_application* p, LPWSTR lpCmdLine);
extern void mm_my_application_assign_cmd_show(struct mm_my_application* p, int nCmdShow);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_application_finish_launching(struct mm_my_application* p);
extern void mm_my_application_before_terminate(struct mm_my_application* p);
//////////////////////////////////////////////////////////////////////////
extern void mm_my_application_start(struct mm_my_application* p);
extern void mm_my_application_interrupt(struct mm_my_application* p);
extern void mm_my_application_shutdown(struct mm_my_application* p);
extern void mm_my_application_join(struct mm_my_application* p);
//////////////////////////////////////////////////////////////////////////
extern ATOM mm_my_application_register_class(HINSTANCE hInstance);
extern BOOL mm_my_application_unregister_class(HINSTANCE hInstance);
//////////////////////////////////////////////////////////////////////////
#include "core/mm_suffix.h"

#endif//__mm_my_application_h__