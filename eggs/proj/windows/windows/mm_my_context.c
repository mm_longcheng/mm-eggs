#include "mm_my_context.h"

//////////////////////////////////////////////////////////////////////////
void mm_my_context_init(struct mm_my_context* p)
{
	p->hInstance = NULL;
}
void mm_my_context_destroy(struct mm_my_context* p)
{
	p->hInstance = NULL;
}
//////////////////////////////////////////////////////////////////////////
void mm_my_context_assign_instance(struct mm_my_context* p, HINSTANCE hInstance)
{
	p->hInstance = hInstance;
}
//////////////////////////////////////////////////////////////////////////
void mm_my_context_on_finish_launching(struct mm_my_context* p)
{

}
void mm_my_context_on_before_terminate(struct mm_my_context* p)
{

}
//////////////////////////////////////////////////////////////////////////
