LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libmm_cherry_shared
LOCAL_MODULE_FILENAME := libmm_cherry_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-undefined-var-template

LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-return-type-c-linkage

LOCAL_CXXFLAGS += -DMM_STATIC_RAPTORQ

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libsqlite_shared
LOCAL_SHARED_LIBRARIES += libCEGUICoreWindowRendererSet_shared
LOCAL_SHARED_LIBRARIES += libCEGUIOgreRenderer_shared
LOCAL_SHARED_LIBRARIES += libmm_net_shared
LOCAL_SHARED_LIBRARIES += libmm_flake_shared
########################################################################
LOCAL_STATIC_LIBRARIES += libmm_fix32_static

LOCAL_STATIC_LIBRARIES += libmm_emulator_static

LOCAL_STATIC_LIBRARIES += libmm_data_openssl_static
LOCAL_STATIC_LIBRARIES += libmm_data_lua_static
LOCAL_STATIC_LIBRARIES += libmm_data_protobuf_static

LOCAL_STATIC_LIBRARIES += libmm_emu_static

LOCAL_STATIC_LIBRARIES += libcrypto_static
LOCAL_STATIC_LIBRARIES += libprotobuf_static
LOCAL_STATIC_LIBRARIES += liblua_static

LOCAL_STATIC_LIBRARIES += libminizip_static

LOCAL_STATIC_LIBRARIES += libOgreAL_static

LOCAL_STATIC_LIBRARIES += libBulletWorldImporter_static
LOCAL_STATIC_LIBRARIES += libBulletFileLoader_static
LOCAL_STATIC_LIBRARIES += libGIMPACTUtils_static
LOCAL_STATIC_LIBRARIES += libConvexDecomposition_static
LOCAL_STATIC_LIBRARIES += libBulletDynamics_static
LOCAL_STATIC_LIBRARIES += libBulletCollision_static
LOCAL_STATIC_LIBRARIES += libLinearMath_static
########################################################################
INSTALL_MODEL := mm_core_packet
INSTALL_FILES := $(MM_HOME)/mm_core/mm/proj/android/bin/mm_core.jar
INSTALL_OPATH := $(NDK_APP_LIBS_OUT)
include $(MM_MAKE_HOME)/compile/prebuilt-install.mk
########################################################################
INSTALL_MODEL := mm_flake_packet
INSTALL_FILES := $(MM_HOME)/mm_core/flake/proj/android/bin/mm_flake.jar
INSTALL_OPATH := $(NDK_APP_LIBS_OUT)
include $(MM_MAKE_HOME)/compile/prebuilt-install.mk
########################################################################
INSTALL_MODEL := mm_flake_native_lib
INSTALL_FILES := $(MM_HOME)/mm_lib/build/Ogre/proj_android/libs/$(TARGET_ARCH_ABI)/libPlugin_ParticleFX_shared.so
INSTALL_FILES += $(MM_HOME)/mm_lib/build/Ogre/proj_android/libs/$(TARGET_ARCH_ABI)/libPlugin_OctreeSceneManager_shared.so
INSTALL_FILES += $(MM_HOME)/mm_lib/build/cegui/proj_android/libs/$(TARGET_ARCH_ABI)/libCEGUICoreWindowRendererSet_shared.so
INSTALL_OPATH := $(NDK_APP_LIBS_OUT)/$(TARGET_ARCH_ABI)
include $(MM_MAKE_HOME)/compile/prebuilt-install.mk
########################################################################
LOCAL_C_INCLUDES += $(MM_HOME)/mm_lib/src/protobuf/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm_lib/src/openssl/include
LOCAL_C_INCLUDES += $(MM_HOME)/mm_lib/build/openssl/include/android
LOCAL_C_INCLUDES += $(MM_HOME)/mm_lib/src/zlib/contrib
LOCAL_C_INCLUDES += $(MM_HOME)/mm_lib/build/sqlite/include/android
LOCAL_C_INCLUDES += $(MM_HOME)/mm_lib/src/sqlite/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm_core/data/src
LOCAL_C_INCLUDES += $(MM_HOME)/mm_nes/emu/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/cherry
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
include $(MM_HOME)/mm_core/flake/proj/android/jni/lib_flake_flags.mk
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/cherry
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/protodef/cxx/protodef
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/cherry/application_old%
MY_SOURCES_FILTER_OUT += ../../../src/cherry/xx%
MY_SOURCES_FILTER_OUT += ../../../src/cherry/layer/emulator/ooo%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################