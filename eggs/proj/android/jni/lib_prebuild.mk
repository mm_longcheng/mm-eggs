# prebuild.mk
################################################################################
MM_HOME ?= ../../../../..
################################################################################
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
include $(MM_HOME)/mm_core/flake/proj/android/jni/lib_flake_prebuild.mk
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/zlib/proj_android/libs/$(TARGET_ARCH_ABI)/libminizip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/zlib/proj_android/obj/local/$(TARGET_ARCH_ABI)/libminizip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_fix32_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_fix32_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_fix32_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_fix32_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_entity_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_entity_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_entity_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_entity_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_raptorq_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_raptorq_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_raptorq_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_raptorq_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_track_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_track_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_track_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_track_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_rq_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_rq_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_rq_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/dish/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_rq_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_data_lua_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/data/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_data_lua_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_data_lua_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/data/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_data_lua_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_data_openssl_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/data/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_data_openssl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_data_openssl_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/data/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_data_openssl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_data_protobuf_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/data/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_data_protobuf_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_data_protobuf_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/data/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_data_protobuf_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_flake_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/flake/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_flake_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_flake_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/flake/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_flake_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libCEGUIOgreRenderer_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/flake/proj/android/libs/$(TARGET_ARCH_ABI)/libCEGUIOgreRenderer_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libCEGUIOgreRenderer_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/flake/proj/android/obj/local/$(TARGET_ARCH_ABI)/libCEGUIOgreRenderer_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_emulator_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/flake/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_emulator_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_emulator_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_core/flake/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_emulator_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_emu_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_nes/emu/proj/android/libs/$(TARGET_ARCH_ABI)/libmm_emu_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_emu_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_nes/emu/proj/android/obj/local/$(TARGET_ARCH_ABI)/libmm_emu_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libcrypto_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/openssl/proj_android/obj/local/$(TARGET_ARCH_ABI)/libcrypto_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libssl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/openssl/proj_android/obj/local/$(TARGET_ARCH_ABI)/libssl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotobuf_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/protobuf/proj_android/libs/$(TARGET_ARCH_ABI)/libprotobuf_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotobuf_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/protobuf/proj_android/obj/local/$(TARGET_ARCH_ABI)/libprotobuf_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libsqlite_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/sqlite/proj_android/libs/$(TARGET_ARCH_ABI)/libsqlite_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libsqlite_static
LOCAL_SRC_FILES := $(MM_HOME)/mm_lib/build/sqlite/proj_android/obj/local/$(TARGET_ARCH_ABI)/libsqlite_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################





