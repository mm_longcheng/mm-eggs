#version 120

uniform sampler2D texture0;

varying vec2 outUV0;
varying vec4 outColor;

void main(void)
{
   gl_FragColor = texture2D(texture0, outUV0) * outColor;
}